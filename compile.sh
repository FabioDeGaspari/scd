COMPILE_DIRS=( "name_server" "district" "controller" "web_frontend")

CURDIR=$(pwd)
cd $(dirname $0)
BASEDIR=$(pwd)
cd $CURDIR
 
 
SRC_PATH=${BASEDIR}/src
BUILD_PATH=${BASEDIR}/build
LIB_PATH=${BASEDIR}/lib
SRC_FILE_LIST=${BASEDIR}/jfile.list
export LIB_PATH=$LIB_PATH
export BUILD_PATH=$BUILD_PATH
 
if [ $# -gt 0 ] && [ $1 == 'clean' ]
then
    if [ -d $BUILD_PATH ]
    then
        rm -rf $BUILD_PATH/*
    fi
    printf "clean complete\n"
    exit
fi

mkdir -p $BUILD_PATH
cd $SRC_PATH

for D in ${COMPILE_DIRS[@]}; do
	cd $D
	pwd
	./build.sh
	cd ../
done
