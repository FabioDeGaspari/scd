var segments = []
var lanes = []
var pois = []
var roundabouts = []
var buslanes = []
var carlanes = []
var walkways = []
var districts = []

segments["423-426"] = new Segment(raphael_canvas, 3600, 1300, 3700, 1300, 0.0, 0.0, "n423.2-n426.1", true, true, true, PoiType.CROSSROAD, "n426.2-n423.1", true, true, true, PoiType.PUB);
segments["380-388"] = new Segment(raphael_canvas, 2850, 1950, 2850, 1700, -0.7853981633974483, -0.7853981633974483, "n380.1-n388.2", true, true, true, PoiType.MALL, "n388.1-n380.2", true, true, true, PoiType.PARKING);
segments["280-281"] = new Segment(raphael_canvas, 2550, 50, 2600, 100, 0.7853981633974483, 0.7853981633974483, "n280.1-n281.1", true, true, true, PoiType.HOME, "n281.2-n280.2", true, true, true, PoiType.POST);
segments["193-194"] = new Segment(raphael_canvas, 750, 2000, 750, 2050, 0.7853981633974483, 0.7853981633974483, "n193.1-n194.1", true, true, true, PoiType.PUB, "n194.2-n193.2", true, true, true, PoiType.PUB);
segments["339-472"] = new Segment(raphael_canvas, 2600, 1100, 2500, 1200, 0.7853981633974483, -0.7853981633974483, "n472.1-n339.1", true, true, true, PoiType.BANK, "n339.2-n472.2", true, true, true, PoiType.BANK);
segments["294-295"] = new Segment(raphael_canvas, 2100, 700, 2100, 600, 0.7853981633974483, 0.7853981633974483, "n294.1-n295.1", true, true, true, PoiType.POST, "n295.2-n294.2", true, true, true, PoiType.SHOP);
segments["355-356"] = new Segment(raphael_canvas, 2300, 1650, 2200, 1650, 0.0, -0.7853981633974483, "n355.1-n356.1", true, true, true, PoiType.AMUSEMENT_PARK, "n356.2-n355.2", true, true, true, PoiType.UNIVERSITY);
segments["67-71"] = new Segment(raphael_canvas, 850, 600, 850, 550, 0.0, 0.0, "n67.1-n71.1", true, true, true, PoiType.UNIVERSITY, "n71.2-n67.2", true, true, true, PoiType.CROSSROAD);
segments["393-394"] = new Segment(raphael_canvas, 3450, 150, 3350, 50, -0.7853981633974483, 5.497787143782138, "n393.1-n394.1", true, true, true, PoiType.BANK, "n394.2-n393.2", true, true, true, PoiType.SPORT_CENTER);
segments["195-196"] = new Segment(raphael_canvas, 700, 2100, 550, 2100, 0.7853981633974483, 0.0, "n195.1-n196.1", true, true, true, PoiType.SPORT_CENTER, "n196.2-n195.2", true, true, true, PoiType.MALL);
segments["33-56"] = new Segment(raphael_canvas, 650, 400, 650, 450, 0.0, -0.7853981633974483, "n33.1-n56.1", true, true, true, PoiType.MALL, "n56.2-n33.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["475-476"] = new Segment(raphael_canvas, 1650, 400, 1650, 450, 0.7853981633974483, 0.0, "n475.1-n476.1", true, true, true, PoiType.MALL, "n476.2-n475.2", true, true, true, PoiType.SPORT_CENTER);
segments["170-171"] = new Segment(raphael_canvas, 900, 1550, 1000, 1650, -0.7853981633974483, 0.7853981633974483, "n170.1-n171.1", true, true, true, PoiType.HOME, "n171.2-n170.2", true, true, true, PoiType.HOME);
segments["208-212"] = new Segment(raphael_canvas, 1700, 900, 1700, 950, 0.7853981633974483, 0.0, "n208.1-n212.1", true, true, true, PoiType.HOME, "n212.2-n208.2", true, true, true, PoiType.CROSSROAD);
segments["400-401"] = new Segment(raphael_canvas, 3150, 50, 3050, 50, 0.0, 0.0, "n400.1-n401.1", true, true, true, PoiType.BANK, "n401.2-n400.2", true, true, true, PoiType.SHOP);
segments["268-272"] = new Segment(raphael_canvas, 2100, 150, 2150, 150, 0.0, 0.0, "n268.2-n272.1", true, true, true, PoiType.CROSSROAD, "n272.2-n268.1", true, true, true, PoiType.BANK);
segments["210-243"] = new Segment(raphael_canvas, 1700, 1100, 1700, 1050, 0.0, 0.0, "n243.1-n210.1", true, true, true, PoiType.POST, "n210.2-n243.2", true, true, true, PoiType.CROSSROAD);
segments["72-197"] = new Segment(raphael_canvas, 900, 500, 950, 500, 0.0, 0.0, "n72.2-n197.1", true, true, true, PoiType.CROSSROAD, "n197.2-n72.1", true, true, true, PoiType.PUB);
segments["122-123"] = new Segment(raphael_canvas, 200, 1000, 200, 1100, 0.0, 0.0, "n122.1-n123.1", true, true, true, PoiType.PUB, "n123.2-n122.2", true, true, true, PoiType.SHOP);
segments["467-468"] = new Segment(raphael_canvas, 3050, 1650, 2950, 1550, 0.0, 0.7853981633974483, "n467.1-n468.1", true, true, true, PoiType.PARKING, "n468.2-n467.2", true, true, true, PoiType.PARKING);
segments["292-293"] = new Segment(raphael_canvas, 2200, 900, 2200, 800, -0.7853981633974483, -0.7853981633974483, "n292.1-n293.1", true, true, true, PoiType.UNIVERSITY, "n293.2-n292.2", true, true, true, PoiType.BANK);
segments["191-192"] = new Segment(raphael_canvas, 550, 1900, 650, 1900, 0.0, 0.7853981633974483, "n191.1-n192.1", true, true, true, PoiType.HOME, "n192.2-n191.2", true, true, true, PoiType.POST);
segments["160-161"] = new Segment(raphael_canvas, 750, 1250, 850, 1250, 0.7853981633974483, 0.0, "n160.1-n161.1", true, true, true, PoiType.MALL, "n161.2-n160.2", true, true, true, PoiType.CROSSROAD);
segments["0-1"] = new Segment(raphael_canvas, 50, 200, 150, 200, 0.0, 0.0, "n0.1-n1.1", true, true, true, PoiType.HOME, "n1.2-n0.2", true, true, true, PoiType.CROSSROAD);
segments["291-292"] = new Segment(raphael_canvas, 2100, 1000, 2200, 900, -0.7853981633974483, -0.7853981633974483, "n291.1-n292.1", true, true, true, PoiType.BANK, "n292.2-n291.2", true, true, true, PoiType.UNIVERSITY);
segments["452-456"] = new Segment(raphael_canvas, 3600, 1550, 3600, 1450, 0.0, 0.0, "n452.2-n456.1", true, true, true, PoiType.CROSSROAD, "n456.2-n452.1", true, true, true, PoiType.POST);
segments["392-406"] = new Segment(raphael_canvas, 3450, 350, 3450, 400, 0.0, 0.0, "n392.2-n406.1", true, true, true, PoiType.CROSSROAD, "n406.2-n392.1", true, true, true, PoiType.BANK);
segments["468-470"] = new Segment(raphael_canvas, 2950, 1550, 2950, 1350, 0.7853981633974483, 0.0, "n468.1-n470.1", true, true, true, PoiType.PARKING, "n470.2-n468.2", true, true, true, PoiType.PARKING);
segments["230-471"] = new Segment(raphael_canvas, 1400, 1450, 1600, 1450, -0.7853981633974483, -0.7853981633974483, "n230.1-n471.1", true, true, true, PoiType.AMUSEMENT_PARK, "n471.2-n230.2", true, true, true, PoiType.PUB);
segments["40-45"] = new Segment(raphael_canvas, 200, 700, 200, 750, 0.0, 0.0, "n40.1-n45.1", true, true, true, PoiType.UNIVERSITY, "n45.2-n40.2", true, true, true, PoiType.CROSSROAD);
segments["336-391"] = new Segment(raphael_canvas, 3350, 300, 3400, 300, 0.0, 0.0, "n336.1-n391.1", true, true, true, PoiType.SPORT_CENTER, "n391.2-n336.2", true, true, true, PoiType.CROSSROAD);
segments["367-368"] = new Segment(raphael_canvas, 1950, 2050, 1850, 2050, 0.0, -5.497787143782138, "n367.1-n368.1", true, true, true, PoiType.AMUSEMENT_PARK, "n368.2-n367.2", true, true, true, PoiType.SPORT_CENTER);
segments["52-53"] = new Segment(raphael_canvas, 500, 800, 600, 800, 0.0, 0.0, "n52.1-n53.1", true, true, true, PoiType.PUB, "n53.2-n52.2", true, true, true, PoiType.POST);
segments["123-124"] = new Segment(raphael_canvas, 200, 1100, 200, 1200, 0.0, -0.7853981633974483, "n123.1-n124.1", true, true, true, PoiType.SPORT_CENTER, "n124.2-n123.2", true, true, true, PoiType.SPORT_CENTER);
segments["269-270"] = new Segment(raphael_canvas, 1900, 150, 1800, 150, 0.0, -0.7853981633974483, "n269.1-n270.1", true, true, true, PoiType.AMUSEMENT_PARK, "n270.2-n269.2", true, true, true, PoiType.SPORT_CENTER);
segments["110-476"] = new Segment(raphael_canvas, 1650, 450, 1650, 550, 0.0, 0.0, "n476.1-n110.1", true, true, true, PoiType.PUB, "n110.2-n476.2", true, true, true, PoiType.CROSSROAD);
segments["297-306"] = new Segment(raphael_canvas, 2500, 550, 2550, 550, 0.0, 0.0, "n306.1-n297.1", true, true, true, PoiType.BANK, "n297.2-n306.2", true, true, true, PoiType.CROSSROAD);
segments["141-144"] = new Segment(raphael_canvas, 250, 1450, 250, 1500, 0.0, 0.0, "n141.1-n144.1", true, true, true, PoiType.PUB, "n144.2-n141.2", true, true, true, PoiType.CROSSROAD);
segments["387-388"] = new Segment(raphael_canvas, 2850, 1700, 2800, 1650, -0.7853981633974483, -0.7853981633974483, "n388.2-n387.2", true, true, true, PoiType.PARKING, "n387.1-n388.1", true, true, true, PoiType.PARKING);
segments["278-279"] = new Segment(raphael_canvas, 2350, 150, 2450, 50, -0.7853981633974483, 0.7853981633974483, "n278.1-n279.1", true, true, true, PoiType.UNIVERSITY, "n279.2-n278.2", true, true, true, PoiType.SHOP);
segments["414-416"] = new Segment(raphael_canvas, 3550, 850, 3550, 1000, 0.0, 0.0, "n414.1-n416.1", true, true, true, PoiType.AMUSEMENT_PARK, "n416.2-n414.2", true, true, true, PoiType.BANK);
segments["256-257"] = new Segment(raphael_canvas, 1950, 600, 2050, 500, -0.7853981633974483, -0.7853981633974483, "n256.1-n257.1", true, true, true, PoiType.SPORT_CENTER, "n257.2-n256.2", true, true, true, PoiType.UNIVERSITY);
segments["142-145"] = new Segment(raphael_canvas, 250, 1600, 250, 1700, 0.0, 0.0, "n142.2-n145.1", true, true, true, PoiType.CROSSROAD, "n145.2-n142.1", true, true, true, PoiType.PUB);
segments["301-477"] = new Segment(raphael_canvas, 2150, 550, 2400, 550, 0.7853981633974483, 0.0, "n477.1-n301.1", true, true, true, PoiType.MALL, "n301.2-n477.2", true, true, true, PoiType.SHOP);
segments["290-291"] = new Segment(raphael_canvas, 2000, 1000, 2100, 1000, 0.0, -0.7853981633974483, "n290.1-n291.1", true, true, true, PoiType.PUB, "n291.2-n290.2", true, true, true, PoiType.MALL);
segments["178-181"] = new Segment(raphael_canvas, 1000, 2000, 1000, 2100, 0.0, 0.0, "n178.2-n181.1", true, true, true, PoiType.CROSSROAD, "n181.2-n178.1", true, true, true, PoiType.BANK);
segments["207-208"] = new Segment(raphael_canvas, 1650, 850, 1700, 900, -0.7853981633974483, 0.7853981633974483, "n207.1-n208.1", true, true, true, PoiType.UNIVERSITY, "n208.2-n207.2", true, true, true, PoiType.POST);
segments["277-278"] = new Segment(raphael_canvas, 2250, 150, 2350, 150, 0.0, -0.7853981633974483, "n277.1-n278.1", true, true, true, PoiType.BANK, "n278.2-n277.2", true, true, true, PoiType.SPORT_CENTER);
segments["219-220"] = new Segment(raphael_canvas, 1250, 750, 1250, 900, 0.0, -0.7853981633974483, "n219.1-n220.1", true, true, true, PoiType.BANK, "n220.2-n219.2", true, true, true, PoiType.SHOP);
segments["466-467"] = new Segment(raphael_canvas, 3150, 1750, 3050, 1650, 0.0, 0.0, "n466.1-n467.1", true, true, true, PoiType.UNIVERSITY, "n467.2-n466.2", true, true, true, PoiType.PARKING);
segments["116-207"] = new Segment(raphael_canvas, 1650, 800, 1650, 850, 0.0, -0.7853981633974483, "n116.1-n207.1", true, true, true, PoiType.PUB, "n207.2-n116.2", true, true, true, PoiType.SPORT_CENTER);
segments["329-331"] = new Segment(raphael_canvas, 3200, 300, 3250, 300, 0.0, 0.0, "n329.2-n331.1", true, true, true, PoiType.CROSSROAD, "n331.2-n329.1", true, true, true, PoiType.PUB);
segments["376-377"] = new Segment(raphael_canvas, 2300, 2050, 2450, 2050, 0.0, 0.0, "n376.1-n377.1", true, true, true, PoiType.UNIVERSITY, "n377.2-n376.2", true, true, true, PoiType.MALL);
segments["283-289"] = new Segment(raphael_canvas, 2600, 250, 2600, 350, 0.0, 0.0, "n289.1-n283.1", true, true, true, PoiType.MALL, "n283.2-n289.2", true, true, true, PoiType.UNIVERSITY);
segments["209-218"] = new Segment(raphael_canvas, 1650, 1000, 1600, 1000, 0.0, 0, "n209.2-n218.2", true, true, true, PoiType.CROSSROAD, "n218.1-n209.1", true, true, true, PoiType.PUB);
segments["425-432"] = new Segment(raphael_canvas, 3500, 1300, 3450, 1300, 0.0, 0.0, "n425.2-n432.1", true, true, true, PoiType.CROSSROAD, "n432.2-n425.1", true, true, true, PoiType.HOME);
segments["192-193"] = new Segment(raphael_canvas, 650, 1900, 750, 2000, 0.7853981633974483, 0.7853981633974483, "n192.1-n193.1", true, true, true, PoiType.HOME, "n193.2-n192.2", true, true, true, PoiType.BANK);
segments["339-340"] = new Segment(raphael_canvas, 2500, 1200, 2500, 1350, -0.7853981633974483, 0.0, "n339.1-n340.1", true, true, true, PoiType.HOME, "n340.2-n339.2", true, true, true, PoiType.MALL);
segments["401-402"] = new Segment(raphael_canvas, 3050, 50, 2950, 50, 0.0, -0.7853981633974483, "n401.1-n402.1", true, true, true, PoiType.POST, "n402.2-n401.2", true, true, true, PoiType.PUB);
segments["455-457"] = new Segment(raphael_canvas, 3600, 1700, 3600, 1800, 0.0, 0.7853981633974483, "n455.1-n457.1", true, true, true, PoiType.POST, "n457.2-n455.2", true, true, true, PoiType.POST);
segments["232-471"] = new Segment(raphael_canvas, 1600, 1450, 1700, 1350, -0.7853981633974483, -0.7853981633974483, "n471.1-n232.1", true, true, true, PoiType.POST, "n232.2-n471.2", true, true, true, PoiType.SPORT_CENTER);
segments["143-146"] = new Segment(raphael_canvas, 300, 1550, 350, 1550, 0.0, 0.0, "n143.2-n146.1", true, true, true, PoiType.CROSSROAD, "n146.2-n143.1", true, true, true, PoiType.BANK);
segments["454-455"] = new Segment(raphael_canvas, 3600, 1650, 3600, 1700, 0.0, 0.0, "n454.2-n455.1", true, true, true, PoiType.CROSSROAD, "n455.2-n454.1", true, true, true, PoiType.BANK);
segments["474-475"] = new Segment(raphael_canvas, 1550, 300, 1650, 400, 0.7853981633974483, 0.7853981633974483, "n474.1-n475.1", true, true, true, PoiType.AMUSEMENT_PARK, "n475.2-n474.2", true, true, true, PoiType.PUB);
segments["220-221"] = new Segment(raphael_canvas, 1250, 900, 1350, 1000, -0.7853981633974483, -0.7853981633974483, "n220.1-n221.1", true, true, true, PoiType.MALL, "n221.2-n220.2", true, true, true, PoiType.SPORT_CENTER);
segments["203-205"] = new Segment(raphael_canvas, 1150, 500, 1250, 600, 0.7853981633974483, 0.7853981633974483, "n203.1-n205.1", true, true, true, PoiType.PUB, "n205.2-n203.2", true, true, true, PoiType.SHOP);
segments["366-367"] = new Segment(raphael_canvas, 2050, 2050, 1950, 2050, 0.0, 0.0, "n366.2-n367.1", true, true, true, PoiType.CROSSROAD, "n367.2-n366.1", true, true, true, PoiType.MALL);
segments["364-371"] = new Segment(raphael_canvas, 2150, 2050, 2200, 2050, 0.0, 0.0, "n364.2-n371.1", true, true, true, PoiType.CROSSROAD, "n371.2-n364.1", true, true, true, PoiType.MALL);
segments["249-256"] = new Segment(raphael_canvas, 1850, 600, 1950, 600, 0.0, -0.7853981633974483, "n249.1-n256.1", true, true, true, PoiType.PUB, "n256.2-n249.2", true, true, true, PoiType.UNIVERSITY);
segments["184-185"] = new Segment(raphael_canvas, 250, 1800, 350, 1900, -0.7853981633974483, -0.7853981633974483, "n184.1-n185.1", true, true, true, PoiType.SHOP, "n185.2-n184.2", true, true, true, PoiType.HOME);
segments["295-477"] = new Segment(raphael_canvas, 2100, 600, 2150, 550, 0.7853981633974483, 0.7853981633974483, "n295.1-n477.1", true, true, true, PoiType.MALL, "n477.2-n295.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["151-157"] = new Segment(raphael_canvas, 450, 1550, 550, 1550, 0.0, -0.7853981633974483, "n151.1-n157.1", true, true, true, PoiType.UNIVERSITY, "n157.2-n151.2", true, true, true, PoiType.MALL);
segments["368-369"] = new Segment(raphael_canvas, 1850, 2050, 1750, 1950, -5.497787143782138, 0.7853981633974483, "n368.1-n369.1", true, true, true, PoiType.POST, "n369.2-n368.2", true, true, true, PoiType.PUB);
segments["379-380"] = new Segment(raphael_canvas, 2750, 2050, 2850, 1950, -0.7853981633974483, -0.7853981633974483, "n379.1-n380.1", true, true, true, PoiType.POST, "n380.2-n379.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["194-195"] = new Segment(raphael_canvas, 750, 2050, 700, 2100, 0.7853981633974483, 0.7853981633974483, "n194.1-n195.1", true, true, true, PoiType.HOME, "n195.2-n194.2", true, true, true, PoiType.SPORT_CENTER);
segments["88-89"] = new Segment(raphael_canvas, 850, 50, 850, 150, 0.0, 0.0, "n89.2-n88.1", true, true, true, PoiType.POST, "n88.2-n89.1", true, true, true, PoiType.CROSSROAD);
segments["369-370"] = new Segment(raphael_canvas, 1750, 1950, 1750, 1800, 0.7853981633974483, 0.0, "n369.1-n370.1", true, true, true, PoiType.SHOP, "n370.2-n369.2", true, true, true, PoiType.HOME);
segments["340-341"] = new Segment(raphael_canvas, 2500, 1350, 2500, 1450, 0.0, 0.0, "n340.1-n341.1", true, true, true, PoiType.MALL, "n341.2-n340.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["125-136"] = new Segment(raphael_canvas, 250, 1250, 250, 1350, 0.7853981633974483, 0.0, "n125.1-n136.1", true, true, true, PoiType.AMUSEMENT_PARK, "n136.2-n125.2", true, true, true, PoiType.SPORT_CENTER);
segments["60-61"] = new Segment(raphael_canvas, 800, 800, 850, 750, -0.7853981633974483, -0.7853981633974483, "n60.1-n61.1", true, true, true, PoiType.HOME, "n61.2-n60.2", true, true, true, PoiType.POST);
segments["377-378"] = new Segment(raphael_canvas, 2450, 2050, 2600, 2050, 0.0, 0.0, "n377.1-n378.1", true, true, true, PoiType.UNIVERSITY, "n378.2-n377.2", true, true, true, PoiType.SHOP);
segments["413-414"] = new Segment(raphael_canvas, 3550, 700, 3550, 850, 0.7853981633974483, 0.0, "n413.1-n414.1", true, true, true, PoiType.UNIVERSITY, "n414.2-n413.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["44-46"] = new Segment(raphael_canvas, 250, 800, 300, 800, 0.0, 0.0, "n44.2-n46.1", true, true, true, PoiType.CROSSROAD, "n46.2-n44.1", true, true, true, PoiType.AMUSEMENT_PARK);
segments["378-379"] = new Segment(raphael_canvas, 2600, 2050, 2750, 2050, 0.0, -0.7853981633974483, "n378.1-n379.1", true, true, true, PoiType.POST, "n379.2-n378.2", true, true, true, PoiType.POST);
segments["108-111"] = new Segment(raphael_canvas, 1650, 650, 1650, 700, 0.0, 0.0, "n108.2-n111.1", true, true, true, PoiType.CROSSROAD, "n111.2-n108.1", true, true, true, PoiType.SHOP);
segments["266-269"] = new Segment(raphael_canvas, 2000, 150, 1900, 150, 0.0, 0.0, "n266.2-n269.1", true, true, true, PoiType.CROSSROAD, "n269.2-n266.1", true, true, true, PoiType.BANK);
segments["270-271"] = new Segment(raphael_canvas, 1800, 150, 1700, 250, -0.7853981633974483, 0.0, "n270.1-n271.1", true, true, true, PoiType.PUB, "n271.2-n270.2", true, true, true, PoiType.MALL);
segments["53-60"] = new Segment(raphael_canvas, 600, 800, 800, 800, 0.0, -0.7853981633974483, "n53.1-n60.1", true, true, true, PoiType.SPORT_CENTER, "n60.2-n53.2", true, true, true, PoiType.UNIVERSITY);
segments["263-267"] = new Segment(raphael_canvas, 2050, 300, 2050, 200, 0.0, 0.0, "n263.1-n267.1", true, true, true, PoiType.AMUSEMENT_PARK, "n267.2-n263.2", true, true, true, PoiType.CROSSROAD);
segments["97-98"] = new Segment(raphael_canvas, 1250, 300, 1350, 300, -0.7853981633974483, 0.0, "n97.1-n98.1", true, true, true, PoiType.SPORT_CENTER, "n98.2-n97.2", true, true, true, PoiType.POST);
segments["56-473"] = new Segment(raphael_canvas, 650, 450, 700, 500, -0.7853981633974483, -0.7853981633974483, "n56.1-n473.1", true, true, true, PoiType.AMUSEMENT_PARK, "n473.2-n56.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["349-350"] = new Segment(raphael_canvas, 2450, 1650, 2400, 1650, 0.0, 0.0, "n349.2-n350.1", true, true, true, PoiType.CROSSROAD, "n350.2-n349.1", true, true, true, PoiType.POST);
segments["429-430"] = new Segment(raphael_canvas, 3200, 1400, 3200, 1500, -0.7853981633974483, -0.7853981633974483, "n429.1-n430.1", true, true, true, PoiType.HOME, "n430.2-n429.2", true, true, true, PoiType.BANK);
segments["422-424"] = new Segment(raphael_canvas, 3550, 1200, 3550, 1250, 0.0, 0.0, "n422.1-n424.1", true, true, true, PoiType.UNIVERSITY, "n424.2-n422.2", true, true, true, PoiType.CROSSROAD);
segments["321-328"] = new Segment(raphael_canvas, 3150, 450, 3150, 350, -0.7853981633974483, 0.0, "n321.1-n328.1", true, true, true, PoiType.SHOP, "n328.2-n321.2", true, true, true, PoiType.CROSSROAD);
segments["293-294"] = new Segment(raphael_canvas, 2200, 800, 2100, 700, -0.7853981633974483, 0.7853981633974483, "n293.1-n294.1", true, true, true, PoiType.MALL, "n294.2-n293.2", true, true, true, PoiType.BANK);
segments["179-182"] = new Segment(raphael_canvas, 1050, 1950, 1150, 1950, 0.0, 0.0, "n179.2-n182.1", true, true, true, PoiType.CROSSROAD, "n182.2-n179.1", true, true, true, PoiType.MALL);
segments["281-289"] = new Segment(raphael_canvas, 2600, 100, 2600, 250, 0.7853981633974483, 0.0, "n281.1-n289.1", true, true, true, PoiType.AMUSEMENT_PARK, "n289.2-n281.2", true, true, true, PoiType.MALL);
segments["177-180"] = new Segment(raphael_canvas, 1000, 1850, 1000, 1900, 0.0, 0.0, "n177.1-n180.1", true, true, true, PoiType.POST, "n180.2-n177.2", true, true, true, PoiType.CROSSROAD);
segments["412-413"] = new Segment(raphael_canvas, 3450, 600, 3550, 700, -0.7853981633974483, 0.7853981633974483, "n412.1-n413.1", true, true, true, PoiType.PUB, "n413.2-n412.2", true, true, true, PoiType.PUB);
segments["17-23"] = new Segment(raphael_canvas, 400, 200, 500, 200, 0.0, 0.0, "n17.1-n23.1", true, true, true, PoiType.POST, "n23.2-n17.2", true, true, true, PoiType.POST);
segments["347-381"] = new Segment(raphael_canvas, 2600, 1650, 2550, 1650, 0.0, 0.0, "n381.2-n347.1", true, true, true, PoiType.AMUSEMENT_PARK, "n347.2-n381.1", true, true, true, PoiType.CROSSROAD);
segments["318-319"] = new Segment(raphael_canvas, 2800, 550, 2900, 550, 0.0, 0.0, "n318.1-n319.1", true, true, true, PoiType.PUB, "n319.2-n318.2", true, true, true, PoiType.SHOP);
segments["363-365"] = new Segment(raphael_canvas, 2100, 1950, 2100, 2000, 0.0, 0.0, "n363.1-n365.1", true, true, true, PoiType.SPORT_CENTER, "n365.2-n363.2", true, true, true, PoiType.CROSSROAD);
segments["211-250"] = new Segment(raphael_canvas, 1750, 1000, 1800, 1000, 0.0, 0.0, "n211.2-n250.1", true, true, true, PoiType.CROSSROAD, "n250.2-n211.1", true, true, true, PoiType.PUB);
segments["337-472"] = new Segment(raphael_canvas, 2600, 900, 2600, 1100, 0.0, 0.7853981633974483, "n337.1-n472.1", true, true, true, PoiType.MALL, "n472.2-n337.2", true, true, true, PoiType.SPORT_CENTER);
segments["229-230"] = new Segment(raphael_canvas, 1300, 1350, 1400, 1450, 0.0, -0.7853981633974483, "n229.1-n230.1", true, true, true, PoiType.PUB, "n230.2-n229.2", true, true, true, PoiType.POST);
segments["171-172"] = new Segment(raphael_canvas, 1000, 1650, 1000, 1750, 0.7853981633974483, 0.0, "n171.1-n172.1", true, true, true, PoiType.UNIVERSITY, "n172.2-n171.2", true, true, true, PoiType.SHOP);
segments["124-125"] = new Segment(raphael_canvas, 200, 1200, 250, 1250, -0.7853981633974483, 0.7853981633974483, "n124.1-n125.1", true, true, true, PoiType.POST, "n125.2-n124.2", true, true, true, PoiType.MALL);
segments["390-393"] = new Segment(raphael_canvas, 3450, 250, 3450, 150, 0.0, -0.7853981633974483, "n390.2-n393.1", true, true, true, PoiType.CROSSROAD, "n393.2-n390.1", true, true, true, PoiType.PUB);
segments["411-412"] = new Segment(raphael_canvas, 3450, 500, 3450, 600, 0.0, -0.7853981633974483, "n411.1-n412.1", true, true, true, PoiType.AMUSEMENT_PARK, "n412.2-n411.2", true, true, true, PoiType.SPORT_CENTER);
segments["327-330"] = new Segment(raphael_canvas, 3100, 300, 2950, 300, 0.0, 0.0, "n327.2-n330.1", true, true, true, PoiType.CROSSROAD, "n330.2-n327.1", true, true, true, PoiType.POST);
segments["162-164"] = new Segment(raphael_canvas, 900, 1300, 900, 1350, 0.0, 0.0, "n162.2-n164.1", true, true, true, PoiType.CROSSROAD, "n164.2-n162.1", true, true, true, PoiType.MALL);
segments["386-387"] = new Segment(raphael_canvas, 2800, 1650, 2700, 1650, -0.7853981633974483, 0.0, "n387.2-n386.2", true, true, true, PoiType.PARKING, "n386.1-n387.1", true, true, true, PoiType.POST);
segments["169-170"] = new Segment(raphael_canvas, 900, 1450, 900, 1550, 0.0, -0.7853981633974483, "n169.1-n170.1", true, true, true, PoiType.SHOP, "n170.2-n169.2", true, true, true, PoiType.SHOP);
segments["73-77"] = new Segment(raphael_canvas, 850, 450, 850, 400, 0.0, 0.0, "n73.2-n77.1", true, true, true, PoiType.CROSSROAD, "n77.2-n73.1", true, true, true, PoiType.UNIVERSITY);
segments["51-52"] = new Segment(raphael_canvas, 400, 800, 500, 800, 0.0, 0.0, "n51.1-n52.1", true, true, true, PoiType.UNIVERSITY, "n52.2-n51.2", true, true, true, PoiType.BANK);
segments["279-280"] = new Segment(raphael_canvas, 2450, 50, 2550, 50, 0.7853981633974483, 0.7853981633974483, "n279.1-n280.1", true, true, true, PoiType.AMUSEMENT_PARK, "n280.2-n279.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["437-438"] = new Segment(raphael_canvas, 3350, 1300, 3300, 1300, 0.0, -0.7853981633974483, "n437.1-n438.1", true, true, true, PoiType.AMUSEMENT_PARK, "n438.2-n437.2", true, true, true, PoiType.UNIVERSITY);
segments["182-183"] = new Segment(raphael_canvas, 1150, 1950, 1250, 1950, 0.0, 0.0, "n182.1-n183.1", true, true, true, PoiType.PUB, "n183.2-n182.2", true, true, true, PoiType.UNIVERSITY);
segments["159-160"] = new Segment(raphael_canvas, 650, 1350, 750, 1250, 0.7853981633974483, 0.7853981633974483, "n159.1-n160.1", true, true, true, PoiType.PUB, "n160.2-n159.2", true, true, true, PoiType.HOME);
segments["163-222"] = new Segment(raphael_canvas, 950, 1250, 1000, 1250, 0.0, 0.0, "n163.2-n222.1", true, true, true, PoiType.CROSSROAD, "n222.2-n163.1", true, true, true, PoiType.AMUSEMENT_PARK);
segments["320-321"] = new Segment(raphael_canvas, 3050, 550, 3150, 450, -0.7853981633974483, -0.7853981633974483, "n320.1-n321.1", true, true, true, PoiType.BANK, "n321.2-n320.2", true, true, true, PoiType.SHOP);
segments["11-34"] = new Segment(raphael_canvas, 200, 400, 200, 500, 0.0, 0.0, "n11.1-n34.1", true, true, true, PoiType.MALL, "n34.2-n11.2", true, true, true, PoiType.HOME);
segments["34-35"] = new Segment(raphael_canvas, 200, 500, 200, 600, 0.0, 0.0, "n34.1-n35.1", true, true, true, PoiType.HOME, "n35.2-n34.2", true, true, true, PoiType.SPORT_CENTER);
segments["2-6"] = new Segment(raphael_canvas, 200, 250, 200, 300, 0.0, 0.0, "n2.2-n6.1", true, true, true, PoiType.CROSSROAD, "n6.2-n2.1", true, true, true, PoiType.UNIVERSITY);
segments["357-358"] = new Segment(raphael_canvas, 2100, 1750, 2100, 1850, -0.7853981633974483, 0.0, "n357.1-n358.1", true, true, true, PoiType.UNIVERSITY, "n358.2-n357.2", true, true, true, PoiType.SHOP);
segments["319-320"] = new Segment(raphael_canvas, 2900, 550, 3050, 550, 0.0, -0.7853981633974483, "n319.1-n320.1", true, true, true, PoiType.POST, "n320.2-n319.2", true, true, true, PoiType.POST);
segments["430-431"] = new Segment(raphael_canvas, 3200, 1500, 3300, 1600, -0.7853981633974483, -0.7853981633974483, "n430.1-n431.1", true, true, true, PoiType.AMUSEMENT_PARK, "n431.2-n430.2", true, true, true, PoiType.POST);
segments["288-300"] = new Segment(raphael_canvas, 2600, 450, 2600, 500, 0.0, 0.0, "n288.1-n300.1", true, true, true, PoiType.SHOP, "n300.2-n288.2", true, true, true, PoiType.CROSSROAD);
segments["61-62"] = new Segment(raphael_canvas, 850, 750, 850, 700, -0.7853981633974483, 0.0, "n61.1-n62.1", true, true, true, PoiType.POST, "n62.2-n61.2", true, true, true, PoiType.POST);
segments["4-5"] = new Segment(raphael_canvas, 200, 50, 200, 150, 0.0, 0.0, "n5.2-n4.1", true, true, true, PoiType.SHOP, "n4.2-n5.1", true, true, true, PoiType.CROSSROAD);
segments["346-348"] = new Segment(raphael_canvas, 2500, 1550, 2500, 1600, 0.0, 0.0, "n346.1-n348.1", true, true, true, PoiType.PUB, "n348.2-n346.2", true, true, true, PoiType.CROSSROAD);
segments["70-473"] = new Segment(raphael_canvas, 700, 500, 800, 500, -0.7853981633974483, 0.0, "n473.1-n70.1", true, true, true, PoiType.SPORT_CENTER, "n70.2-n473.2", true, true, true, PoiType.CROSSROAD);
segments["87-90"] = new Segment(raphael_canvas, 900, 200, 950, 200, 0.0, 0.0, "n87.2-n90.1", true, true, true, PoiType.CROSSROAD, "n90.2-n87.1", true, true, true, PoiType.POST);
segments["213-221"] = new Segment(raphael_canvas, 1350, 1000, 1500, 1000, -0.7853981633974483, 0.0, "n221.1-n213.1", true, true, true, PoiType.AMUSEMENT_PARK, "n213.2-n221.2", true, true, true, PoiType.UNIVERSITY);
segments["416-417"] = new Segment(raphael_canvas, 3550, 1000, 3550, 1100, 0.0, 0.0, "n416.1-n417.1", true, true, true, PoiType.MALL, "n417.2-n416.2", true, true, true, PoiType.BANK);
segments["158-159"] = new Segment(raphael_canvas, 650, 1450, 650, 1350, -0.7853981633974483, 0.7853981633974483, "n158.1-n159.1", true, true, true, PoiType.SPORT_CENTER, "n159.2-n158.2", true, true, true, PoiType.MALL);
segments["24-27"] = new Segment(raphael_canvas, 650, 50, 650, 150, 0.0, 0.0, "n27.2-n24.1", true, true, true, PoiType.AMUSEMENT_PARK, "n24.2-n27.1", true, true, true, PoiType.CROSSROAD);
segments["23-25"] = new Segment(raphael_canvas, 500, 200, 600, 200, 0.0, 0.0, "n23.1-n25.1", true, true, true, PoiType.BANK, "n25.2-n23.2", true, true, true, PoiType.CROSSROAD);
segments["26-28"] = new Segment(raphael_canvas, 650, 250, 650, 300, 0.0, 0.0, "n26.2-n28.1", true, true, true, PoiType.CROSSROAD, "n28.2-n26.1", true, true, true, PoiType.SPORT_CENTER);
segments["232-238"] = new Segment(raphael_canvas, 1700, 1350, 1700, 1200, -0.7853981633974483, 0.0, "n232.1-n238.1", true, true, true, PoiType.POST, "n238.2-n232.2", true, true, true, PoiType.MALL);
segments["157-158"] = new Segment(raphael_canvas, 550, 1550, 650, 1450, -0.7853981633974483, -0.7853981633974483, "n157.1-n158.1", true, true, true, PoiType.SHOP, "n158.2-n157.2", true, true, true, PoiType.PUB);
segments["458-459"] = new Segment(raphael_canvas, 3500, 1900, 3300, 1900, 0.7853981633974483, -5.497787143782138, "n458.1-n459.1", true, true, true, PoiType.PUB, "n459.2-n458.2", true, true, true, PoiType.MALL);
segments["312-337"] = new Segment(raphael_canvas, 2600, 750, 2600, 900, 0.0, 0.0, "n312.1-n337.1", true, true, true, PoiType.POST, "n337.2-n312.2", true, true, true, PoiType.POST);
segments["228-229"] = new Segment(raphael_canvas, 1200, 1250, 1300, 1350, 0.7853981633974483, 0.0, "n228.1-n229.1", true, true, true, PoiType.PUB, "n229.2-n228.2", true, true, true, PoiType.PUB);
segments["257-258"] = new Segment(raphael_canvas, 2050, 500, 2050, 400, -0.7853981633974483, 0.0, "n257.1-n258.1", true, true, true, PoiType.SPORT_CENTER, "n258.2-n257.2", true, true, true, PoiType.UNIVERSITY);
segments["402-403"] = new Segment(raphael_canvas, 2950, 50, 2850, 150, -0.7853981633974483, 0.0, "n402.1-n403.1", true, true, true, PoiType.MALL, "n403.2-n402.2", true, true, true, PoiType.POST);
segments["356-357"] = new Segment(raphael_canvas, 2200, 1650, 2100, 1750, -0.7853981633974483, -0.7853981633974483, "n356.1-n357.1", true, true, true, PoiType.UNIVERSITY, "n357.2-n356.2", true, true, true, PoiType.SPORT_CENTER);
segments["459-461"] = new Segment(raphael_canvas, 3300, 1900, 3250, 1850, -5.497787143782138, 0.0, "n459.1-n461.1", true, true, true, PoiType.SHOP, "n461.2-n459.2", true, true, true, PoiType.MALL);
segments["394-395"] = new Segment(raphael_canvas, 3350, 50, 3250, 50, 5.497787143782138, 0.0, "n394.1-n395.1", true, true, true, PoiType.HOME, "n395.2-n394.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["255-290"] = new Segment(raphael_canvas, 1900, 1000, 2000, 1000, 0.0, 0.0, "n255.1-n290.1", true, true, true, PoiType.BANK, "n290.2-n255.2", true, true, true, PoiType.PUB);
segments["429-438"] = new Segment(raphael_canvas, 3300, 1300, 3200, 1400, -0.7853981633974483, -0.7853981633974483, "n438.1-n429.1", true, true, true, PoiType.PUB, "n429.2-n438.2", true, true, true, PoiType.PUB);
segments["43-117"] = new Segment(raphael_canvas, 200, 850, 200, 900, 0.0, 0.0, "n43.2-n117.1", true, true, true, PoiType.CROSSROAD, "n117.2-n43.1", true, true, true, PoiType.AMUSEMENT_PARK);
segments["298-307"] = new Segment(raphael_canvas, 2600, 600, 2600, 650, 0.0, 0.0, "n298.2-n307.1", true, true, true, PoiType.CROSSROAD, "n307.2-n298.1", true, true, true, PoiType.MALL);
segments["96-97"] = new Segment(raphael_canvas, 1150, 200, 1250, 300, 0.7853981633974483, -0.7853981633974483, "n96.1-n97.1", true, true, true, PoiType.POST, "n97.2-n96.2", true, true, true, PoiType.HOME);
segments["299-313"] = new Segment(raphael_canvas, 2650, 550, 2700, 550, 0.0, 0.0, "n299.2-n313.1", true, true, true, PoiType.CROSSROAD, "n313.2-n299.1", true, true, true, PoiType.SHOP);
segments["431-446"] = new Segment(raphael_canvas, 3300, 1600, 3400, 1600, -0.7853981633974483, 0.0, "n431.1-n446.1", true, true, true, PoiType.POST, "n446.2-n431.2", true, true, true, PoiType.BANK);
segments["95-96"] = new Segment(raphael_canvas, 1050, 200, 1150, 200, 0.0, 0.7853981633974483, "n95.1-n96.1", true, true, true, PoiType.BANK, "n96.2-n95.2", true, true, true, PoiType.UNIVERSITY);
segments["3-12"] = new Segment(raphael_canvas, 250, 200, 300, 200, 0.0, 0.0, "n3.2-n12.1", true, true, true, PoiType.CROSSROAD, "n12.2-n3.1", true, true, true, PoiType.POST);
segments["82-86"] = new Segment(raphael_canvas, 850, 300, 850, 250, 0.0, 0.0, "n82.1-n86.1", true, true, true, PoiType.UNIVERSITY, "n86.2-n82.2", true, true, true, PoiType.CROSSROAD);
segments["109-244"] = new Segment(raphael_canvas, 1700, 600, 1750, 600, 0.0, 0.0, "n109.2-n244.1", true, true, true, PoiType.CROSSROAD, "n244.2-n109.1", true, true, true, PoiType.AMUSEMENT_PARK);
segments["103-474"] = new Segment(raphael_canvas, 1450, 300, 1550, 300, 0.0, 0.7853981633974483, "n103.1-n474.1", true, true, true, PoiType.AMUSEMENT_PARK, "n474.2-n103.2", true, true, true, PoiType.SHOP);
segments["202-203"] = new Segment(raphael_canvas, 1050, 500, 1150, 500, 0.0, 0.7853981633974483, "n202.1-n203.1", true, true, true, PoiType.SPORT_CENTER, "n203.2-n202.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["205-219"] = new Segment(raphael_canvas, 1250, 600, 1250, 750, 0.7853981633974483, 0.0, "n205.1-n219.1", true, true, true, PoiType.POST, "n219.2-n205.2", true, true, true, PoiType.HOME);
segments["145-184"] = new Segment(raphael_canvas, 250, 1700, 250, 1800, 0.0, -0.7853981633974483, "n145.1-n184.1", true, true, true, PoiType.UNIVERSITY, "n184.2-n145.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["227-228"] = new Segment(raphael_canvas, 1100, 1250, 1200, 1250, 0.0, 0.7853981633974483, "n227.1-n228.1", true, true, true, PoiType.POST, "n228.2-n227.2", true, true, true, PoiType.UNIVERSITY);
segments["451-453"] = new Segment(raphael_canvas, 3500, 1600, 3550, 1600, 0.0, 0.0, "n451.1-n453.1", true, true, true, PoiType.SHOP, "n453.2-n451.2", true, true, true, PoiType.CROSSROAD);
segments["185-186"] = new Segment(raphael_canvas, 350, 1900, 450, 1900, -0.7853981633974483, 0.0, "n185.1-n186.1", true, true, true, PoiType.MALL, "n186.2-n185.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["457-458"] = new Segment(raphael_canvas, 3600, 1800, 3500, 1900, 0.7853981633974483, 0.7853981633974483, "n457.1-n458.1", true, true, true, PoiType.SHOP, "n458.2-n457.2", true, true, true, PoiType.PUB);
new Crossing(raphael_canvas, 850, 700, 850, 600, 0.0, 0.0, "n62.1", "n64.1", "n65.1", "n66.1", "n67.1", PoiType.POST, true, true, "n67.2", "n65.2", "n64.2", "n63.2", "n62.2", PoiType.POST, true, true)
roundabouts["n364.1"] = new Roundabout(raphael_canvas, "n364.1", 2100, 2050, 50, 3, 3.141592653589793);
new Crossing(raphael_canvas, 3400, 1600, 3500, 1600, 0.0, 0.0, "n446.1", "n448.1", "n449.1", "n450.1", "n451.1", PoiType.SHOP, true, true, "n451.2", "n449.2", "n448.2", "n447.2", "n446.2", PoiType.SHOP, true, true)
new Crossing(raphael_canvas, 1350, 300, 1450, 300, 0.0, 0.0, "n98.1", "n100.1", "n101.1", "n102.1", "n103.1", PoiType.SHOP, true, true, "n103.2", "n101.2", "n100.2", "n99.2", "n98.2", PoiType.SHOP, true, true)
roundabouts["n161.1"] = new Roundabout(raphael_canvas, "n161.1", 900, 1250, 50, 3, 0.0);
roundabouts["n24.1"] = new Roundabout(raphael_canvas, "n24.1", 650, 200, 50, 3, 1.5707963267948966);
roundabouts["n142.1"] = new Roundabout(raphael_canvas, "n142.1", 250, 1550, 50, 3, -1.5707963267948966);
roundabouts["n86.1"] = new Roundabout(raphael_canvas, "n86.1", 850, 200, 50, 3, -1.5707963267948966);
roundabouts["n347.1"] = new Roundabout(raphael_canvas, "n347.1", 2500, 1650, 50, 3, 3.141592653589793);
roundabouts["n297.1"] = new Roundabout(raphael_canvas, "n297.1", 2600, 550, 50, 4, 0.0);
new Crossing(raphael_canvas, 350, 1550, 450, 1550, 0.0, 0.0, "n146.1", "n148.1", "n149.1", "n150.1", "n151.1", PoiType.POST, true, true, "n151.2", "n149.2", "n148.2", "n147.2", "n146.2", PoiType.POST, true, true)
new Crossing(raphael_canvas, 2600, 350, 2600, 450, 0.0, 0.0, "n283.1", "n285.1", "n286.1", "n287.1", "n288.1", PoiType.PUB, true, true, "n288.2", "n286.2", "n285.2", "n284.2", "n283.2", PoiType.PUB, true, true)
new Crossing(raphael_canvas, 2600, 650, 2600, 750, 0.0, 0.0, "n307.1", "n309.1", "n310.1", "n311.1", "n312.1", PoiType.UNIVERSITY, true, true, "n312.2", "n310.2", "n309.2", "n308.2", "n307.2", PoiType.UNIVERSITY, true, true)
new Crossing(raphael_canvas, 850, 400, 850, 300, 0.0, 0.0, "n77.1", "n79.1", "n80.1", "n81.1", "n82.1", PoiType.UNIVERSITY, true, true, "n82.2", "n80.2", "n79.2", "n78.2", "n77.2", PoiType.UNIVERSITY, true, true)
new Crossing(raphael_canvas, 250, 1350, 250, 1450, 0.0, 0.0, "n136.1", "n138.1", "n139.1", "n140.1", "n141.1", PoiType.SPORT_CENTER, true, true, "n141.2", "n139.2", "n138.2", "n137.2", "n136.2", PoiType.SPORT_CENTER, true, true)
new Crossing(raphael_canvas, 1500, 1000, 1600, 1000, 0.0, 0.0, "n213.1", "n215.1", "n216.1", "n217.1", "n218.1", PoiType.HOME, true, true, "n218.2", "n216.2", "n215.2", "n214.2", "n213.2", PoiType.HOME, true, true)
new Crossing(raphael_canvas, 3450, 1300, 3350, 1300, 0.0, 0.0, "n432.1", "n434.1", "n435.1", "n436.1", "n437.1", PoiType.SPORT_CENTER, true, true, "n437.2", "n435.2", "n434.2", "n433.2", "n432.2", PoiType.SPORT_CENTER, true, true)
new Crossing(raphael_canvas, 2600, 1650, 2700, 1650, 0.0, 0.0, "n381.1", "n383.1", "n384.1", "n385.1", "n386.1", PoiType.SPORT_CENTER, true, true, "n386.2", "n384.2", "n383.2", "n382.2", "n381.2", PoiType.SPORT_CENTER, true, true)
new Crossing(raphael_canvas, 2400, 550, 2500, 550, 0.0, 0.0, "n301.1", "n303.1", "n304.1", "n305.1", "n306.1", PoiType.SPORT_CENTER, true, true, "n306.2", "n304.2", "n303.2", "n302.2", "n301.2", PoiType.SPORT_CENTER, true, true)
roundabouts["n209.1"] = new Roundabout(raphael_canvas, "n209.1", 1700, 1000, 50, 4, 0.0);
new Crossing(raphael_canvas, 200, 900, 200, 1000, 0.0, 0.0, "n117.1", "n119.1", "n120.1", "n121.1", "n122.1", PoiType.SPORT_CENTER, true, true, "n122.2", "n120.2", "n119.2", "n118.2", "n117.2", PoiType.SPORT_CENTER, true, true)
roundabouts["n452.1"] = new Roundabout(raphael_canvas, "n452.1", 3600, 1600, 50, 3, 1.5707963267948966);
new Crossing(raphael_canvas, 1000, 1250, 1100, 1250, 0.0, 0.0, "n222.1", "n224.1", "n225.1", "n226.1", "n227.1", PoiType.AMUSEMENT_PARK, true, true, "n227.2", "n225.2", "n224.2", "n223.2", "n222.2", PoiType.AMUSEMENT_PARK, true, true)
new Crossing(raphael_canvas, 2100, 1850, 2100, 1950, 0.0, 0.0, "n358.1", "n360.1", "n361.1", "n362.1", "n363.1", PoiType.SPORT_CENTER, true, true, "n363.2", "n361.2", "n360.2", "n359.2", "n358.2", PoiType.SPORT_CENTER, true, true)
new Crossing(raphael_canvas, 200, 600, 200, 700, 0.0, 0.0, "n35.1", "n37.1", "n38.1", "n39.1", "n40.1", PoiType.PUB, true, true, "n40.2", "n38.2", "n37.2", "n36.2", "n35.2", PoiType.PUB, true, true)
new Crossing(raphael_canvas, 1750, 600, 1850, 600, 0.0, 0.0, "n244.1", "n246.1", "n247.1", "n248.1", "n249.1", PoiType.UNIVERSITY, true, true, "n249.2", "n247.2", "n246.2", "n245.2", "n244.2", PoiType.UNIVERSITY, true, true)
new Crossing(raphael_canvas, 2150, 150, 2250, 150, 0.0, 0.0, "n272.1", "n274.1", "n275.1", "n276.1", "n277.1", PoiType.BANK, true, true, "n277.2", "n275.2", "n274.2", "n273.2", "n272.2", PoiType.BANK, true, true)
new Crossing(raphael_canvas, 300, 200, 400, 200, 0.0, 0.0, "n12.1", "n14.1", "n15.1", "n16.1", "n17.1", PoiType.MALL, true, true, "n17.2", "n15.2", "n14.2", "n13.2", "n12.2", PoiType.MALL, true, true)
new Crossing(raphael_canvas, 950, 500, 1050, 500, 0.0, 0.0, "n197.1", "n199.1", "n200.1", "n201.1", "n202.1", PoiType.SPORT_CENTER, true, true, "n202.2", "n200.2", "n199.2", "n198.2", "n197.2", PoiType.SPORT_CENTER, true, true)
new Crossing(raphael_canvas, 3550, 1100, 3550, 1200, 0.0, 0.0, "n417.1", "n419.1", "n420.1", "n421.1", "n422.1", PoiType.PUB, true, true, "n422.2", "n420.2", "n419.2", "n418.2", "n417.2", PoiType.PUB, true, true)
roundabouts["n327.1"] = new Roundabout(raphael_canvas, "n327.1", 3150, 300, 50, 3, 0.0);
new Crossing(raphael_canvas, 2400, 1650, 2300, 1650, 0.0, 0.0, "n350.1", "n352.1", "n353.1", "n354.1", "n355.1", PoiType.AMUSEMENT_PARK, true, true, "n355.2", "n353.2", "n352.2", "n351.2", "n350.2", PoiType.AMUSEMENT_PARK, true, true)
roundabouts["n266.1"] = new Roundabout(raphael_canvas, "n266.1", 2050, 150, 50, 3, 0.0);
new Crossing(raphael_canvas, 2050, 400, 2050, 300, 0.0, 0.0, "n258.1", "n260.1", "n261.1", "n262.1", "n263.1", PoiType.POST, true, true, "n263.2", "n261.2", "n260.2", "n259.2", "n258.2", PoiType.POST, true, true)
new Crossing(raphael_canvas, 3450, 400, 3450, 500, 0.0, 0.0, "n406.1", "n408.1", "n409.1", "n410.1", "n411.1", PoiType.AMUSEMENT_PARK, true, true, "n411.2", "n409.2", "n408.2", "n407.2", "n406.2", PoiType.AMUSEMENT_PARK, true, true)
new Crossing(raphael_canvas, 450, 1900, 550, 1900, 0.0, 0.0, "n186.1", "n188.1", "n189.1", "n190.1", "n191.1", PoiType.MALL, true, true, "n191.2", "n189.2", "n188.2", "n187.2", "n186.2", PoiType.MALL, true, true)
new Crossing(raphael_canvas, 3250, 1850, 3150, 1750, 0.0, 0.0, "n461.1", "n463.1", "n464.1", "n465.1", "n466.1", PoiType.MALL, true, true, "n466.2", "n464.2", "n463.2", "n462.2", "n461.2", PoiType.MALL, true, true)
new Crossing(raphael_canvas, 2200, 2050, 2300, 2050, 0.0, 0.0, "n371.1", "n373.1", "n374.1", "n375.1", "n376.1", PoiType.AMUSEMENT_PARK, true, true, "n376.2", "n374.2", "n373.2", "n372.2", "n371.2", PoiType.AMUSEMENT_PARK, true, true)
new Crossing(raphael_canvas, 900, 1350, 900, 1450, 0.0, 0.0, "n164.1", "n166.1", "n167.1", "n168.1", "n169.1", PoiType.PUB, true, true, "n169.2", "n167.2", "n166.2", "n165.2", "n164.2", PoiType.PUB, true, true)
new Crossing(raphael_canvas, 950, 200, 1050, 200, 0.0, 0.0, "n90.1", "n92.1", "n93.1", "n94.1", "n95.1", PoiType.MALL, true, true, "n95.2", "n93.2", "n92.2", "n91.2", "n90.2", PoiType.MALL, true, true)
roundabouts["n423.1"] = new Roundabout(raphael_canvas, "n423.1", 3550, 1300, 50, 3, 3.141592653589793);
roundabouts["n108.1"] = new Roundabout(raphael_canvas, "n108.1", 1650, 600, 50, 3, -1.5707963267948966);
new Crossing(raphael_canvas, 2700, 550, 2800, 550, 0.0, 0.0, "n313.1", "n315.1", "n316.1", "n317.1", "n318.1", PoiType.SHOP, true, true, "n318.2", "n316.2", "n315.2", "n314.2", "n313.2", PoiType.SHOP, true, true)
new Crossing(raphael_canvas, 200, 300, 200, 400, 0.0, 0.0, "n6.1", "n8.1", "n9.1", "n10.1", "n11.1", PoiType.BANK, true, true, "n11.2", "n9.2", "n8.2", "n7.2", "n6.2", PoiType.BANK, true, true)
new Crossing(raphael_canvas, 3250, 300, 3350, 300, 0.0, 0.0, "n331.1", "n333.1", "n334.1", "n335.1", "n336.1", PoiType.HOME, true, true, "n336.2", "n334.2", "n333.2", "n332.2", "n331.2", PoiType.HOME, true, true)
roundabouts["n1.1"] = new Roundabout(raphael_canvas, "n1.1", 200, 200, 50, 4, 0.0);
new Crossing(raphael_canvas, 3250, 50, 3150, 50, 0.0, 0.0, "n395.1", "n397.1", "n398.1", "n399.1", "n400.1", PoiType.AMUSEMENT_PARK, true, true, "n400.2", "n398.2", "n397.2", "n396.2", "n395.2", PoiType.AMUSEMENT_PARK, true, true)
new Crossing(raphael_canvas, 2500, 1450, 2500, 1550, 0.0, 0.0, "n341.1", "n343.1", "n344.1", "n345.1", "n346.1", PoiType.POST, true, true, "n346.2", "n344.2", "n343.2", "n342.2", "n341.2", PoiType.POST, true, true)
new Crossing(raphael_canvas, 650, 300, 650, 400, 0.0, 0.0, "n28.1", "n30.1", "n31.1", "n32.1", "n33.1", PoiType.BANK, true, true, "n33.2", "n31.2", "n30.2", "n29.2", "n28.2", PoiType.BANK, true, true)
roundabouts["n70.1"] = new Roundabout(raphael_canvas, "n70.1", 850, 500, 50, 4, 0.0);
new Crossing(raphael_canvas, 300, 800, 400, 800, 0.0, 0.0, "n46.1", "n48.1", "n49.1", "n50.1", "n51.1", PoiType.MALL, true, true, "n51.2", "n49.2", "n48.2", "n47.2", "n46.2", PoiType.MALL, true, true)
new Crossing(raphael_canvas, 1800, 1000, 1900, 1000, 0.0, 0.0, "n250.1", "n252.1", "n253.1", "n254.1", "n255.1", PoiType.SPORT_CENTER, true, true, "n255.2", "n253.2", "n252.2", "n251.2", "n250.2", PoiType.SPORT_CENTER, true, true)
new Crossing(raphael_canvas, 1000, 1750, 1000, 1850, 0.0, 0.0, "n172.1", "n174.1", "n175.1", "n176.1", "n177.1", PoiType.SHOP, true, true, "n177.2", "n175.2", "n174.2", "n173.2", "n172.2", PoiType.SHOP, true, true)
roundabouts["n178.1"] = new Roundabout(raphael_canvas, "n178.1", 1000, 1950, 50, 3, -1.5707963267948966);
new Crossing(raphael_canvas, 1650, 700, 1650, 800, 0.0, 0.0, "n111.1", "n113.1", "n114.1", "n115.1", "n116.1", PoiType.SHOP, true, true, "n116.2", "n114.2", "n113.2", "n112.2", "n111.2", PoiType.SHOP, true, true)
roundabouts["n390.1"] = new Roundabout(raphael_canvas, "n390.1", 3450, 300, 50, 3, 1.5707963267948966);
roundabouts["n43.1"] = new Roundabout(raphael_canvas, "n43.1", 200, 800, 50, 3, -1.5707963267948966);
new Crossing(raphael_canvas, 1700, 1200, 1700, 1100, 0.0, 0.0, "n238.1", "n240.1", "n241.1", "n242.1", "n243.1", PoiType.MALL, true, true, "n243.2", "n241.2", "n240.2", "n239.2", "n238.2", PoiType.MALL, true, true)
districts["dist1"] = GraphicUtils.getScaledBBox(0, 0, 1196, 1150);
districts["dist4"] = GraphicUtils.getScaledBBox(2796, -5, 809, 905);
districts["dist5"] = GraphicUtils.getScaledBBox(1695, 1300, 1210, 805);
districts["dist2"] = GraphicUtils.getScaledBBox(146, 1146, 1154, 1009);
districts["dist3"] = GraphicUtils.getScaledBBox(1195, -5, 1755, 1510);
districts["dist6"] = GraphicUtils.getScaledBBox(2895, 950, 855, 1005);