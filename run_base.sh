CURDIR=$(pwd)
cd $(dirname $0)
BASEDIR=$(pwd)
cd $CURDIR

CONFIG_PATH=${BASEDIR}/config
SRC_PATH=${BASEDIR}/src
BUILD_PATH=${BASEDIR}/build
LIB_PATH=${BASEDIR}/lib
SRC_FILE_LIST=${BASEDIR}/jfile.list
FRONTEND_APP="web_frontend"
FRONTEND_VERSION="-1.0"
export LIB_PATH=$LIB_PATH
export BUILD_PATH=$BUILD_PATH

#start name server
gnome-terminal --title="Name Server" --command="sh $BUILD_PATH/name_server/run.sh localhost:3333"
#start controller
gnome-terminal --title="Controller" --command="sh $BUILD_PATH/controller/run.sh localhost:3333 localhost:4444"
sleep 2
chromium-browser "http://127.0.0.1:9000/" &
exit
#sleep 1
#start districts
DISTRICTS=()
BASE_PORT=5000
INC=1
for f in $CONFIG_PATH/*; do
	DIST=${f#*_}
	DIST=${DIST%_*}
	if [[ "$DIST" == *.def ]]; then
		DIST=${DIST%.*}
		DISTRICTS+=($DIST)
	fi
done

for d in ${DISTRICTS[@]}; do
	gnome-terminal --title="District $INC" --command="sh $BUILD_PATH/district/run.sh $CONFIG_PATH/city_dist${INC}.def localhost:3333 localhost:4444 localhost:$(($BASE_PORT+$INC))"
	((INC+=1))
done

#gnome-terminal --title="District 1" --command="sh $BUILD_PATH/district/run.sh $CONFIG_PATH/complex_dist1.def localhost:3333 localhost:4444 localhost:5555"
#gnome-terminal --title="District 2" --command="sh $BUILD_PATH/district/run.sh $CONFIG_PATH/complex_dist2.def localhost:3333 localhost:4444 localhost:5556"
#gnome-terminal --title="District 3" --command="sh $BUILD_PATH/district/run.sh $CONFIG_PATH/complex_dist3.def localhost:3333 localhost:4444 localhost:5557"
#gnome-terminal --title="District 4" --command="sh $BUILD_PATH/district/run.sh $CONFIG_PATH/complex_dist4.def localhost:3333 localhost:4444 localhost:5558"
