CURDIR=$(pwd)
cd $(dirname $0)
BASEDIR=$(pwd)
cd $CURDIR

CONFIG_PATH=${BASEDIR}/config
SRC_PATH=${BASEDIR}/src
BUILD_PATH=${BASEDIR}/build
LIB_PATH=${BASEDIR}/lib
SRC_FILE_LIST=${BASEDIR}/jfile.list
GUI_PATH=/home/fabio/workspace/web_frontend/target/universal/stage/bin
FRONTEND_APP="web_frontend"
export LIB_PATH=$LIB_PATH
export BUILD_PATH=$BUILD_PATH
DISTRICTS=()
BASE_PORT=5010
INC=1
for f in $CONFIG_PATH/*; do
	DIST=${f#*_}
	DIST=${DIST%_*}
	if [[ "$DIST" == *.def ]]; then
		DIST=${DIST%.*}
		DISTRICTS+=($DIST)
	fi
done

for d in ${DISTRICTS[@]}; do
	gnome-terminal --title="District $INC" -x bash -c "sh $BUILD_PATH/district/run.sh $CONFIG_PATH/city_dist${INC}.def localhost:3333 localhost:4444 localhost:$(($BASE_PORT+$INC)) > dist${INC}.log 2>&1;"
	((INC+=1))
done
