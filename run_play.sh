cd $(dirname $0)
BASEDIR=$(pwd)
cd $CURDIR

CONFIG_PATH=${BASEDIR}/config
SRC_PATH=${BASEDIR}/src
BUILD_PATH=${BASEDIR}/build
LIB_PATH=${BASEDIR}/lib
SRC_FILE_LIST=${BASEDIR}/jfile.list
FRONTEND_APP="web_frontend"
FRONTEND_VERSION="-1.0"
export LIB_PATH=$LIB_PATH
export BUILD_PATH=$BUILD_PATH

#start gui backend
gnome-terminal --title="Frontend Server" -e "bash -c \"$BUILD_PATH/$FRONTEND_APP$FRONTEND_VERSION/bin/./$FRONTEND_APP\""
