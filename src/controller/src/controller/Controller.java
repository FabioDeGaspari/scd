package controller;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.inspirel.yami.IncomingMessage;
import com.inspirel.yami.IncomingMessageCallback;
import com.inspirel.yami.Parameters;

import controller.events.SimEvent;
import controller.net.INameServer;
import controller.net.MessageBroker;
import controller.ui.UI;
import definitions.Constants;
import definitions.Services;


public class Controller implements IncomingMessageCallback {
	private static final Logger log = Logger.getLogger(Controller.class.getName());
	private static final String id = "controller";

	private Map<String, String> simNodes = new ConcurrentHashMap<>();
	private AtomicInteger districts  = new AtomicInteger(0);
	private AtomicInteger to_init = new AtomicInteger(0);
	private String resolved_target;
	private String name_server;
	private MessageBroker broker;
	private Publisher publisher;
	private Gson gson = new Gson();
	private UI ui;

	public Controller(String addr, String name_server) {
		broker = MessageBroker.instance();
		resolved_target = broker.listenOn(addr);
		this.name_server = name_server;
		broker.registerHandler(Constants.CONTROLLER, this);
		log.info("Controller running at "+resolved_target);
		publisher = new Publisher();

	}
	
	public void setUI(UI ui) {
		this.ui = ui;
	}

	@Override
	public void call(IncomingMessage msg) throws Exception {
		Parameters params = msg.getParameters();
		String node_id = params.getString("host_id");
		switch(msg.getMessageName()) {
		case Services.CONTROLLER_INIT_BEGIN:
			simNodes.put(node_id, params.getString("addr"));
			ui.addNode(node_id);
			log.info("now node "+node_id+" registered");
			publisher.publishSetup(node_id, params.getString("objs"));
			districts.incrementAndGet();
			to_init.incrementAndGet();
			break;
		case Services.CONTROLLER_INIT_COMPLETE:
			ui.nodeInitComplete(node_id);
			to_init.decrementAndGet();
			if(to_init.get()==0) {
				ui.enableStart(true);
				log.info("All nodes ready");
			}
			break;
		case Services.CONTROLLER_EVENT:
			publisher.publishEvent(params.getString("event"));
			break;
		case Services.SUBSCRIPTION_UPDATE:
			publisher.updateSubscription(msg.getSource(), node_id, params.getStringArray("interests"),
					params.getStringArray("lost_interests"), new Integer(msg.getParameters().getInteger("interest_level")));
			break;
		case Services.CONTROLLER_TERMINATION_STATUS:
			params.getString("term_stage").equals(Constants.TERMINATION_STAGE1);
			ui.setTerminated(node_id);
			if(to_init.incrementAndGet() == districts.get()) {
				System.out.println("All nodes have completed termination stage 1, completing termination");
				completeTermination();
				return;
			}
			System.out.println(to_init.get()+" nodes have completed termination stage 1");
			break;
		}
	}
	
	private Parameters getParams() {
		Parameters args = new Parameters();
		args.setString("host_id", id);
		return args;
	}
	
	public String startSimulation() {
		if(to_init.get() != 0) 
			return "Start aborted. Waiting for simulation nodes to complete initialisation";
		for(Map.Entry<String, String> e  : simNodes.entrySet()) {
			System.out.println("starting node "+e.getKey());
			MessageBroker.instance().sendOneWay(e.getValue(), Constants.DISTRICT, Services.DISTRICT_BEGIN, getParams());
		}
		return null;
	}
	
	public void beginTermination() {
		if(simNodes.size() == 0) {
			completeTermination();
			return;
		}
		for(Map.Entry<String, String> e  : simNodes.entrySet())
			MessageBroker.instance().sendOneWay(e.getValue(), Constants.DISTRICT, Services.DISTRICT_MARKER, getParams());
	}
	
	private void completeTermination() {
		publisher.sendTermination();
		for(Map.Entry<String, String> e  : simNodes.entrySet())
			MessageBroker.instance().sendOneWay(e.getValue(), Constants.DISTRICT, Services.TERMINATE, getParams());
		INameServer.terminate(name_server, id);
		ui.terminate();
		System.out.println("Simulation terminated.");
	}
}
