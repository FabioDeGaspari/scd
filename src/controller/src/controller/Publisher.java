package controller;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.gson.Gson;
import com.inspirel.yami.OptionNames;
import com.inspirel.yami.Parameters;

import controller.events.SimEvent;
import controller.events.SubscriberEvent;
import controller.net.MessageBroker;

public class Publisher {
	private static final int DISPATCHER_THREADS_NUM = 1;
	private static final int LEVEL_COARSE_PUSH_THRESHOLD = 1;

	//for each event source (district), keep a map of the current event for each object
	private Map<String, Map<String, SimEvent>> curr_events = new ConcurrentHashMap<String, Map<String, SimEvent>>();
	//for each event source (district), keep a map of the current segment traffic state
	private Map<String, Map<String, IdStatePair>> segment_state = new ConcurrentHashMap<String, Map<String, IdStatePair>>();
	private Map<String, Subscriber> subscribers = new ConcurrentHashMap<String, Subscriber>();
	private MessageBroker broker;
	private Gson gson;

	public class IdStatePair {
		public String id;
		public long state;
		public IdStatePair(String id, long state) { this.id = id; this.state = state; }
		public void updateState(int delta) { state += delta; }
	}

	private class Subscriber {
		private static final int UNSUBSCRIBE = -1;
		public static final int LEVEL_COARSE = 0;
		public static final int LEVEL_FINE = 1;
		public static final int LEVEL_FOCUS = 2;
		public String addr;
		public String id;
		public int level;
		public List<String> interests = new LinkedList<String>();
		public String focus_interest = "";

		public Subscriber(String addr, String id, int level) {
			this.addr = addr;
			this.id = id;
			this.level = level;
		}

		@Override
		public boolean equals(Object obj) {
			if(obj instanceof Subscriber)
				return id.equals(((Subscriber)obj).id);
			return false;
		}
	}

	public Publisher() {
		Parameters args = new Parameters();
		args.setInteger(OptionNames.DISPATCHER_THREADS, DISPATCHER_THREADS_NUM);
		MessageBroker.initBrokerOptions(args);
		broker = MessageBroker.instance();
		gson = new Gson();
	}

	private void unsubscribe(Subscriber subscriber, List<String> lost_interests) {
		subscriber.interests.removeAll(lost_interests);
	}

	private void subscribe(Subscriber subscriber, List<String> interests, boolean show_all) {
		subscriber.interests.addAll(interests);
		interests = show_all ? subscriber.interests : interests;
		sendEvent(getCurrentEvents(subscriber.interests, subscriber.level), subscriber);
	}
	
	private List<SubscriberEvent> getCurrentEvents(List<String> interests, int level) {
		long now = System.currentTimeMillis();
		List<SubscriberEvent> events = new LinkedList<SubscriberEvent>();
		for(String s : interests) {
			if(level == Subscriber.LEVEL_COARSE) {
				if(!segment_state.containsKey(s))
					continue;
				for(IdStatePair state : segment_state.get(s).values()){
					events.add(new SubscriberEvent(SubscriberEvent.SEGMENT_STATUS_CHANGE, state, LEVEL_COARSE_PUSH_THRESHOLD));
				}
			} else if(level == Subscriber.LEVEL_FINE) {
				if(!curr_events.containsKey(s))
					continue;
				for(Map.Entry<String, SimEvent> e : curr_events.get(s).entrySet())
					events.add(SubscriberEvent.fromSimEvent(e.getValue(), now));
			} else {
				for(Map<String, SimEvent> obj_ev : curr_events.values()) {
					if(obj_ev.containsKey(s)) {
						events.add(SubscriberEvent.fromSimEvent(obj_ev.get(s), s, now));
					}
				}
			}
		}
		return events;
	}

	public void updateSubscription(String addr, String id, String[] interests, String[] lost_interests, Integer level) {
		if(level == Subscriber.UNSUBSCRIBE) {
			System.out.println("Subscriber "+id+" removed");
			subscribers.remove(id);
			return;
		}
		if(!subscribers.containsKey(id) && interests.length != 0)
			subscribers.put(id, new Subscriber(addr, id, level));
		Subscriber subscriber = subscribers.get(id);
		boolean show_all = subscriber.level != level;
		if(level == Subscriber.LEVEL_FOCUS) {
			if(interests.length == 0)
				subscriber.focus_interest = "";
			else
				subscriber.focus_interest = interests[0];
			System.out.println("focus request for "+subscriber.focus_interest);
			sendEvent(getCurrentEvents(Arrays.asList(subscriber.focus_interest), level), subscriber);
			return;
		}
		subscriber.level = level;
		unsubscribe(subscriber, Arrays.asList(lost_interests));
		subscribe(subscriber, Arrays.asList(interests), show_all);
	}

	private Parameters eventToParam(List<SubscriberEvent> ev) {
		Parameters param = new Parameters();
		param.setString("event", gson.toJson(ev));
		return param;
	}

	private void sendEvent(SubscriberEvent event, Subscriber s) {
		if(event==null)
			return;
		List<SubscriberEvent> e = new LinkedList<SubscriberEvent>();
		e.add(event);
		sendEvent(e, s);
	}

	private void sendEvent(List<SubscriberEvent> events, Subscriber s) {
		if(events.size() == 0)
			return;
		Parameters args = eventToParam(events);
		args.setString("subscriber", s.id);
		broker.sendOneWay(s.addr, "subscriber", "event", args);
	}

	private void removeEvents(String source, List<String> ids) {
		if(!curr_events.containsKey(source))
			return;
		Map<String, SimEvent> tmp = curr_events.get(source);
		for(String id : ids)
			tmp.remove(id); 
	}

	public void publishSetup(String source, String setup) {
		SubscriberEvent event = new SubscriberEvent(SubscriberEvent.OBJECTS_SETUP, setup, "");
		for(Subscriber s : subscribers.values())
			if(s.level == Subscriber.LEVEL_FINE)
				sendEvent(event, s);
	}
	
	public void sendTermination() {
		SubscriberEvent event = new SubscriberEvent(SubscriberEvent.TERMINATE, "", "");
		for(Subscriber s : subscribers.values())
			sendEvent(event, s);
	}

	public void publishEvent(String e) {
		SimEvent event = gson.fromJson(e, SimEvent.class);
		if(event.event_type.equals(SimEvent.TYPE_DIST_LEAVE)) {
			removeEvents(event.source, event.obj_id);
			return;
		}
		event.rec_timestamp = System.currentTimeMillis();
		String seg_base = updateSegmentsState(event);
		updateEvents(event);
		SubscriberEvent coarse_ev = null;
		SubscriberEvent fine_ev = null;
		if(seg_base != null)
			coarse_ev = new SubscriberEvent(SubscriberEvent.SEGMENT_STATUS_CHANGE, segment_state.get(event.source).get(seg_base), LEVEL_COARSE_PUSH_THRESHOLD);
		fine_ev = SubscriberEvent.fromSimEvent(event, event.rec_timestamp);
		for(Subscriber s : subscribers.values()) {
			if(s.interests.contains(event.source)) {
				if(s.level == Subscriber.LEVEL_COARSE && seg_base != null)
					sendEvent(coarse_ev, s);
				else if(s.level == Subscriber.LEVEL_FINE && fine_ev !=null)
					sendEvent(fine_ev, s);
			}
			if(event.obj_id.contains(s.focus_interest)) {
				sendEvent(SubscriberEvent.fromSimEvent(event, s.focus_interest, event.rec_timestamp), s);
			}
		}
	}

	private String updateSegmentsState(SimEvent event) {
		if(event.event_type.equals(SimEvent.TYPE_ENTERS) || event.event_type.equals(SimEvent.TYPE_LEAVES)) {
			boolean notify = false;
			String seg_base;
			if(!event.additional_desc.equals(""))
				seg_base = event.additional_desc;
			else
				seg_base = event.target_id.split("-")[0];
			if(!segment_state.containsKey(event.source))
				segment_state.put(event.source, new ConcurrentHashMap<String, IdStatePair>());
			Map<String, IdStatePair> state = segment_state.get(event.source);
			//if event is a TYPE_ENTERS event, then target contains the destination segment's id
			if(!state.containsKey(seg_base))
				state.put(seg_base, new IdStatePair(event.target_id, 0));
			int old_lev = (int)(state.get(seg_base).state/LEVEL_COARSE_PUSH_THRESHOLD);
			int delta = event.obj_id.size();
			if(event.event_type.equals(SimEvent.TYPE_LEAVES))
				delta *= -1;
			state.get(seg_base).updateState(delta);
			int new_lev = (int)(state.get(seg_base).state/LEVEL_COARSE_PUSH_THRESHOLD);
			if(new_lev != old_lev)
				notify = true;
			return notify ? seg_base : null;
		}
		return null;
	}

	private void updateEvents(SimEvent event) {
		if(event.event_type.equals(SimEvent.TYPE_LEAVES))
			return;
		if(!curr_events.containsKey(event.source))
			curr_events.put(event.source, new ConcurrentHashMap<String, SimEvent>());
		for(String s : event.obj_id)
			curr_events.get(event.source).put(s, event);
	}
}
