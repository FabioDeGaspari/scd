package controller.events;

import java.util.LinkedList;
import java.util.List;

public class ObjectInfo {
	String id;
	String name;
	String type;
	
	public ObjectInfo(String id, String name, String type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public static List<ObjectInfo> fromSimEvent(SimEvent e) {
		List<ObjectInfo> res = new LinkedList<ObjectInfo>();
		for(String oid : e.obj_id)
			res.add(new ObjectInfo(oid, "", e.obj_type));
		return res;
	}
	
	public static List<ObjectInfo> fromSimEvent(SimEvent e, String focus) {
		List<ObjectInfo> res = new LinkedList<ObjectInfo>();
		for(String oid : e.obj_id)
			if(oid.equals(focus))
				res.add(new ObjectInfo(oid, "", e.obj_type));
		return res;
	}
}
