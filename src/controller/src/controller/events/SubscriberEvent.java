package controller.events;

import java.util.List;

import controller.Publisher.IdStatePair;

public class SubscriberEvent {
	public final static String TERMINATE = "terminate";
	public final static String OBJECTS_SETUP = "objs_setup";
	public final static String INIT_COMPLETE = "init_complete";
	public final static String SEGMENT_STATUS_CHANGE = "segment_status_change";
	public final static String OBJECT_MOVE_TO = "object_move_to";
	public final static String OBJECT_FOLLOW = "object_follow";
	public final static String OBJECT_WAIT = "wait";
	public static final String OBJECT_WAIT_FOR_EVENT = "wait_for_event";
	public final static String OBJECT_USES = "use";
	public final static String OBJECT_DO_STOP = "do_stop";
	public final static String FOCUS = "focus_";
	
	public String event_type;
	public List<ObjectInfo> obj_ids;
	public String target_id;
	public float progress;
	public long duration;
	public String additional_info;
	public String description;
	
	public SubscriberEvent(String event_type, List<ObjectInfo> obj_info, String target_id,
			long action_duration, long wait_duration, String info, String description, float progress) {
		this.event_type = event_type;
		this.obj_ids = obj_info;
		this.target_id = target_id;
		this.duration = action_duration;
		this.additional_info = info;
		this.description = description;
		this.progress = progress;
	}
	
	public SubscriberEvent(String event_type, List<ObjectInfo> obj_info, String target_id,
			long action_duration, String info, String desc, float progress) {
		this(event_type, obj_info, target_id, action_duration, 0, info, desc, progress);
	}
	
	public SubscriberEvent(String event_type, List<ObjectInfo> obj_info, String target_id, String desc, float prog) {
		this(event_type, obj_info, target_id, 0, 0, "", desc, prog);
	}
	
	public SubscriberEvent(String event_type, String target, String desc) {
		this(event_type, null, target, 0, 0, "", desc, 0);
	}
	
	public SubscriberEvent(String event_type, IdStatePair idStatePair, int thresh) {
		this(event_type, null, idStatePair.id, idStatePair.state/thresh, 0, "", "", 0);
	}
	
	public static SubscriberEvent fromSimEvent(SimEvent e, String focus, long now) {
		if(e.obj_id.contains(focus)) {
			SubscriberEvent event = fromSimEvent(e, now);
			event.obj_ids = ObjectInfo.fromSimEvent(e, focus);
			event.event_type = FOCUS+event.event_type;
			return event;
		}
		else
			return null;
	}

	public static SubscriberEvent fromSimEvent(SimEvent e, long now) {
		float prog = (now - e.rec_timestamp < e.duration) ? ((float)(now - e.rec_timestamp))/e.duration : 1;
		switch(e.event_type) {
		case SimEvent.TYPE_ENTERS:
			return new SubscriberEvent(OBJECT_MOVE_TO, ObjectInfo.fromSimEvent(e), e.target_id, e.duration, e.additional_desc, e.human_readable_desc, prog);
		case SimEvent.TYPE_WAIT:
			return new SubscriberEvent(OBJECT_WAIT, ObjectInfo.fromSimEvent(e), e.target_id, e.duration, e.additional_desc, e.human_readable_desc, 0);
		case SimEvent.TYPE_WAIT_FOR_EVENT:
			return new SubscriberEvent(OBJECT_WAIT_FOR_EVENT, ObjectInfo.fromSimEvent(e), e.target_id, e.duration, e.additional_desc, e.human_readable_desc, 0);
		case SimEvent.TYPE_USES:
			return new SubscriberEvent(OBJECT_USES, ObjectInfo.fromSimEvent(e), e.target_id, e.duration, e.additional_desc, e.human_readable_desc, prog);
		case SimEvent.TYPE_DO_STOP:
//			Gson gson = new Gson();
//			List<SubscriberEvent> tmp = new LinkedList<SubscriberEvent>();
//			tmp.add(fromSimEvent(gson.fromJson(e.additional_desc, SimEvent.class), now));
			return new SubscriberEvent(OBJECT_DO_STOP, ObjectInfo.fromSimEvent(e), e.target_id, e.duration, e.additional_desc, e.human_readable_desc, prog);
		case SimEvent.TYPE_LEAVES: //ignore
			return null;
		}
		return null;
	}
}
