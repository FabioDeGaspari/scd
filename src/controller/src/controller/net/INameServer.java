package controller.net;

import com.inspirel.yami.Parameters;

import definitions.Constants;
import definitions.Services;

public class INameServer {

	public static void terminate(String server_addr, String node_id) {
		Parameters args = new Parameters();
		args.setString("host_id", node_id);
		MessageBroker.instance().sendOneWay(server_addr, Constants.NAME_SERVER, Services.TERMINATE, args);
	}
}
