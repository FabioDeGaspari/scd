package controller.net;
import java.util.logging.Logger;

import com.inspirel.yami.Agent;
import com.inspirel.yami.IncomingMessageCallback;
import com.inspirel.yami.OutgoingMessage;
import com.inspirel.yami.OutgoingMessageCallback;
import com.inspirel.yami.Parameters;
import com.inspirel.yami.YAMIIOException;

import definitions.Constants;

public class MessageBroker {
	private final static boolean DEBUG = false;
	private final static Logger log = Logger.getLogger(MessageBroker.class.getName());
	private final static long MAX_TRANSMISSION_TIMEOUT = 2000;
	private static MessageBroker singleton = null;
	private static Parameters init_args= null;
	private String res_address;
	private Agent agent;

	public static MessageBroker instance() {
		if(singleton == null)
			singleton = new MessageBroker(init_args);
		return singleton;
	}
	
	public static void initBrokerOptions(Parameters args) {
		init_args = args;
	}

	/*
	 * default response checking function
	 */
	public static boolean isSuccess(Parameters args) {
		String resp = args.getString("response");
		if(resp != null && resp.equals(Constants.SUCCESS))
			return true;
		return false;
	}

	private MessageBroker(Parameters args){
		try {
			agent = new Agent(init_args);
		} catch (YAMIIOException e) {
			log.severe(e.getStackTrace().toString());
		}
	}
	
	public void registerHandler(String obj_name, IncomingMessageCallback handler) {
		agent.registerObject(obj_name, handler);
	}

	public void close() {
		agent.close();
	}

	public String listenOn(String address)
	{
		try {
			res_address = agent.addListener(address);
			return res_address;
		} catch (YAMIIOException e) {
			log.severe(e.getMessage());
			log.severe(e.getStackTrace().toString());
		}
		return null;
	}

	public Parameters send(String dst_addr, String dst_obj, String service, Parameters args) {
		try {
			OutgoingMessage msg = agent.send(dst_addr, dst_obj, service, args);
			if(!msg.waitForCompletion(MAX_TRANSMISSION_TIMEOUT)) {
				return null;
			}
			if(msg.getState() == OutgoingMessage.MessageState.REPLIED) {
				return msg.getReply();
			}
			else if (msg.getState() == OutgoingMessage.MessageState.REJECTED) {
				log.warning("Message rejected at destination: "+msg.getExceptionMsg());
			} else {
				log.warning("Channel was closed. Message abandoned");
			}
			msg.close();
		} catch (YAMIIOException e) {
			log.severe("Could not establish a connection. Message not transmitted. "+e.getMessage());
			log.severe(e.getStackTrace().toString());
		}
		return null;
	}

	public boolean sendAsync(String dst_addr, String dst_obj, String service, Parameters args, OutgoingMessageCallback callback) {
		try {
			OutgoingMessage msg;
			if(callback != null)
				msg = agent.send(callback, dst_addr, dst_obj, service, args);
			else
				msg = agent.send(dst_addr, dst_obj, service, args);
			if(!msg.waitForTransmission(MAX_TRANSMISSION_TIMEOUT)) {
				return false;
			}
			return true;
		} catch (YAMIIOException e) {
			log.severe("Could not establish a connection. Message not transmitted. "+e.getMessage());
			log.severe(e.getStackTrace().toString());
		}
		return false;
	}
	
	public void sendOneWay(String dst_addr, String dst_obj, String service, Parameters args) {
		if(DEBUG) {
			send(dst_addr, dst_obj, service, args);
			log.info("update sent to "+dst_addr+":"+dst_obj+"/"+service);
			return;
		}
		try {
			agent.sendOneWay(dst_addr, dst_obj, service, args);
		} catch (YAMIIOException e) {
			log.severe("Could not establish a connection. Message not transmitted. "+e.getMessage());
			log.severe(e.getStackTrace().toString());
		}
	}
}
