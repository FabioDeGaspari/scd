package controller.ui;

import java.awt.FlowLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DistState extends JPanel {
	private static final long serialVersionUID = 1L;
	private JLabel dist;
	private JCheckBox ready = new JCheckBox("ready");
	private JCheckBox running = new JCheckBox("running");
	private JCheckBox terminated = new JCheckBox("terminated");

	public DistState(String id) {
		this.setLayout(new FlowLayout());
		dist = new JLabel("District "+id);
		this.add(dist);
		this.add(ready);
		this.add(running);
		this.add(terminated);
	}
	
	public void setRunning(boolean flag) {
		running.setSelected(flag);
	}
	
	public void setTerminated(boolean flag) {
		terminated.setSelected(flag);
	}
	
	public void setReady(boolean flag) {
		ready.setSelected(flag);
	}
}
