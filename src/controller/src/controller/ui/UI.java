package controller.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import controller.Controller;

public class UI extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private  Controller controller;
	JPanel content;
	public JButton start_btn;
	public JButton stop_btn;
	private Map<String, DistState> nodes = new HashMap<String, DistState>();

	public static void main(String[] args) {
		if(args.length < 2) {
			System.out.println("too few arguments: <name_server_addr> <controller_addr>");
			return;
		}
		final String protocol = "tcp://";
		final Controller controller = new Controller(protocol.concat(args[1]), protocol.concat(args[0]));
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI frame = new UI(controller);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public UI(Controller controller) {
		super("Controller");
		this.controller = controller;
		controller.setUI(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		content = new JPanel();
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		scrollPane.setViewportView(content);
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		start_btn = new JButton("Start");
		panel_1.add(start_btn);
		start_btn.setEnabled(false);
		stop_btn = new JButton("Stop");
		panel_1.add(stop_btn);
		start_btn.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent arg0) {popup(UI.this.controller.startSimulation());
																											for(DistState d:nodes.values()) d.setRunning(true);}});
		stop_btn.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent arg0) {UI.this.controller.beginTermination();
																											UI.this.disableAll();}});
	}
	
	private void disableAll() {
		start_btn.setEnabled(false);
		stop_btn.setEnabled(false);
	}
	
	public void popup(String msg) {
		if(msg != null)
			JOptionPane.showMessageDialog(this, msg);
	}
	
	public void terminate() {
		JOptionPane.showMessageDialog(this, "Simulation terminated");
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}
	
	public void addNode(String node_id) {
		DistState tmp = new DistState(node_id);
		nodes.put(node_id, tmp);
		content.add(tmp);
		content.validate();
	}
	
	public void setTerminated(String node_id) {
		nodes.get(node_id).setTerminated(true);
	}
	
	public void nodeInitComplete(String node_id) {
		nodes.get(node_id).setReady(true);
	}
	
	public void enableStart(boolean flag) {
		start_btn.setEnabled(flag);
	}
	
	public void enableStop(boolean flag) {
		stop_btn.setEnabled(flag);
	}

}
