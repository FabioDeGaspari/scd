package definitions;

public final class Constants {
	// Constants Version
	public static final String VERSION = "0.0";
	
	// System Objects Constants
	public static final String NAME_SERVER = "name_server";
	public static final String DISTRICT = "district";
	public static final String ROUTER = "router";
	public static final String CONTROLLER = "controller";
	public static final String SUBSCRIBER = "subscriber";
	
	//Termination Constants
	public static final String TERMINATION_STAGE1 = "termination_stage1";
	public static final String TERMINATION_STAGE_COMPLETE = "termination_complete";
	
	//System Response Constants
	public static final String SUCCESS = "response_success";
	public static final String FAILURE = "response_failure";

	private Constants(){}
}
