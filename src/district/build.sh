SRC_FILE_LIST=jfile.list
SRC_PATH=src
CUR_DIR=${PWD##*/}  

mkdir -p $BUILD_PATH/$CUR_DIR
cp run.sh $BUILD_PATH/$CUR_DIR/
find $SRC_PATH/ -name *.java -print>$SRC_FILE_LIST
printf "javac -classpath \"%s\" -d %s %s\n" "$LIB_PATH/*" "$BUILD_PATH/$CUR_DIR" "@$SRC_FILE_LIST"
javac -classpath "$LIB_PATH/*:." -d $BUILD_PATH/$CUR_DIR @$SRC_FILE_LIST
rm -f $SRC_FILE_LIST


