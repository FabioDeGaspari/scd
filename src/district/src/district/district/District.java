package district.district;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import district.definitions.Constants;
import district.events.EventManager;
import district.events.SimEvent;
import district.graph.Graph;
import district.net.IController;
import district.net.IDistrict;
import district.net.INameServer;
import district.net.MessageBroker;
import district.objects.ObjectDescriptor;
import district.routing.Router;
import district.segment.ISegment;
import district.segment.RemoteSegment;
import district.segment.Roundabout;
import district.segment.Segment;

public class District {
	private static final Logger log = Logger.getLogger(District.class.getName());
	protected String id;
	protected String this_address;
	protected String name_server_addr;
	protected String cont_addr;
	protected Map<String, Segment> segments = new HashMap<String, Segment>();
	protected Graph graph;
	protected Router router;
	protected EventManager ev_manager;
	protected DistrictHandler handler = new DistrictHandler(this);

	public District(String def_file, String name_server_addr, String cont_addr, String address) throws InterruptedException {
		this_address = MessageBroker.instance().listenOn(address);
		this.name_server_addr = name_server_addr;
		this.cont_addr = cont_addr;
		id = def_file.substring(def_file.indexOf("_")+1, def_file.indexOf("."));
		IDistrict.setup(id);
		log.info("District id: "+id);
		MessageBroker.instance().registerHandler(Constants.DISTRICT, handler);
		graph = Graph.parse(this, def_file.substring(0, def_file.indexOf("."))+Graph.file_suffix);
		parseSegments(def_file);	//+1 for the routing service
		Map<String, List<ObjectDescriptor>> objs = parseObjects(def_file);
		router = new Router(this);
		INameServer.bind(name_server_addr, id, this_address);
		IController.initBegin(id, this_address, cont_addr, objs);
		Thread.sleep(8000);	//wait for everyone to register with name server
		router.setAddressCache(INameServer.list(name_server_addr, id));
		discoverNeighbours();
		ev_manager = new EventManager(id, cont_addr, 1);
		for(Map.Entry<String, List<ObjectDescriptor>> entry : objs.entrySet()) {
			segments.get(entry.getKey()).init(entry.getValue());
		}
		router.init();
		log.info("setup complete");
		IController.initComplete(id, cont_addr);
		router.start();
		ev_manager.loop();
		router.join();
		for(Segment s : segments.values()) {
			s.join();
		}
		MessageBroker.instance().close();
	}

	private int parseSegments(String def_file) {
//		final int DEBUG_LENGTH = 150;
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(def_file));
			Gson gson = new Gson();
			String in;
			JsonParser parser = new JsonParser();
			int counter = 0;
			while((in = reader.readLine()) != null) {
				JsonObject obj = parser.parse(in).getAsJsonObject();
				List<String> ids = gson.fromJson(obj.get("id"), (new TypeToken<List<String>>(){}).getType());
				List<String> in_edges = gson.fromJson(obj.get("in_edges"), (new TypeToken<List<String>>(){}).getType());
				Segment s;
				if(ids.size()>1)
					s = new Roundabout(this, ids, obj.get("poi").getAsString(), "", obj.get("has_walkway").getAsBoolean(), obj.get("has_carlane").getAsBoolean(), obj.get("has_buslane").getAsBoolean(), 
							obj.get("length").getAsInt(), obj.get("max_speed").getAsInt(), obj.get("mutually_exclusive").getAsBoolean(), in_edges);
				else
					s = new Segment(this, ids.get(0), obj.get("poi").getAsString(), obj.get("poiType").getAsString(), obj.get("has_walkway").getAsBoolean(), obj.get("has_carlane").getAsBoolean(), obj.get("has_buslane").getAsBoolean(), 
							obj.get("length").getAsInt(), obj.get("max_speed").getAsInt(), obj.get("mutually_exclusive").getAsBoolean(), in_edges);
				for(String seg_id : ids)
					segments.put(seg_id, s);
				++counter;
			}
			reader.close();
			return counter;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	private Map<String, List<ObjectDescriptor>> parseObjects(String def_file) {
		try {
			Gson gson = new Gson();
			HashSet<String> ids = new HashSet<String>();
			Map<String, List<ObjectDescriptor>> objs = gson.fromJson(new FileReader(def_file.substring(0, def_file.indexOf('.'))+"_objects.def"), new TypeToken<Map<String, List<ObjectDescriptor>>>(){}.getType());
			for(Map.Entry<String,  List<ObjectDescriptor>> entry : objs.entrySet())
				for(ObjectDescriptor desc : entry.getValue()) {
					if(ids.contains(desc.getId())) {
						log.severe("ERROR: duplicate object ID "+desc.getId()+" found, aborting");
						System.exit(1);
					}
					ids.add(desc.getId());
//					System.out.println(gson.toJson(desc));
					desc.setCurNode(entry.getKey());
				}
			return objs; 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HashMap<String, List<ObjectDescriptor>>();
	}

	private void discoverNeighbours() {
		Map<String, String> edge_map = graph.getEdgeMap();
		for(Map.Entry<String, String> entry : router.addressCache().entrySet()) {
			if(!entry.getKey().startsWith("dist")) //districts' ID start with "dist" keyword
				continue;
			if(IDistrict.notifyEdgeNodes(entry.getValue(), edge_map)) {
				router.addNeighbour(entry.getKey());
			}
		}
	}
	
	public void start() {
		for(Segment s : segments.values())
			s.start();
	}
	
	public void beginTermination() {
		System.out.println("Releasing Name Server bind");
		INameServer.release(name_server_addr, id);
		System.out.println("Terminating segments. Total number: "+ev_manager.totalSegments());
		for(Segment s : segments.values())
			s.terminate();
		router.terminate();
	}
	
	public void segmentStarted() {
		ev_manager.segmentStarted();
	}
	
	public void segmentTerminated() {
		ev_manager.segmentTerminated();
	}
	
	public void fireEvent(SimEvent e) {
		e.source = id;
		ev_manager.pushEvent(e);
	}

	public boolean containsNode(String node) {
		return graph.contains(node);
	}

	public ISegment getSegment(String seg_id, String dist_id, String next_id) {
		if(graph.isPseudoEdge(seg_id+"-"+next_id))
			return null;
		if(segments.containsKey(seg_id))
			return segments.get(seg_id);
		String rem_dist = graph.getEdgeNodeDistrict(seg_id);
		if(rem_dist != null) {
//			log.info("edge node id: "+seg_id+"; belongs to district "+rem_dist);
			return new RemoteSegment(seg_id, rem_dist, router.getAddress(rem_dist), this);
		}
		return new RemoteSegment(seg_id, dist_id, router.getAddress(dist_id), this);
	}
	
	public int getSegmentCost(String seg_id) {
		if(segments.containsKey(seg_id))
			return segments.get(seg_id).getCost();
		return new RemoteSegment(seg_id, "", "", this).getCost();
	}

	public void resolvePath(String src_node, String dst_zone, String dst_node, ObjectDescriptor desc) {
		if(dst_zone.equals(id)) {
			graph.resolvePath(src_node, dst_node, desc);
			return;
		}
		String next_dist;
		if(!router.addressCache().containsKey(dst_zone)) {
			String res = INameServer.resolve(name_server_addr, dst_zone);
			router.updateAddressCache(dst_zone, res);
			log.severe("Attempting to resolve path to unknown district "+dst_zone);
			next_dist = router.firstNeighbour();
		} else
			next_dist = router.getNextHop(dst_zone);
		List<String> to_avoid = desc.curTask().additional_info;	//check if we can pass through that district
		while(to_avoid !=null && to_avoid.contains(next_dist) && !to_avoid.containsAll(router.getNeighbours()))
			next_dist = router.randomNeighbour();
		desc.curTask().additional_info = null;	//can be used only once, clear it
		graph.resolveExternalPath(src_node, next_dist, desc);
	}

	public String resolveZone(String node) {
		if(graph.contains(node))
			return id;
		else {
//			log.info("Resolving remote node "+node+" district");
			for(Map.Entry<String, String> host : router.addressCache().entrySet()) {
				if(IDistrict.containsNode(node, host.getValue())) {
//					log.info("node "+node+" belongs to remote district "+host.getKey());
					return host.getKey();
				}
			}
			log.severe("Node zone resolution for "+node+" retunred null result");
			return null;
		}
	}

	public String getID() {
		return id;
	}

	public String getAddr() {
		return this_address;
	}

	public static void main(String[] args) {
		final String protocol = "tcp://";
		String addr = "tcp://";
		if(args.length < 3) {
			System.out.println("too few arguments: <filename> <name_server_addr> <controller_addr> [this_addr]");
			return;
		}
		if(args.length > 3)
			addr = addr.concat(args[3]);
		else
			addr = addr.concat("*");
		try {
			new District(args[0], protocol.concat(args[1]), protocol.concat(args[2]), addr);
			System.out.println("Node terminated. Press enter to exit");
			return;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
