package district.district;

import java.util.Arrays;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inspirel.yami.IncomingMessage;
import com.inspirel.yami.IncomingMessageCallback;
import com.inspirel.yami.Parameters;

import district.definitions.Constants;
import district.definitions.Services;
import district.net.IDistrict;
import district.objects.ObjectDescriptor;

public class DistrictHandler implements IncomingMessageCallback {
	private static final Logger log = Logger.getLogger(DistrictHandler.class
			.getName());
	private District district;
	private Gson gson = new Gson();

	public DistrictHandler(District district) {
		this.district = district;
	}

	@Override
	public void call(IncomingMessage msg) throws Exception {
		String service = msg.getMessageName();
		Parameters args = msg.getParameters();
		String host_id = args.getString("host_id");
		switch(service) {
		case Services.ROUTER_STATE_UPDATE:
			long seq = args.getLong("seq");
			district.router.stateUpdate(host_id, Arrays.asList(args.getStringArray("neigh")), seq);
			break;
		case Services.ROUTER_REQUEST_STATE_UPDATE:
			district.router.propagateStateInfo(host_id, msg.getSource());
			break;
		case Services.DISTRICT_LIST_EDGE_NODES:
			boolean neigh = false;
			Map<String, String> remote_map = gson.fromJson(args.getString("edgeMap"), new TypeToken<Map<String, String>>(){}.getType());
			for(Map.Entry<String, String> entry : remote_map.entrySet()) {
//				System.out.println(entry);
				if(district.containsNode(entry.getKey())) {
					district.graph.registerRemoteNode(entry.getValue(), host_id);
					neigh = true;
					log.info("Neighbour "+host_id+" added on node "+entry.getValue());
				}
			}
//			log.info("sending reply");
			Parameters reply = new Parameters();
			if(neigh)
				reply.setString("response", Constants.SUCCESS);
			else
				reply.setString("response", Constants.FAILURE);
			msg.reply(reply);
			if(!district.router.addressCache().containsKey(host_id)) {
				log.info("received edge nodes list from unknown node "+host_id);
				district.router.updateAddressCache(host_id, msg.getSource());
				if(neigh) {
					district.router.addNeighbour(host_id);
					IDistrict.notifyEdgeNodes(msg.getSource(), district.graph.getEdgeMap());
					district.router.setDirty(); //new neighbour discovered. Need to update link state
				}
			}
			break;
		case Services.DISTRICT_CONTAINS_NODE:
			reply = new Parameters();
			if(district.containsNode(args.getString("node")))
				reply.setString("response", Constants.SUCCESS);
			else
				reply.setString("response", Constants.FAILURE);
			msg.reply(reply);
			break;
		case Services.DISTRICT_PUSH_DESCRIPTOR:
			String seg_id = args.getString("seg_id");
			String in_edge = args.getString("in_edge");
			Queue<ObjectDescriptor> descs = gson.fromJson(args.getString("descriptor"), new TypeToken<Queue<ObjectDescriptor>>(){}.getType());
			//TODO: debug code
			if(!district.segments.containsKey(seg_id)) {
				log.warning("Segment "+seg_id+" is not part of district "+district.id);
				return;
			}
//			for(ObjectDescriptor d : descs)
//				System.out.println("Received object descriptor: "+gson.toJson(d)+"\n");
			district.segments.get(seg_id).pushAll(in_edge, descs);
//			System.out.println("Pushed received objects to destination "+seg_id);
			break;
		case Services.DISTRICT_BEGIN:
			district.start();
			break;
		case Services.DISTRICT_MARKER:
			district.beginTermination();
			break;
		case Services.TERMINATE:
			district.ev_manager.terminate();
			break;
		}
	}
}
