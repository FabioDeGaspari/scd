package district.events;

import com.google.gson.Gson;

import district.objects.ObjectDescriptor;
import district.objects.ObjectDescriptor.Type;
import district.objects.Task;
import district.segment.Segment;

public class DescriptionBuilder {
	private DescriptionBuilder() {}
	
	public static String describeObject(ObjectDescriptor desc) {
		if(desc == null)
			return "";
		String subject = "";
		switch(desc.getType()) {
		case PEDESTRIAN:
			subject = "Pedestrian ";
			break;
		case PRIVATE_VEHICLE:
			subject = "Car ";
			break;
		case PUBLIC_VEHICLE:
			subject = "Bus line "+desc.getAdditionalInfo()+" ";
			break;
		}
		return subject+desc.getName()+"(id "+desc.getId()+") ";
	}
	
	public static String describeDestination(Task task) {
		if(task == null)
			return "";
		return task.dest_id+" in district "+task.dest_zone;
	}
	
	public static String describeAction(ObjectDescriptor subject, Task.Type task_type, Task action, boolean is_bus, ObjectDescriptor target, Segment cur_seg) {
		String base = describeObject(subject);
		String sub;
		String action_str;
		String target_id;
		switch(task_type) {
		case USE:
			if(target == null) {
				return base+" is leaving the bus at "+cur_seg.getID();
			}
			return base+"is taking "+(is_bus?"bus "+target.getId()+"; line "+target.getAdditionalInfo() : "the car "+target.getId())
					+" to destination "+describeDestination(action);
		case WAIT_FOR_EVENT:
			target_id = target==null?action.event_id:target.getId();
			if (subject.getType()==ObjectDescriptor.Type.PEDESTRIAN) {
				sub = "waiting ";
				action_str = " for "+target_id+" to arrive at the stop";
			} else {
				sub = "parked ";
				action_str = "";
			}
			return base+"is "+(action.completed_flag?"in the bus "+target_id+" moving to destination "+describeDestination(action) :
				is_bus?(sub+" at node "+cur_seg.getID()+action_str) : "driving the car "+target_id+" to destination "+describeDestination(action));
		case DO_STOP:
			return base+" is stopping at bus stop "+cur_seg.getID();
		case MOVE_TO:
			target_id = cur_seg.getID()+"-"+action.peekNode(0);
			return base+(cur_seg.isMutuallyEx()&&subject.getType()==Type.PEDESTRIAN ? "is crossing the road" : 
				"is traveling "+ (is_bus?"the roundabout "+target_id : "along segment "+target_id));
		case WAIT_FOR_TIME:
			if(subject.getType() == Type.PUBLIC_VEHICLE)
				return base+" is stopping at the station for "+actionDuration(action);
			if(subject.getType()!=Type.PEDESTRIAN && action != null && action.temp_task)
				return "";
			sub = subject.getType()==Type.PEDESTRIAN? "waiting to cross the road" : "waiting for the pedestrians to finish crossing";
			return base+" is "+(action.temp_task?"leaving the car" : (cur_seg.isMutuallyEx()? sub : "at the "+cur_seg.getPoiType()+" for "+actionDuration(action)));
		}
		return "";
	}
	
	public static String describeAction(ObjectDescriptor subject, Task action, boolean is_bus, ObjectDescriptor target, Segment cur_seg) {
		return describeAction(subject, action.type, action, is_bus, target, cur_seg);
	}
	
	private static String actionDuration(Task task) {
		return (int)task.duration/1000+" seconds";
	}
}
