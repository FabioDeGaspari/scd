package district.events;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import district.definitions.Constants;
import district.net.IController;

public class EventManager {
	private Queue<SimEvent> events_queue = new ConcurrentLinkedQueue<SimEvent>();
	private String dist_id;
	private String cont_addr;
	private AtomicInteger threads_num = new AtomicInteger(0);
	private AtomicInteger terminated = new AtomicInteger(0);
	private boolean terminate = false;
	private final Object queue_signal = new Object();

	public EventManager(String dist_id, String cont_addr, int threads_num) {
		this.dist_id = dist_id;
		this.cont_addr = cont_addr;
		this.threads_num.set(threads_num);
	}

	public void pushEvent(SimEvent e) {
		events_queue.add(e);
		synchronized (queue_signal) {
			queue_signal.notifyAll();
		}
	}
	
	public void segmentStarted() {
		threads_num.incrementAndGet();
	}
	
	public int totalSegments() {
		return threads_num.get();
	}
	
	public void segmentTerminated() {
		if(terminated.incrementAndGet() == threads_num.get()) {
			System.out.println("EventManager: all threads terminated, sending confirmation to controller");
			IController.notifyTermination(dist_id, cont_addr, Constants.TERMINATION_STAGE1);
		}else
			System.out.println(terminated.get()+" segments terminated");
	}

	public void loop() {
		while(!terminate || !events_queue.isEmpty()){
			while(!events_queue.isEmpty()) {
				SimEvent e = events_queue.poll();
				IController.fireEvent(dist_id, cont_addr, e);
			}
			synchronized (queue_signal) {
				try {
					queue_signal.wait();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		System.out.println("Event Manager terminated");
	}
	
	public void terminate() {
		terminate = true;
		synchronized (queue_signal) {
			queue_signal.notifyAll();
		}
	}
}
