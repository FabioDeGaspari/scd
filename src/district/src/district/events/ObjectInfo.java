package district.events;

import district.objects.ObjectDescriptor;

public class ObjectInfo {
	String id;
	String name;
	String type;
	
	public ObjectInfo(ObjectDescriptor d) {
		id = d.getId();
		name = d.getName();
		type = d.getType().toString();
	}
}
