package district.events;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;

import district.objects.ObjectDescriptor;

public class SimEvent {
	public static final String TYPE_DIST_LEAVE = "dist_leave";
	public static final String TYPE_ENTERS = "enter";
	public static final String TYPE_LEAVES = "leaves";
	public static final String TYPE_USES = "use";
	public static final String TYPE_WAIT = "wait";
	public static final String TYPE_DO_STOP = "do_stop";
	public static final String TYPE_WAIT_FOR_EVENT = "wait_for_event";
	
	public String event_type;
	public String source;
	public List<String> obj_id = new ArrayList<String>();
	public String obj_type;
	public String target_id;
	public String additional_desc = "";
	public String human_readable_desc = "";
	public long rec_timestamp;
	public long duration;
	
	private SimEvent(String event_type, String source, String obj_type,
			String additional_desc, String target, long rec_timestamp, long duration) {
		this.event_type = event_type;
		this.source = source;
		this.obj_type = obj_type;
		this.additional_desc = additional_desc;
		this.target_id = target;
		this.rec_timestamp = rec_timestamp;
		this.duration = duration;
	}
	
	public SimEvent(String event_type, String obj_id, String obj_type, SimEvent event, String target, long duration) {
		this(event_type, "", obj_type, new Gson().toJson(event), target, -1, duration);
		this.obj_id.add(obj_id);
	}
	
	public SimEvent(String event_type, String source, String obj_id, String obj_type,
			String additional_desc, String target, long rec_timestamp, long duration) {
		this(event_type, source, obj_type, additional_desc, target, -1, duration);
		this.obj_id.add(obj_id);
	}
	
	public SimEvent(String event_type, String obj_id, String obj_type, String target, long duration) {
		this(event_type, "", obj_id, obj_type, "", target, -1, duration);
	}
	
	public SimEvent(String event_type, String obj_type, String target, long duration) {
		this(event_type, "", obj_type, "", target, -1, duration);
	}
	
	public SimEvent(String event_type, List<String> obj_id, String obj_type, String target, long duration) {
		this(event_type, "", obj_type, "", target, -1, duration);
		this.obj_id.addAll(obj_id);
	}
	
	public static SimEvent newLeaveDistrictEvent(List<ObjectDescriptor> descs, String obj_type, String target, long duration) {
		List<String> ids = new LinkedList<String>();
		for(ObjectDescriptor d : descs)
			ids.add(d.getId());
		return new SimEvent(SimEvent.TYPE_DIST_LEAVE, ids, obj_type, target, duration);
	}
}
