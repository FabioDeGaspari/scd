package district.graph;

import java.io.FileReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import district.district.District;
import district.graph.Graph.Algorithms.EdgeLength;
import district.objects.ObjectDescriptor;
import district.objects.Task;

public class Graph {
	public static final String file_suffix = ".graph";
	private District district;
	private List<String> edge_nodes;
	private Set<String> pseudo_edges;
	private Map<String, String> edgeMap = null;
	private Map<String, ConcurrentLinkedQueue<String>> remote_nodes= new ConcurrentHashMap<String, ConcurrentLinkedQueue<String>>(); //For each district id, list of neighbour nodes. Used for faster access.
	private Map<String, Map<String, Boolean>> crossing_info;
	private Map<String, List<String>> graph;

	public static class Algorithms {
		private static class GraphNode implements Comparable<GraphNode> {
			public String id;
			public long distance;
			public String previous;

			public int compareTo(GraphNode o) {
				if(distance>o.distance) return 1;
				if(distance<o.distance) return -1;
				return 0;
			}
		}

		public interface EdgeLength {
			int length(String n1, String n2);
			boolean canCross(String n1, String n2);
		}

		public static Stack<String> bestPath(Map<String, List<String>> graph, String src, String dst, EdgeLength length_func) {
			Stack<String> res = new Stack<String>();
			Map<String, GraphNode> distance = new HashMap<String, GraphNode>();
			PriorityQueue<GraphNode> set = new PriorityQueue<GraphNode>();
			for(final String node : graph.keySet()) {
				if(!node.equals(src)) {
					distance.put(node, new GraphNode(){{id=node; distance=Long.MAX_VALUE; previous=null;}});
				} else
					distance.put(node, new GraphNode(){{id=node; distance=0; previous=null;}});
				set.add(distance.get(node));
			}
			while(!set.isEmpty()) {
				GraphNode min = set.poll();
//				System.out.println("min is "+min.id);
				if(min.distance == Long.MAX_VALUE)
					return null;
				if(min.id.equals(dst)) {
					while(min.previous != null) {
						res.push(min.id);
						min = distance.get(min.previous);
					}
					return res;
				}
				for(String n : graph.get(min.id)) {
					if(!length_func.canCross(min.id, n))
						continue;
					GraphNode neigh = distance.get(n);
					if(neigh == null)
						continue;
					long tmp = min.distance + length_func.length(min.id, n);
					if(tmp < neigh.distance) {
						neigh.distance = tmp;
						neigh.previous = min.id;
						set.remove(neigh);
						set.add(neigh);
					}
				}
			}
			return null;
		}
	}

	private class GraphEdgeLength implements EdgeLength {
		ObjectDescriptor desc;
		
		public GraphEdgeLength(ObjectDescriptor desc) {
			this.desc = desc;
		}

		public int length(String n1, String n2) {
			return edgeCost(n1, n2);
		}

		public boolean canCross(String n1, String n2) {
			return canObjectCross(n1, n2, desc.getType());
		}
	};

	private Graph(District dist) {
		this.district = dist;
	}

	public static Graph parse(District dist, String file) {
		Graph g = new Graph(dist);
		Gson gson = new Gson();
		JsonElement json;
		try {
			json = new JsonParser().parse(new FileReader(file));
			g.edge_nodes = gson.fromJson(json.getAsJsonObject().get("edge").getAsJsonArray(), (new TypeToken<List<String>>(){}).getType());
			g.pseudo_edges = gson.fromJson(json.getAsJsonObject().get("pseudo").getAsJsonArray(), (new TypeToken<Set<String>>(){}).getType());
			g.graph = gson.fromJson(json.getAsJsonObject().get("graph").getAsJsonObject(), (new TypeToken<Map<String, List<String>>>(){}).getType());
			g.crossing_info = gson.fromJson(json.getAsJsonObject().get("edge_info").getAsJsonObject(), (new TypeToken<Map<String, Map<String, Boolean>>>(){}).getType());
			//TODO: fix
			g.getEdgeMap();
			for(String node : g.edge_nodes)
				if(!g.graph.containsKey(node))
					g.graph.put(node, new LinkedList<String>());
			return g;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isPseudoEdge(String id) {
		return pseudo_edges.contains(id);
	}

	public List<String> edgeNodes() {
		return edge_nodes;
	}

	public boolean contains(String node) {
		return graph.containsKey(node);
	}

	public Task resolvePath(String src, String dst, final ObjectDescriptor desc) {
//		System.out.println("Resolving path from "+src+" to "+dst);
		EdgeLength length_func = new GraphEdgeLength(desc);
		Stack<String> res = Algorithms.bestPath(graph, src, dst, length_func); 
		if(res != null) {
			Collections.reverse(res);
			desc.curTask().setResolvedPath(res);
			return desc.curTask();
		}
		System.out.println("no intra-district path exists between "+src+" and "+dst+". Searching for inter-district paths...");
		//no path from current node to dest, check if there is one from an edge node
		return resolveInterDistrictPath(src, dst, desc);
	}

	public Task resolveExternalPath(String src, String dst_dist, final ObjectDescriptor desc) {
//		System.out.println("resolve external path from "+src+" to district "+dst_dist);
		EdgeLength length_func = new GraphEdgeLength(desc);
		//look for a direct path to every possible edge node of dst_dist
		for(String dst : remote_nodes.get(dst_dist)) {
			for(Map.Entry<String, List<String>> entry : graph.entrySet()) {
				Stack<String> path;
				if(entry.getValue().contains(dst) && (path=Algorithms.bestPath(graph, src, dst, length_func))!=null) {
					Collections.reverse(path);
					desc.curTask().setResolvedPath(path);
					return desc.curTask();
				}
			}
		}
		for(String dst : remote_nodes.get(dst_dist)) {
			for(Map.Entry<String, List<String>> entry : graph.entrySet()) {
				if(entry.getValue().contains(dst))
					return resolveInterDistrictPath(src, dst, desc);
			}
		}
		//no direct paths to dst_dist exist, look for an inter-district path
		System.out.println("No path to remote district "+dst_dist+" found");
		return null;
	}
	
	private Task resolveInterDistrictPath(String src, String dst, final ObjectDescriptor desc) {
		EdgeLength length_func = new GraphEdgeLength(desc);
		Stack<String> res;
		Map<String, Stack<String>> path_to_edge = new HashMap<String, Stack<String>>(); 		//map of paths from src to each reachable edge node, indexed by edge node
		Map<String, List<String>> reachable_from_edge = new HashMap<String, List<String>>();	//list of edge nodes from which we can reach dst, indexed by district
		for(String s : edge_nodes) {
			Stack<String> path = Algorithms.bestPath(graph, src, s, length_func);
			if(path!=null) {
//				System.out.println("path found from "+src+" to edge node "+s);
				path_to_edge.put(s, path);
			}
//			else
//				System.out.println("no path exists from "+src+" to edge node "+s);
			Stack<String> reachable = Algorithms.bestPath(graph, s, dst, length_func);
			if(reachable!=null) {
//				System.out.println("found path from edge node "+s+" to destination "+dst);
				String host_dist = getEdgeNodeDistrict(s);
				if(!reachable_from_edge.containsKey(host_dist))
					reachable_from_edge.put(host_dist, new LinkedList<String>());
				reachable_from_edge.get(host_dist).add(s);
			}
		}
		Stack<String> path = null;
		String remote_dest = null;
		String remote_dist = null;
		if(path_to_edge.keySet().size() == 0)
			System.out.println("ERROR: no reachable edge node from current position: from "+src+" to "+dst);
		for(Map.Entry<String, Stack<String>> entry : path_to_edge.entrySet()) { //for each reachable edge node
			String host_dist = getEdgeNodeDistrict(entry.getKey());
			if(reachable_from_edge.containsKey(host_dist)) {
				path = entry.getValue();
				remote_dest = reachable_from_edge.get(host_dist).get(0);
				remote_dist = host_dist;
			}
		}
		if(path!=null) {
//			boolean temp = desc.curTask().temp_task;
			//expand task list
			List<Task> extension = new LinkedList<Task>();
			Collections.reverse(path);
			extension.add(Task.newMoveTask(path.peek(), remote_dist, path, true));
			extension.add(Task.newMoveTask(remote_dest, remote_dist, null, true));
			extension.add(desc.curTask());
			desc.expandCurrentTask(extension);
			return desc.curTask();
		}
		//can not find any good solution, return path to random reachable neighbour
		String next_dist = path_to_edge.keySet().iterator().next();
		System.out.println("no inter-district path found. Forwarding to random reachable district: "+next_dist);
		res = path_to_edge.get(next_dist);
		Collections.reverse(res);
		desc.curTask().setResolvedPath(res);
		return desc.curTask();
	}
	
	public Map<String, String> getEdgeMap() {
		if(edgeMap!=null)
			return edgeMap;
		edgeMap = new HashMap<String, String>();
		for(String node : edge_nodes) {
			if(graph.containsKey(node)) {
				edgeMap.put(node, graph.get(node).get(0)); //remote nodes are always connected 1-1
			} else
			for(Map.Entry<String, List<String>> entry: graph.entrySet()) {
				if(entry.getValue().contains(node))
					edgeMap.put(node, entry.getKey());
			}
		}
		return edgeMap;
	}

	public void registerRemoteNode(String node, String rem_district) {
		if(!remote_nodes.containsKey(rem_district))
			remote_nodes.put(rem_district, new ConcurrentLinkedQueue<String>());
		remote_nodes.get(rem_district).add(node);
	}

//	public void registerRemoteNodes(List<String> rem_nodes, String rem_district) {
//		if(!remote_nodes.containsKey(rem_district))
//			remote_nodes.put(rem_district, new ConcurrentLinkedQueue<String>());
//		remote_nodes.get(rem_district).addAll(rem_nodes);
//	}

	private int edgeCost(String src, String dst) {
		if(pseudo_edges.contains(src+"-"+dst))
			return 0;
		return district.getSegmentCost(src); 
	}

	private boolean canObjectCross(String src, String dst, ObjectDescriptor.Type type) {
		return crossing_info.get(src+"-"+dst).get(type.toString());
	}

	public String getEdgeNodeDistrict(String node) {
		for(Map.Entry<String, ConcurrentLinkedQueue<String>> entry : remote_nodes.entrySet()) {
//			System.out.println("district "+entry.getKey()+" remote nodes are: "+new Gson().toJson(entry.getValue()));
			if(entry.getValue().contains(node))
				return entry.getKey();
		}
		return null;
	}
}
