package district.net;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.inspirel.yami.Parameters;

import district.definitions.Constants;
import district.definitions.Services;
import district.events.ObjectInfo;
import district.events.SimEvent;
import district.objects.ObjectDescriptor;

public class IController {
	private static final Gson gson = new Gson();
	
	public static void fireEvent(String node_id, String cont_addr, SimEvent event) {
		Parameters args = new Parameters();
		args.setString("host_id", node_id);
		args.setString("event", gson.toJson(event));
		MessageBroker.instance().sendOneWay(cont_addr, Constants.CONTROLLER, Services.CONTROLLER_EVENT, args);
	}
	
	public static void initBegin(String node_id, String addr, String cont_addr, Map<String, List<ObjectDescriptor>> objs) {
		Parameters args = new Parameters();
		args.setString("host_id", node_id);
		args.setString("addr", addr);
		Map<String, List<ObjectInfo>> objs_setup = new HashMap<String, List<ObjectInfo>>();
		for(Map.Entry<String, List<ObjectDescriptor>> entry : objs.entrySet()) {
			List<ObjectInfo> infos = new LinkedList<ObjectInfo>();
			for(ObjectDescriptor desc : entry.getValue())
				infos.add(new ObjectInfo(desc));
			objs_setup.put(entry.getKey(), infos);
		}
		args.setString("objs", gson.toJson(objs_setup));
		MessageBroker.instance().sendOneWay(cont_addr,Constants.CONTROLLER, Services.CONTROLLER_INIT_BEGIN, args);
	}
	
	public static void initComplete(String node_id, String cont_addr) {
		Parameters args = new Parameters();
		args.setString("host_id", node_id);
		MessageBroker.instance().sendOneWay(cont_addr, Constants.CONTROLLER, Services.CONTROLLER_INIT_COMPLETE, args);
	}
	
	public static void notifyTermination(String node_id, String cont_addr, String term_stage) {
		Parameters args = new Parameters();
		args.setString("host_id", node_id);
		args.setString("term_stage", term_stage);
		MessageBroker.instance().send(cont_addr, Constants.CONTROLLER, Services.CONTROLLER_TERMINATION_STATUS, args);
	}
}
