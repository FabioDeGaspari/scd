package district.net;

import java.util.Map;

import com.google.gson.Gson;
import com.inspirel.yami.Parameters;

import district.definitions.Constants;
import district.definitions.Services;
import district.objects.ObjectDescriptor;

public class IDistrict {
	private static Gson gson = new Gson();
	private static String dist_id = "";
	
	public static void setup(String dist) {
		dist_id = dist;
	}

	public static boolean notifyEdgeNodes(String dst_addr, Map<String, String> edgeMap) {
		Parameters args = new Parameters();
		args.setString("host_id", dist_id);
		args.setString("edgeMap", gson.toJson(edgeMap));
		Parameters res = MessageBroker.instance().send(dst_addr, Constants.DISTRICT, Services.DISTRICT_LIST_EDGE_NODES, args);
		if(res == null || res.getString("response").equals(Constants.FAILURE))
			return false;
		return true;
	}

	public static boolean containsNode(String node, String addr) {
		Parameters args = new Parameters();
		args.setString("host_id", dist_id);
		args.setString("node", node);
		args = MessageBroker.instance().send(addr, Constants.DISTRICT, Services.DISTRICT_CONTAINS_NODE, args);
		if(args != null && args.getString("response").equals(Constants.SUCCESS))
			return true;
		return false;
	}
	
	public static void pushDescriptor(String addr, ObjectDescriptor[] descriptors, String in_edge, String seg_id) {
		String json = gson.toJson(descriptors);
		Parameters args = new Parameters();
		args.setString("host_id", dist_id);
		args.setString("seg_id", seg_id);
		args.setString("in_edge", in_edge);
		args.setString("descriptor", json);
//		for(ObjectDescriptor d : descriptors)
//			System.out.println("Sent object descriptor: "+gson.toJson(d)+"\n");
		MessageBroker.instance().sendOneWay(addr, Constants.DISTRICT, Services.DISTRICT_PUSH_DESCRIPTOR, args);
	}
}