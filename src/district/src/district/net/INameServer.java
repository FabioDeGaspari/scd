package district.net;

import java.util.Map;
import java.util.logging.Logger;

import com.inspirel.yami.Parameters;

import district.definitions.Constants;
import district.definitions.Services;
import district.yami.Converter;

public class INameServer {
	private static final Logger log = Logger.getLogger(MessageBroker.class.getName());
	private static final int RETRY = 200;
	private static final int MAX_RETRIES = 3;

	public static void bind(String server_addr, String node_id, String node_addr) {
		int counter = MAX_RETRIES;
		Parameters args = new Parameters();
		args.setString("host_id", node_id);
		args.setString("addr", node_addr);
		while(counter-- != 0) {
			Parameters res =  MessageBroker.instance().send(server_addr, Constants.NAME_SERVER, Services.NAME_SERVER_BIND, args);
			if(res != null && MessageBroker.isSuccess(res))
				return;
			//sleep before retry
			log.severe("bind call failed");
			try {
				Thread.sleep(RETRY);
			} catch (InterruptedException e) {
				log.severe(e.getStackTrace().toString());
			}
		}
		log.severe("Fatal: cannot register node with Name Server");
		System.exit(0);
	}

	public static void release(String server_addr, String node_id) {
		Parameters args = new Parameters();
		args.setString("host_id", node_id);
		Parameters res =  MessageBroker.instance().send(server_addr, Constants.NAME_SERVER, Services.NAME_SERVER_RELEASE, args);
		if(!MessageBroker.isSuccess(res))
			log.warning("Could not release address");
	}

	public static Map<String, String> list(String server_addr, String node_id) {
		Parameters args = new Parameters();
		args.setString("host_id", node_id);
		Parameters res =  MessageBroker.instance().send(server_addr, Constants.NAME_SERVER, Services.NAME_SERVER_LIST, args);
		if(res!=null && MessageBroker.isSuccess(res))
			return Converter.paramsToConcurrentMap(res);
		else
		{
			log.warning("Could not retrieve nodes list");
			return null;
		}
		
	}

	public static String resolve(String server_addr, String node_id) {
		Parameters args = new Parameters();
		args.setString("host_id", node_id);
		Parameters res =  MessageBroker.instance().send(server_addr, Constants.NAME_SERVER, Services.NAME_SERVER_RESOLVE, args);
		if(MessageBroker.isSuccess(res))
			return res.getString("resolved_address");
		else
		{
			log.warning("Could not resolved target address");
			return null;
		}
	}
}
