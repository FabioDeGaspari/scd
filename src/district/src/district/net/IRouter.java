package district.net;

import java.util.List;

import com.inspirel.yami.Parameters;

import district.definitions.Constants;
import district.definitions.Services;

public class IRouter {
	
	public static void stateUpdate(String id, String dst, List<String> neighbours, long seq) {
		Parameters args = new Parameters();
		args.setString("host_id", id);
		args.setLong("seq", seq);
		args.setStringArray("neigh", neighbours.toArray(new String[0]));
		MessageBroker.instance().sendAsync(dst, Constants.DISTRICT, Services.ROUTER_STATE_UPDATE, args, null);
	}
	
	public static void requestStateUpdate(String id, String dst) {
		Parameters args = new Parameters();
		args.setString("host_id", id);
		MessageBroker.instance().sendAsync(dst,Constants.DISTRICT, Services.ROUTER_REQUEST_STATE_UPDATE, args, null);
	}
}
