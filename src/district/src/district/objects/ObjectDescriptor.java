package district.objects;

import java.util.List;

import district.district.District;

//TODO: fix transient fields
public class ObjectDescriptor implements Comparable<ObjectDescriptor> {
	public static long PED_MOUNT = 2000;
	public static long MAX_BUS_STOP_LENGTH = 10000;
	public enum Type {PEDESTRIAN, PRIVATE_VEHICLE, PUBLIC_VEHICLE};	
	private List<Task> tasks;
	private String id;
	private String name;
	private String node = "";
	private ObjectDescriptor.Type type;
	private String additional_info; //can be owner_id for a car, or car_id for a pedestrian
	private int pos;
	private int cur_speed;
	private int max_speed;


	public ObjectDescriptor(List<Task> tasks, String id, String name, Type type, String additional_info, int max_speed) {
		this.tasks = tasks;
		pos = 0;
		this.id = id;
		this.name = name;
		this.type = type;
		this.additional_info = additional_info;
		this.max_speed = max_speed;
	}

	public void initAllTasks(District district) {
		System.out.println("descriptor "+id);
		for(Task task : tasks) {
			if(task.type == Task.Type.MOVE_TO || task.type == Task.Type.WAIT_FOR_EVENT || task.type == Task.Type.USE) {
				if(type == Type.PRIVATE_VEHICLE && task.type == Task.Type.WAIT_FOR_EVENT)
					return;
				task.dest_zone = district.resolveZone(task.dest_id);
			}
		}
	}

	public void setCurNode(String n) {
		node = n;
	}

	public String getCurNode() {
		return node;
	}

	public void expandCurrentTask(List<Task> expansion) {
		tasks.remove(pos);
		tasks.addAll(pos, expansion);
	}

	public String getAdditionalInfo() {
		return additional_info;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getMaxSpeed() {
		return max_speed;
	}

	public int getCurSpeed() {
		return cur_speed;
	}

	public void setCurSpeed(int cur_speed) {
		this.cur_speed = cur_speed;
	}

	public ObjectDescriptor.Type getType() {
		return type;
	}

	public void taskCompleted() {
		if(curTask().temp_task) {
			tasks.remove(curTask());
		} else {
			curTask().clear();
			++pos;
		}
		if(pos >= tasks.size())
			pos = 0;
	}

	public Task curTask() {
		return tasks.get(pos);
	}
	
	public boolean hasPrevious() {
		return pos > 0;
	}

	//set next task to shared task
	public void shareTask(Task t) {
		tasks.add(Task.getTempTask(t));
		pos = tasks.size()-1;
	}
	
	public void addTempTask(Task t) {
		tasks.add(pos, Task.getTempTask(t));
//		new Gson().toJson(tasks);
	}

	@Override
	public int compareTo(ObjectDescriptor o) {
		if(curTask().expiration_time > o.curTask().expiration_time)
			return 1;
		if(curTask().expiration_time < o.curTask().expiration_time)
			return -1;
		return 0;
	}
}
