package district.objects;

import java.util.List;
import java.util.logging.Logger;

//TODO: fix transient fields
public class Task {
	private static final Logger log = Logger.getLogger(Task.class.getName());
	public enum Type {USE, WAIT_FOR_EVENT, WAIT_FOR_TIME, MOVE_TO, DO_STOP};
	public Type type;
	public long duration = 0;
	public long expiration_time = Long.MAX_VALUE;
	public String dest_zone = null;
	public String dest_id = null;
	public String event_id = null;
	public boolean completed_flag = false;
	public boolean cross_flag = false;
	public boolean temp_task = false;
	public List<String> additional_info = null;
	public List<String> resolved_path = null;
	private int it = 0;

	public static Task newMoveTask(String dst, String zone, List<String> resolved_path, boolean temp) {
		Task t = new Task();
		t.type = Type.MOVE_TO;
		t.dest_id = dst;
		t.dest_zone = zone;
		t.resolved_path = resolved_path;
		t.temp_task = temp;
		return t;
	}
	
	public static Task newWaitTask(long duration) {
		Task t = new Task();
		t.type = Type.WAIT_FOR_TIME;
		t.duration = duration;
		return t;
	}
	
	public static Task newWaitForTask(String dst, String zone, String ev_id) {
		Task t = new Task();
		t.type = Type.WAIT_FOR_EVENT;
		t.dest_id = dst;
		t.dest_zone = zone;
		t.event_id = ev_id;
		return t;
	}
	
	public static Task getTempTask(Task t) {
		Task task = new Task();
		task.type = t.type;
		task.dest_zone = t.dest_zone;
		task.dest_id = t.dest_id;
		task.duration = t.duration;
		task.event_id = t.event_id;
		task.temp_task = true;
		return task;
	}

	public boolean isPathResolved() {
		return resolved_path != null && it<resolved_path.size();
	}

	public String getNextNode() {
		if(resolved_path == null || !(it<resolved_path.size())) {
			resolved_path = null;
			return null;
		}
		return resolved_path.get(it++);
	}

	public String peekNextNode() {
		if(resolved_path == null || !(it<resolved_path.size())) {
//			resolved_path = null;
			return null;
		}					 
		return resolved_path.get(it);
	}
	
	public String peekNode(int offset) {
		if(resolved_path == null || !(it+offset<resolved_path.size()) || (it+offset)<0) {
//			resolved_path = null;
			return null;
		}
		return resolved_path.get(it+offset);
	}

	public void setResolvedPath(List<String> path) {
//		System.out.println((new Gson()).toJson(path));
		if(path.size()==0) {
			log.severe("Resolved path of zero length");
			log.severe(Thread.currentThread().getStackTrace().toString());
		}
		resolved_path = path;
		it = 0;
	}

	public void clear() {
		if(!type.equals(Type.WAIT_FOR_TIME))
			duration = 0;
		if(event_id != null && event_id.contains(":"))
			event_id = event_id.split(":")[0];
		expiration_time = Long.MAX_VALUE;
		resolved_path = null;
		it = 0;
		completed_flag = false;
	}
}
