package district.routing;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Logger;

import district.district.District;
import district.graph.Graph;
import district.graph.Graph.Algorithms.EdgeLength;
import district.net.IRouter;

public class Router extends Thread {
	private static final Logger log = Logger.getLogger(Router.class.getName());
	private static final int ROUTER_SETUP_TIME = 500;
	private District district;
	private SortedSet<String> neighbours = new ConcurrentSkipListSet<String>();
	private Map<String, String> addr_cache;
	long seq_num = 0;
	private Map<String, Long> seq_info;
	private Map<String, String> routing_table = new ConcurrentHashMap<String, String>();
	private Map<String, List<String>> hosts_graph = new ConcurrentHashMap<String, List<String>>();
	private final Object dirty_lock = new Object();
	private boolean dirty = false;
	public boolean terminate = false;

	private class HopCost implements EdgeLength {
		@Override
		public int length(String n1, String n2) {
			return 1;
		}

		@Override
		public boolean canCross(String n1, String n2) {
			return true;
		}
	}

	public Router(District district) {
		this.district = district;
		addr_cache = new ConcurrentHashMap<String, String>();
		hosts_graph.put(district.getID(), new LinkedList<String>());
		seq_info = new ConcurrentHashMap<String, Long>();
	}

	public void init() throws InterruptedException {
		hosts_graph.get(district.getID()).addAll(neighbours);
		seq_info.put(district.getID(), seq_num);
		propagateStateInfo();
		__dump_host_graph();
		Thread.sleep(ROUTER_SETUP_TIME);
		for(String host : addr_cache.keySet()) {
			if(!hosts_graph.containsKey(host)) { //host didn't send us state update yet, send request
				IRouter.requestStateUpdate(district.getID(), addr_cache.get(host));
			}
		}
	}

	@Override
	public void run() {
		while(!terminate)
			updateLoop();
		System.out.println("Routing service terminated");
		district.segmentTerminated();
	}

	public void updateLoop() {
		synchronized (dirty_lock) {
			while(!dirty && !terminate) {
				try {
					dirty_lock.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			dirty = false;
		}
		seq_num++;
		stateUpdate(district.getID(), new LinkedList<String>(neighbours), seq_num);
		propagateStateInfo();
	}

	public void terminate() {
		terminate = true;
		synchronized (dirty_lock) {
			dirty_lock.notifyAll();
		}
	}

	public String firstNeighbour() {
		return neighbours.first();
	}

	public String randomNeighbour() {
		return neighbours.toArray(new String[0])[(int)(Math.random()%neighbours.size())];
	}

	public List<String> getNeighbours() {
		return new LinkedList<String>(neighbours);
	}

	public String getAddress(String dist_id) {
		String res = addr_cache.get(dist_id);
		if(res == null) {
			log.severe("Router could not resolve "+dist_id+" address");
		}
		return res;
	}

	public void updateAddressCache(String dist_id, String address) {
		//		routing_table.put(dist_id, "");
		addr_cache.put(dist_id, address);
	}

	public Map<String, String> addressCache() {
		return addr_cache;
	}

	public void addNeighbour(String neigh) {
		if(!neighbours.contains(neigh)) {
			log.info("new neighbour "+neigh+" added to list");
			neighbours.add(neigh);
		}
		if(!hosts_graph.containsKey(neigh)) {
			hosts_graph.put(neigh, new LinkedList<String>());
		}
	}

	public void setAddressCache(Map<String, String> cache) {
		addr_cache.putAll(cache);
		addr_cache.remove(district.getID());
	}

	public void propagateStateInfo() {
		if(terminate)
			return;
		for(Map.Entry<String, String> entry: addr_cache.entrySet()) {
			IRouter.stateUpdate(district.getID(), entry.getValue(), hosts_graph.get(district.getID()), seq_num);
		}
	}

	public void propagateStateInfo(String dst_dist, String address) {
		if(!addr_cache.containsKey(dst_dist)) {
			addr_cache.put(dst_dist, address);
		}
		IRouter.stateUpdate(district.getID(), address, hosts_graph.get(district.getID()), seq_num);
	}

	public void stateUpdate(String dist_id, List<String> neighbours, long seq) {
		if(terminate)
			return;
		//		log.info("State update from "+dist_id);
		if(seq_info.containsKey(dist_id) && seq < seq_info.get(dist_id)) {
//			log.info("Old state update, discarded");
			return;
		}
		seq_info.put(dist_id, seq);
		hosts_graph.put(dist_id, neighbours);
		__dump_host_graph();
		updateBestPath();
	}

	private void __dump_host_graph() {
		//		if (Project.DEBUG) {
		//			String dbg = "host graph:\n";
		//			for(Map.Entry<String, List<String>> e : hosts_graph.entrySet()) {
		//				dbg += "\t"+e.getKey()+": "+e.getValue().toString()+"\n";
		//			}
		//			log.info(dbg);
		//		}
	}

	private void __dump_routing_table() {
//		String dbg = "routing table:\n";
//		for(Map.Entry<String, String> e : routing_table.entrySet()) {
//			dbg += "\t"+e.getKey()+": "+e.getValue()+"\n";
//		}
//		log.info(dbg);
	}

	public void setDirty() {
		synchronized (dirty_lock) {
			dirty = true;
//			log.info("router state dirty");
			dirty_lock.notifyAll();
		}
	}

	private void updateBestPath() {
		for(String dst : addr_cache.keySet()) {
			if(dst.equals(district.getID())) continue;
			Stack<String> path = Graph.Algorithms.bestPath(hosts_graph, district.getID(), dst, new HopCost());
			if(path != null)
				routing_table.put(dst, path.peek());
			else {
//				log.severe("no path to "+dst);
				routing_table.put(dst, "");
			}
		}
		__dump_routing_table();
	}

	public String getNextHop(String dst_node) {
		String res = routing_table.get(dst_node);
		if(res.equals("")) {
			log.severe("No next hop for requested destination "+dst_node);
			return randomNeighbour();
		}
		return res;
	}
}
