package district.segment;

import java.util.Queue;

import district.objects.ObjectDescriptor;

public interface ISegment {

	public void pushAll(Queue<ObjectDescriptor> queue);
	public void pushAll(String in_edge, Queue<ObjectDescriptor> queue);
	public void pushDescriptor(ObjectDescriptor desc);
	public void pushDescriptor(String in_edge, ObjectDescriptor desc);
	public int getCost();
	public boolean canCross(ObjectDescriptor.Type type);
}