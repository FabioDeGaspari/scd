package district.segment;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import district.district.District;
import district.events.SimEvent;
import district.net.IDistrict;
import district.objects.ObjectDescriptor;
import district.objects.ObjectDescriptor.Type;

public class RemoteSegment implements ISegment {
	private static final int REMOTE_EDGE_COST = Integer.MAX_VALUE;
	private String addr;
	private String id;
	private District dist;
	
	public RemoteSegment(String id, String rem_dist, String addr, District dist) {
		System.out.println("new remote segment "+id+"; district: "+rem_dist+"; addr "+addr);
		this.id = id;
		this.addr = addr;
		this.dist = dist;
	}

	@Override
	public void pushAll(Queue<ObjectDescriptor> queue) {
		fireEvent(new LinkedList<ObjectDescriptor>(queue));
		IDistrict.pushDescriptor(addr, queue.toArray(new ObjectDescriptor[0]), "", id);
	}

	@Override
	public void pushAll(String in_edge, Queue<ObjectDescriptor> queue) {
		fireEvent(new LinkedList<ObjectDescriptor>(queue));
		IDistrict.pushDescriptor(addr, queue.toArray(new ObjectDescriptor[0]), in_edge, id);
	}

	@Override
	public void pushDescriptor(ObjectDescriptor desc) {
		LinkedList<ObjectDescriptor> tmp = new LinkedList<ObjectDescriptor>();
		tmp.add(desc);
		fireEvent(tmp);
		IDistrict.pushDescriptor(addr, new ObjectDescriptor[]{desc}, "", id);
	}

	@Override
	public void pushDescriptor(String in_edge, ObjectDescriptor desc) {
		LinkedList<ObjectDescriptor> tmp = new LinkedList<ObjectDescriptor>();
		tmp.add(desc);
		fireEvent(tmp);
		IDistrict.pushDescriptor(addr, new ObjectDescriptor[]{desc}, in_edge, id);
	}

	@Override
	public int getCost() {
		return REMOTE_EDGE_COST;
	}

	@Override
	public boolean canCross(Type type) {
		return true;
	}
	
	private void fireEvent(List<ObjectDescriptor> descs) {
		dist.fireEvent(SimEvent.newLeaveDistrictEvent(descs, descs.get(0).getType().toString(), dist.getID(), 0));
	}

}
