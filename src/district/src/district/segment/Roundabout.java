package district.segment;

import java.util.List;

import com.util.PriorityDeque;

import district.district.District;
import district.objects.ObjectDescriptor;

public class Roundabout extends Segment {
	private List<String> ids;

	public Roundabout(District district, List<String> id, String poi, String poiType, boolean has_walkway, boolean has_carlane, boolean has_buslane,
			int length, int max_speed, boolean mutually_exclusive, List<String> in_edges) {
		super(district, poi, poiType, length, max_speed, in_edges);
		this.ids = id;
		if(has_walkway)
			lanes.put(ObjectDescriptor.Type.PEDESTRIAN, new PriorityDeque<ObjectDescriptor>());
		if(has_carlane)
			lanes.put(ObjectDescriptor.Type.PRIVATE_VEHICLE, new PriorityDeque<ObjectDescriptor>());
		if(has_buslane)
			lanes.put(ObjectDescriptor.Type.PUBLIC_VEHICLE, new PriorityDeque<ObjectDescriptor>());
//		System.out.println("New roundabout segment created: "+(new Gson()).toJson(this));
	}

	@Override
	public String getID() {
		return ids.get(0);
	}

	@Override
	public int getCrossingTime(ObjectDescriptor desc) {
		int counter = 1;
		String cur_node = desc.curTask().peekNode(-1);
		String next_node = desc.curTask().peekNode(0);
		next_node = next_node.substring(0, next_node.indexOf('.'));
		int idx = (ids.indexOf(cur_node)+1)%ids.size();
		while(!ids.get(idx).startsWith(next_node)) {
			++counter;
			idx = (++idx)%ids.size();
		}
		return (length*counter/desc.getCurSpeed())*100	;
	}
	
	@Override
	public boolean needsAdditionalDesc() {
		return true;
	}
}
