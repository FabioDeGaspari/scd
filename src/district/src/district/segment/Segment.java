package district.segment;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.util.PriorityDeque;

import district.district.District;
import district.events.SimEvent;
import district.objects.ObjectDescriptor;
import district.objects.ObjectDescriptor.Type;
import district.objects.Task;

public class Segment implements ISegment {
	private static final Logger log = Logger.getLogger(Segment.class.getName());
	protected static final long MAX_CROSSING_WAITING = 10000;
	private transient District district;
	private String id;
	protected String poi; //point of interest managed by this segment (node id)
	protected String poiType;
	protected int length;
	protected int max_speed;
	protected boolean mutually_exclusive; //used for pedestrian crossings: either pedestrians advance or cars advance
	protected long started_waiting; //used to avoid starvation if this segment is a pedestrian crossing
	protected boolean pedestrinas_crossing;
	protected boolean vehicles_can_cross;
	protected transient Deque<ObjectDescriptor> ped_waiting_queue = null;
	protected transient Deque<ObjectDescriptor> car_waiting_queue = null;
	protected transient Map<ObjectDescriptor.Type, Deque<ObjectDescriptor>> lanes = new HashMap<ObjectDescriptor.Type, Deque<ObjectDescriptor>>();
	protected transient Deque<ObjectDescriptor> timed_queue = new PriorityDeque<ObjectDescriptor>();
	protected transient Map<String, Queue<ObjectDescriptor>> event_queue = new HashMap<String, Queue<ObjectDescriptor>>(); 
	protected transient Map<String, ConcurrentLinkedQueue<ObjectDescriptor>> in_queues = new HashMap<String, ConcurrentLinkedQueue<ObjectDescriptor>>();
	protected long next_event = Long.MAX_VALUE;
	private transient Thread worker_thread;
	private boolean started = false;
	private final transient Object queue_signal = new Object();
	private boolean queue_dirty = false;
	private long now;
	private boolean do_terminate = false;
	private boolean done = false;

	private class Worker implements Runnable {

		@Override
		public void run() {
			district.segmentStarted();
			now = System.currentTimeMillis();
			while(!done)
			{
				synchronized (queue_signal) {
					while(!queue_dirty && !timerExpired() && !done) try {
//						System.out.println(now+": Segment "+getID()+" sleeping for "+(next_event-now < 0 ? 0 : next_event-now));
						queue_signal.wait(next_event-now < 0 ? 0 : next_event-now);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					queue_dirty = false;
				}
				processInQueues();
				processTimedAction();
				isDone();
			}
			System.out.println("Segment "+getID()+" terminated");
			district.segmentTerminated();
		}
	}
	
	protected Segment(District district, String poi, String poiType, int length, int max_speed, List<String> in_edges) {
		this.district = district;
		this.poi = poi;
		this.poiType = poiType;
		this.length = length;
		this.max_speed = max_speed;
		this.mutually_exclusive = false;
		for(String in_id : in_edges)
			in_queues.put(in_id, new ConcurrentLinkedQueue<ObjectDescriptor>());
		in_queues.put("default", new ConcurrentLinkedQueue<ObjectDescriptor>());
		worker_thread = new Thread(new Worker());
	}
	
	public Segment(District district, String id, String poi, String poiType, boolean has_walkway, boolean has_carlane, boolean has_buslane, int length,
			int max_speed, boolean mutually_exclusive, List<String> in_edges) {
		this(district, poi, poiType, length, max_speed, in_edges);
		this.id = id;
		if(has_walkway)
			lanes.put(ObjectDescriptor.Type.PEDESTRIAN, new LinkedList<ObjectDescriptor>());
		if(has_carlane)
			lanes.put(ObjectDescriptor.Type.PRIVATE_VEHICLE, new LinkedList<ObjectDescriptor>());
		if(has_buslane)
			lanes.put(ObjectDescriptor.Type.PUBLIC_VEHICLE, new LinkedList<ObjectDescriptor>());
		if(this.mutually_exclusive = mutually_exclusive) {
			ped_waiting_queue = new LinkedList<ObjectDescriptor>();
			car_waiting_queue = new LinkedList<ObjectDescriptor>();
		}
//		System.out.println("New segment created: "+(new Gson()).toJson(this));
	}
	
	public void init(List<ObjectDescriptor> objs) {
		for(ObjectDescriptor obj : objs) {
			obj.initAllTasks(district);
			pushDescriptor(obj);
		}
	}
	
	protected void fireEvent(SimEvent e) {
		if(e==null)
			return;
//		System.out.println((new Gson()).toJson(e));
		district.fireEvent(e);
	}
	
	private void processTimedAction() {
		now = System.currentTimeMillis();
		while(next_event <= now) {
//			System.out.println(System.currentTimeMillis()+": Segment "+getID()+" timed action expired: "+next_event);
			for(Deque<ObjectDescriptor> queue : lanes.values())
				processQueue(queue, now);
			processQueue(ped_waiting_queue, now);
			processQueue(car_waiting_queue, now);
			processQueue(timed_queue, now);
			updateNextEvent();
		}
//		System.out.println(System.currentTimeMillis()+": Segment "+getID()+": timed actions cleared");
	}

	private void processQueue(Deque<ObjectDescriptor> q, long now) {
		ObjectDescriptor desc;
		while(q!=null && !q.isEmpty() && (desc=q.peekFirst()).curTask().expiration_time <= now) {
			q.pollFirst();
			processObjectDescriptor(desc);
		}
		return;
	}

	private void processInQueues() {
		if(do_terminate)
			return;
		for(ConcurrentLinkedQueue<ObjectDescriptor> e : in_queues.values()) {
			while(!e.isEmpty()) {
				processObjectDescriptor(e.poll());
			}
		}
//		System.out.println(System.currentTimeMillis()+": Segment "+getID()+": in queues cleared");
	}

	protected void processObjectDescriptor(ObjectDescriptor desc) {
//		log.info(System.currentTimeMillis()+"Segment "+getID()+" processing object "+desc.getId());
		switch(desc.getType()) {
		case PEDESTRIAN:
			SegmentOperations.handlePedestrian(this, desc);
			break;
		case PRIVATE_VEHICLE:
			SegmentOperations.handlePrivateTransport(this, desc);
			break;
		case PUBLIC_VEHICLE:
			SegmentOperations.handlePublicTransport(this, desc);
			break;
		}
//		log.info("Segment "+getID()+": object "+desc.getId()+" handled");
	}
	
	private void isDone() {
		if(!do_terminate)
			return;
		done = true;
		long timestamp = System.currentTimeMillis();
		for(Deque<ObjectDescriptor> queue : lanes.values())
			if(!queue.isEmpty()) {
				System.out.println("Segment "+getID()+": there are still objects traveling, waiting "+(queue.getLast().curTask().expiration_time-timestamp));
				for(ObjectDescriptor d : queue)
					System.out.println("Segment "+getID()+": "+new Gson().toJson(d));
				done = false;
			}
		if(ped_waiting_queue!=null && !ped_waiting_queue.isEmpty()) {
			System.out.println("Segment "+getID()+": there are still pedestrians waiting to cross: "+(ped_waiting_queue.getLast().curTask().expiration_time-timestamp));
			done = false;
		}
		if(car_waiting_queue!=null && !car_waiting_queue.isEmpty()) {
			System.out.println("Segment "+getID()+": there are still cars waiting to cross: "+(car_waiting_queue.getLast().curTask().expiration_time-timestamp));
			done = false;
		}
		if(!timed_queue.isEmpty()) {
			System.out.println("Segment "+getID()+": there are still objects in timed queue, waiting "+(timed_queue.getLast().curTask().expiration_time-timestamp));
			for(ObjectDescriptor d : timed_queue)
				System.out.println("Segment "+getID()+": "+new Gson().toJson(d));
			done = false;
		}
	}

	private boolean timerExpired() {
		now = System.currentTimeMillis();
		return next_event <= now;
	}
	
	protected void updateNextEvent() {
		next_event = Long.MAX_VALUE;
		for(Deque<ObjectDescriptor> queue : lanes.values()) {
			if(!queue.isEmpty())
				next_event = Math.min(next_event, queue.peek().curTask().expiration_time);
		}
		if(!timed_queue.isEmpty())
			next_event = Math.min(next_event, timed_queue.peek().curTask().expiration_time);
		if(ped_waiting_queue!=null && !ped_waiting_queue.isEmpty())
			next_event = Math.min(next_event, ped_waiting_queue.peek().curTask().expiration_time);
		if(car_waiting_queue!=null && !car_waiting_queue.isEmpty())
			next_event = Math.min(next_event, car_waiting_queue.peek().curTask().expiration_time);
	}
	
	protected void pushToTimedQueue(ObjectDescriptor desc) {
		for(ObjectDescriptor d : timed_queue)
			if(d.getId().equals(desc.getId()))
				System.out.println("ERROR: descriptor already in timed queue. "+new Gson().toJson(desc));
		timed_queue.add(desc);
		newEvent(desc.curTask());
	}
	
	protected void pushToEventQueue(ObjectDescriptor desc, String event) {
		if(!event_queue.containsKey(event))
			event_queue.put(event, new LinkedList<ObjectDescriptor>());
		event_queue.get(event).add(desc);
	}
	
	protected Queue<ObjectDescriptor> getEventQueue(String event) {
		if(!event_queue.containsKey(event))
			return new LinkedList<ObjectDescriptor>();
		return event_queue.get(event);
	}
	
	protected Deque<ObjectDescriptor> getLane(ObjectDescriptor.Type type) {
		return lanes.get(type);
	}
	
	protected List<Deque<ObjectDescriptor>> getConcurrentLanes(ObjectDescriptor.Type type) {
		List<Deque<ObjectDescriptor>> res = new LinkedList<Deque<ObjectDescriptor>>();
		if (type.equals(Type.PEDESTRIAN)) {
			res.add(lanes.get(Type.PRIVATE_VEHICLE));
			res.add(lanes.get(Type.PUBLIC_VEHICLE));
		} else
			res.add(lanes.get(Type.PEDESTRIAN));
		return res;
	}
	
	protected Deque<ObjectDescriptor> getWaitingQueue(ObjectDescriptor.Type type) {
		switch (type) {
		case PEDESTRIAN:
			return ped_waiting_queue;
		default:
			return car_waiting_queue;
		}
	}
	
	protected Deque<ObjectDescriptor> getConcurrentWaitingQueue(ObjectDescriptor.Type type) {
		if (type.equals(Type.PEDESTRIAN)) {
			return car_waiting_queue;
		} else
			return ped_waiting_queue;
	}
	
	public void newEvent(Task task) {
//		System.out.println("task expiration: "+task.expiration_time+"; next event: "+next_event);
		if(task.expiration_time < next_event)
			next_event = task.expiration_time;
	}

	public ISegment getSegment(String seg_id, String dist, Task task) {
		if(task == null)
			return district.getSegment(seg_id, dist, null);
		ISegment res = null;
		while((res = district.getSegment(seg_id, dist, task.peekNextNode()))==null && task.resolved_path!=null)	{//skip pseudo edges
			seg_id = task.getNextNode();
		}
		if(res == null)
			log.severe("Critical error while getting segment: cannot find specified segment id");
		return res;
	}
	
	public void resolvePath(String zone, String dst_id, ObjectDescriptor desc) {
		district.resolvePath(getID(), zone, dst_id, desc);
	}

	public void start() {
		if(!started) {
			worker_thread.start();
			started = true;
		}
	}
	
	public void join() throws InterruptedException {
		worker_thread.join();
	}

	public void terminate() {
		do_terminate = true;
		done = true;
		synchronized (queue_signal) {
			queue_signal.notifyAll();
		}
	}
	
	@Override
	public void pushAll(Queue<ObjectDescriptor> queue) {
		pushAll(in_queues.entrySet().iterator().next().getKey(), queue);
	}

	@Override
	public void pushAll(String in_edge, Queue<ObjectDescriptor> queue) {
		if(in_edge == null || in_edge.equals("") || !in_queues.containsKey(in_edge)) {
			pushAll(queue);
			return;
		}
		in_queues.get(in_edge).addAll(queue);
//		System.out.println("getting lock");
		synchronized (queue_signal) {
			queue_dirty = true;
			queue_signal.notifyAll();
		}
//		System.out.println("segment "+getID()+"signaled, releasing lock");
	}

	@Override
	public void pushDescriptor(ObjectDescriptor desc){
		pushDescriptor(in_queues.entrySet().iterator().next().getKey(), desc);
	}

	@Override
	public void pushDescriptor(String in_edge, ObjectDescriptor desc) {
//		System.out.println("descriptor "+desc.getId()+" pushed from "+in_edge);
		if(in_edge == null || in_edge.equals("") || !in_queues.containsKey(in_edge))
			in_edge = in_queues.entrySet().iterator().next().getKey();
		in_queues.get(in_edge).add(desc);
//		System.out.println("getting lock");
		synchronized (queue_signal) {
			queue_dirty = true;
			queue_signal.notifyAll();
		}
//		System.out.println("segment signaled, releasing lock");
	}
	
	public String getID() {
		return id;
	}
	
	public String getPoiType() {
		return poiType;
	}
	
	public boolean isMutuallyEx() {
		return mutually_exclusive;
	}

	public boolean needsAdditionalDesc() {
		return false;
	}
	
	public int getCrossingTime(ObjectDescriptor desc) {
		int res = (length/desc.getCurSpeed())*100;
		if(res > 25000)
			System.out.println("ERROR: segment "+getID()+"crossing time "+res+" for descriptor "+desc.getId()+"; desc speed "+desc.getCurSpeed()+", max desc speed "+desc.getMaxSpeed());
		return res;
	}
	
	public int getCost() {
		return length/max_speed;
	}

	@Override
	public boolean canCross(Type type) {
		switch(type) {
		case PEDESTRIAN: return lanes.containsKey(Type.PEDESTRIAN);
		case PRIVATE_VEHICLE: return lanes.containsKey(Type.PRIVATE_VEHICLE);
		case PUBLIC_VEHICLE: return lanes.containsKey(Type.PUBLIC_VEHICLE);
		default: return false;
		}
	}
}
