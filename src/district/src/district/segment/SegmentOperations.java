package district.segment;

import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Logger;

import com.google.gson.Gson;

import district.events.DescriptionBuilder;
import district.events.SimEvent;
import district.objects.ObjectDescriptor;
import district.objects.ObjectDescriptor.Type;
import district.objects.Task;

public class SegmentOperations {
	private static final Logger log = Logger.getLogger(SegmentOperations.class.getName());

	private SegmentOperations(){}

	public static void handlePedestrian(Segment segment, ObjectDescriptor desc) {
		Task task =  desc.curTask();
		switch(task.type) {
		case MOVE_TO:
			moveTo(segment, desc, true, segment.getLane(Type.PEDESTRIAN), segment.getWaitingQueue(Type.PEDESTRIAN),
					segment.getConcurrentLanes(Type.PEDESTRIAN), segment.getConcurrentWaitingQueue(Type.PEDESTRIAN));
			break;
		case USE:
			//TODO: check
			SimEvent event = new SimEvent(SimEvent.TYPE_USES, desc.getId(), desc.getType().toString(), segment.poi, ObjectDescriptor.PED_MOUNT);
			ObjectDescriptor obj = segment.getEventQueue(desc.getId()).poll();
			if(obj == null) {
				log.severe("Segment "+segment.getID()+" critical Error: object "+desc.getAdditionalInfo()+" not in event queue for "+desc.getId());
				for(Queue<ObjectDescriptor> ds : segment.event_queue.values())
					for(ObjectDescriptor d : ds)
						System.out.print(d.getId()+", ");
				System.out.println("");
				desc.addTempTask(Task.newWaitForTask(segment.getID(), "", desc.getAdditionalInfo()));
				handlePedestrian(segment, desc);
				return;
			}
			event.human_readable_desc = DescriptionBuilder.describeAction(desc, task, false, obj, segment);
			segment.fireEvent(event);
			obj.shareTask(Task.newMoveTask(task.dest_id, task.dest_zone, null, true));	//share task with car
			//			String dest_node = desc.curTask().dest_id, dest_zone = desc.curTask().dest_zone;
			Task wait_for_car = Task.newWaitForTask(task.dest_id, task.dest_zone, desc.getAdditionalInfo());
			desc.taskCompleted();// Use task completed;
			desc.addTempTask(wait_for_car);
			ISegment dest = segment.getSegment(task.dest_id, task.dest_zone, null);
			dest.pushDescriptor(desc); //push to destination segment, where it will be added to event queue
			obj.addTempTask(Task.newWaitTask(ObjectDescriptor.PED_MOUNT));
			handlePrivateTransport(segment, obj);
			break;
		case WAIT_FOR_TIME:
			waitFor(segment, desc, task);
			break;
		case WAIT_FOR_EVENT:
			String tmp = DescriptionBuilder.describeAction(desc, task, !task.temp_task, null, segment);
			if(!task.completed_flag)
				task.completed_flag = true;
			segment.pushToEventQueue(desc, task.event_id);
			SimEvent e = new SimEvent(SimEvent.TYPE_WAIT_FOR_EVENT, desc.getId(), desc.getType().toString(), task.event_id, 0);
			e.human_readable_desc = tmp;
			segment.fireEvent(e);
			break;
		default:
			break;
		}
	}

	public static void handlePrivateTransport(Segment segment, ObjectDescriptor desc) {
		Task task= desc.curTask();
		switch(task.type) {
		case MOVE_TO:
			moveTo(segment, desc, false, segment.getLane(Type.PRIVATE_VEHICLE), segment.getWaitingQueue(Type.PRIVATE_VEHICLE),
					segment.getConcurrentLanes(Type.PRIVATE_VEHICLE), segment.getConcurrentWaitingQueue(Type.PRIVATE_VEHICLE));
			break;
		case WAIT_FOR_EVENT:
			//TODO: check
			segment.pushToEventQueue(desc, desc.getAdditionalInfo());
			SimEvent e = new SimEvent(SimEvent.TYPE_WAIT_FOR_EVENT, desc.getId(), desc.getType().toString(), segment.poi, 0);
			e.human_readable_desc = DescriptionBuilder.describeAction(desc, task, false, null, segment);
			segment.fireEvent(e);
			desc.taskCompleted();
			ObjectDescriptor obj;
			while(!segment.getEventQueue(desc.getId()).isEmpty()) {
				obj = segment.getEventQueue(desc.getId()).poll();
				obj.taskCompleted();
				obj.addTempTask(Task.newWaitTask(ObjectDescriptor.PED_MOUNT));
				handlePedestrian(segment, obj);
			}
			break;
		case WAIT_FOR_TIME:
			waitFor(segment, desc, task);
			break;
		default:
			break;
		}
	}

	public static void handlePublicTransport(Segment segment, ObjectDescriptor desc) {
		Task task = desc.curTask();
		switch(task.type) {
		case DO_STOP:
			if(segment.getEventQueue(desc.getAdditionalInfo()).isEmpty() &&
					segment.getEventQueue(desc.getAdditionalInfo()+":"+desc.getId()).isEmpty()) {	//no pedestrian needs to mount or alight here. Move on
				desc.taskCompleted();
				handlePublicTransport(segment, desc);
				return;
			}
			if(!task.completed_flag) {
				task.duration = Math.min((segment.getEventQueue(desc.getAdditionalInfo()).size() + segment.getEventQueue(desc.getAdditionalInfo()+":"+desc.getId()).size()) 
						* ObjectDescriptor.PED_MOUNT, ObjectDescriptor.MAX_BUS_STOP_LENGTH);
				List<SimEvent> ped_events = boardPedestrians(segment, desc);
				SimEvent board_event = new SimEvent(SimEvent.TYPE_DO_STOP, desc.getId(), desc.getType().toString(), segment.poi, task.duration);
				board_event.human_readable_desc = DescriptionBuilder.describeAction(desc, task, true, null, segment);
				segment.fireEvent(board_event);
				for(SimEvent e : ped_events)
					segment.fireEvent(e);
				updateExpiration(task, 0);
				segment.pushToTimedQueue(desc);
				task.completed_flag = true;
			} else {
				movePedestrians(segment, desc);
				desc.taskCompleted();
				handlePublicTransport(segment, desc);
			}
			break;
		case MOVE_TO:
			moveTo(segment, desc, false, segment.getLane(Type.PUBLIC_VEHICLE), segment.getWaitingQueue(Type.PRIVATE_VEHICLE),
					segment.getConcurrentLanes(Type.PUBLIC_VEHICLE), segment.getConcurrentWaitingQueue(Type.PUBLIC_VEHICLE));
			break;
		case WAIT_FOR_TIME:
			waitFor(segment, desc, task);
			break;
		default:
			log.severe("handlePublicTransport unknown action: "+task.type);
			break;
		}
	}

	public static void updateExpiration(Task task, long suppl_duration) {
		long now = System.currentTimeMillis();
		task.expiration_time = now + suppl_duration + task.duration;
		task.duration = suppl_duration + task.duration;
		//		System.out.println("now: "+now+"; duration: "+suppl_duration+"; expiration_time: "+task.expiration_time);
	}

	private static void moveTo(Segment segment, ObjectDescriptor desc, boolean is_ped, Deque<ObjectDescriptor> dest_queue, Deque<ObjectDescriptor> wait_queue,
			List<Deque<ObjectDescriptor>> cunc_queue, Deque<ObjectDescriptor> cunc_waiting_queue) {
		//		System.out.println("moveTo "+desc.getId()+": "+desc.getType().toString());
		//		System.out.println(__debug.toJson(desc));
		Task task = desc.curTask();
		if(segment.getID().equals(task.dest_id)) {	//destination reached, next task
			//			System.out.println(desc.getId()+": destination reached, current task completed");
			desc.taskCompleted();
			segment.processObjectDescriptor(desc);
			return;
		}
		if(!task.isPathResolved()) {	//we're not at destination, do we have a path to reach it?
			if(task.dest_id==null)
				System.out.println("ERROR: null dest_id: "+new Gson().toJson(desc));
			segment.resolvePath(task.dest_zone, task.dest_id, desc);
		}
		task = desc.curTask();
		if(!task.completed_flag) {	//if flag == false, the object still needs to travel the segment
			//			System.out.println(desc.getId()+": segment not traveled yet, need to travel it");
			SimEvent e = travelSegment(segment, desc, is_ped, dest_queue, wait_queue, cunc_queue, cunc_waiting_queue);
			//			SimEvent e = new SimEvent(SimEvent.TYPE_ENTERS, desc.getId(), desc.getType().toString(), segment.id+"-"+task.peekNextNode(), task.duration);
			//			System.out.println((new Gson()).toJson(task));
			segment.fireEvent(e);
			return;
		}
		//		System.out.println(desc.getId()+": segment already traveled, moving to next node "+task.peekNextNode());
		//segment already traveled, move on
		task.completed_flag = false;	//reset for next segment
		String next = task.getNextNode();
		if(next != null) {
			segment.getSegment(next, "", task).pushDescriptor(segment.getID(), desc);
			desc.setCurNode(task.peekNode(-1));
			segment.fireEvent(new SimEvent(SimEvent.TYPE_LEAVES, desc.getId(), desc.getType().toString(), segment.getID()+"-"+next, 0));
			return;
		} else
			log.severe("ERROR: next hop for objecbt "+desc.getId()+" is null; " + (new Gson()).toJson(desc.curTask()));
	}

	private static void waitFor(Segment segment, ObjectDescriptor desc, Task task) {
		if(!task.completed_flag) {
			task.completed_flag = true;
			updateExpiration(task, 0);
			if(task.expiration_time>System.currentTimeMillis()+100000)
				System.out.println("ERROR: invalid expiration time:\n"+new Gson().toJson(desc));
			segment.pushToTimedQueue(desc);
			SimEvent e = new SimEvent(SimEvent.TYPE_WAIT, desc.getId(), desc.getType().toString(), segment.poi, task.duration);
			e.human_readable_desc = DescriptionBuilder.describeAction(desc, task, false, null, segment);
			segment.fireEvent(e);
			return;
		}
		desc.taskCompleted();
		segment.processObjectDescriptor(desc);
	}

	private static SimEvent travelSegment(Segment segment, ObjectDescriptor desc, boolean is_ped, Deque<ObjectDescriptor> dest_queue, Deque<ObjectDescriptor> wait_queue,
			List<Deque<ObjectDescriptor>> cunc_queue, Deque<ObjectDescriptor> cunc_waiting_queue) {
		Task task = desc.curTask();
		task.duration = 0;
		long time = crossingTime(segment, desc, dest_queue);
		updateExpiration(task, time);
		if(segment.mutually_exclusive && !canCross(segment, desc, time, is_ped, cunc_queue, cunc_waiting_queue)) {
			wait_queue.add(desc);
			segment.newEvent(task);
			//			SimEvent e = new SimEvent(SimEvent.TYPE_WAIT, desc.getId(), desc.getType().toString(), null, task.duration);
			//			e.human_readable_desc = DescriptionBuilder.describeAction(desc, Task.Type.WAIT_FOR_TIME, null, false, null, segment);
			return null;
		}
		dest_queue.add(desc);
		task.completed_flag = true;
		segment.newEvent(task);
		SimEvent e = new SimEvent(SimEvent.TYPE_ENTERS, desc.getId(), desc.getType().toString(), segment.getID()+"-"+task.peekNode(task.cross_flag?1:0), task.duration);
		if(segment.needsAdditionalDesc()) {
			e.target_id = desc.getCurNode()+"-"+task.peekNextNode();
			e.additional_desc = segment.getID();
		}
		task.cross_flag = segment.mutually_exclusive && !task.cross_flag && is_ped;
		e.human_readable_desc = DescriptionBuilder.describeAction(desc, task, segment.needsAdditionalDesc(), null, segment);
		return e;
	}

	private static boolean canCross(Segment segment, ObjectDescriptor desc, long crossing_time, boolean is_pedestrian, List<Deque<ObjectDescriptor>> others_crossing, Deque<ObjectDescriptor> others_waiting) {
		long now = System.currentTimeMillis();
		desc.curTask().duration = 0;
		boolean other_line_free = true;
		long max_wait = 0;
		for(Deque<ObjectDescriptor> q : others_crossing) {
			if(!q.isEmpty()) {
				other_line_free = false;
				max_wait = Math.max(max_wait, q.getLast().curTask().expiration_time-now);
			}
		}
		if(other_line_free  && others_waiting.isEmpty()) { //no one is crossing and no one is waiting to cross, take priority regardless of turns
			segment.pedestrinas_crossing = is_pedestrian;
			segment.started_waiting = now;
			updateExpiration(desc.curTask(), crossing_time);
			return true;
		}
		if(is_pedestrian != segment.pedestrinas_crossing)
			avoidStarvation(segment, crossing_time, is_pedestrian, others_crossing);
		//either someone is crossing or is waiting in queue
		if(is_pedestrian == segment.pedestrinas_crossing) { //is it our turn to cross?
			if(!other_line_free) { //it's our turn, but the crossing is occupied. We need to wait for last object to cross
				System.out.println("This id: "+desc.getId()+", crossing occupied. Waiting "+max_wait);
				updateExpiration(desc.curTask(), max_wait);
				return false;
			}
			updateExpiration(desc.curTask(), crossing_time);
			return true;
		}
		//we need to wait the minimum between the time until our turn and the time until no one is crossing (and we can relinquish priority) 
		long waiting_time = Math.min(segment.started_waiting+Segment.MAX_CROSSING_WAITING-now, max_wait != 0 ? max_wait : Long.MAX_VALUE);
		System.out.println("This id: "+desc.getId()+"; started w: "+(segment.started_waiting-now)+"; other crossing: "+(max_wait != 0 ? max_wait : 0)+"; waiting for "+waiting_time);
		updateExpiration(desc.curTask(), waiting_time);
		return false;
	}

	private static List<SimEvent> boardPedestrians(Segment segment, ObjectDescriptor desc) {
		String event_id = desc.getAdditionalInfo();
		String bus_id = desc.getId();
		Queue<ObjectDescriptor> queue = segment.getEventQueue(event_id);
		SimEvent event1 = new SimEvent(SimEvent.TYPE_USES, ObjectDescriptor.Type.PEDESTRIAN.toString(), bus_id, ObjectDescriptor.PED_MOUNT);
		SimEvent event2 = new SimEvent(SimEvent.TYPE_USES, ObjectDescriptor.Type.PEDESTRIAN.toString(), bus_id, ObjectDescriptor.PED_MOUNT);
		for(ObjectDescriptor ped : queue) {
				event1.obj_id.add(ped.getId());
		}
		event1.human_readable_desc = DescriptionBuilder.describeAction(null, Task.Type.USE, null, true, desc, segment);
		queue = segment.getEventQueue(event_id+":"+bus_id);
		for(ObjectDescriptor ped : queue) {
				event2.obj_id.add(ped.getId());
		}
		event2.human_readable_desc = DescriptionBuilder.describeAction(null, Task.Type.USE, null, true, null, segment);
		List<SimEvent> res = new ArrayList<SimEvent>();
		if(event1.obj_id.size()>0) res.add(event1);
		if(event2.obj_id.size()>0) res.add(event2);
		return res;
	}

	private static void movePedestrians(Segment segment, ObjectDescriptor desc) {
		String event_id = desc.getAdditionalInfo();
		Queue<ObjectDescriptor> alighting = segment.getEventQueue(event_id+":"+desc.getId());
		Queue<ObjectDescriptor> boarding = segment.getEventQueue(event_id);
		Map<String, Queue<ObjectDescriptor>> destinations = new HashMap<String, Queue<ObjectDescriptor>>();
		ObjectDescriptor ped;
		while((ped=alighting.poll()) != null) {
			ped.taskCompleted();
			handlePedestrian(segment, ped);
		}
		while((ped=boarding.poll()) != null) {
			//			if(segment.getID().equals(ped.curTask().dest_id)) {
			//				ped.taskCompleted();
			//				handlePedestrian(segment, ped); //destination reached, handle next task
			//			}else {
			ped.curTask().event_id = ped.curTask().event_id+":"+desc.getId();
			if(!destinations.containsKey(ped.curTask().dest_id))
				destinations.put(ped.curTask().dest_id, new LinkedList<ObjectDescriptor>());
			destinations.get(ped.curTask().dest_id).add(ped);
			//			}
		}
		for(Map.Entry<String, Queue<ObjectDescriptor>> e : destinations.entrySet()) {
			segment.getSegment(e.getKey(), e.getValue().peek().curTask().dest_zone, null).pushAll(e.getValue()); //move to appropriate destination
		}
	}

	private static long crossingTime(Segment segment, ObjectDescriptor desc, Deque<ObjectDescriptor> queue) {
		int speed = Math.min(desc.getMaxSpeed(), segment.max_speed);
		if(!queue.isEmpty())
			speed = Math.min(speed, queue.getLast().getCurSpeed());
		desc.setCurSpeed(speed);
		return segment.getCrossingTime(desc);
	}

	//check if maximum waiting time has been reached. If so, give priority to the currently waiting queue
	private static void avoidStarvation(Segment segment, long crossing_time, boolean is_pedestrian, List<Deque<ObjectDescriptor>> others_crossing) {
		long now = (new Date()).getTime();
		System.out.println("is_ped: "+is_pedestrian+"; ped_crossing: "+segment.pedestrinas_crossing+"; started w: "+segment.started_waiting);
		if(now >= segment.started_waiting+Segment.MAX_CROSSING_WAITING) {
			segment.pedestrinas_crossing = !segment.pedestrinas_crossing;
			//if there are objects still crossing, we need to take it into account
			long additional_waiting_time = 0;
			for(Deque<ObjectDescriptor> q : others_crossing) {
				additional_waiting_time = q.isEmpty() ? additional_waiting_time : Math.max(additional_waiting_time, q.getLast().curTask().expiration_time-now);
			}
			segment.started_waiting = now + additional_waiting_time;
			System.out.println("ped_crossing: "+segment.pedestrinas_crossing+"; started w: "+segment.started_waiting);
		}
	}
}

