package server;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import server.definitions.Constants;
import server.definitions.Services;
import server.yami.Converter;

import com.inspirel.yami.Agent;
import com.inspirel.yami.IncomingMessage;
import com.inspirel.yami.IncomingMessageCallback;
import com.inspirel.yami.Parameters;
import com.inspirel.yami.YAMIIOException;

public class NameServer implements IncomingMessageCallback {
	private final static Logger log = Logger.getLogger(NameServer.class.getName());
	
	private final Object signal_object = new Object();
	private Boolean terminated = false;
	private Map<String, String> table;
	private Agent agent;
	private String resolvedTarget;
	
	public NameServer(String address) {
		try {
			table = new ConcurrentHashMap<String, String>();
			agent = new Agent();
			resolvedTarget = agent.addListener(address);
			agent.registerObject(Constants.NAME_SERVER, this);
			log.info("Name server running at "+resolvedTarget);
			synchronized(signal_object) {
				while(!terminated)
					signal_object.wait();
			}
				
		} catch (YAMIIOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void respondSuccess(IncomingMessage msg) throws YAMIIOException {
		Parameters reply = new Parameters();
		reply.setString("response", Constants.SUCCESS);
		msg.reply(reply);
	}
	
	private void respondFailure(IncomingMessage msg) throws YAMIIOException {
		Parameters reply = new Parameters();
		reply.setString("response", Constants.FAILURE);
		msg.reply(reply);
	}

	@Override
	public void call(IncomingMessage msg) throws Exception {
		Parameters response;
		String nodeID = msg.getParameters().getString("host_id");
		switch(msg.getMessageName()) {
		case Services.NAME_SERVER_BIND:
			String nodeAddr = msg.getParameters().getString("addr");
			table.put(nodeID, nodeAddr);
			log.info(nodeID+" bound at address "+nodeAddr);
			respondSuccess(msg);
			break;
			
		case Services.NAME_SERVER_RELEASE:
			if(table.remove(nodeID) != null) {
				log.info(nodeID+" removed from name server.");
				respondSuccess(msg);
			}
			else {
				log.info(nodeID+" not in table, can not remove.");
				respondFailure(msg);
			}
			break;
			
		case Services.NAME_SERVER_LIST:
			response = Converter.mapToParams(table);
			response.setString("response", Constants.SUCCESS);
			msg.reply(response);
			break;
			
		case Services.NAME_SERVER_RESOLVE:
			response = new Parameters();
			String addr = table.get(nodeID);
			if(addr != null)
				response.setString("response", Constants.SUCCESS);
			else
				response.setString("response", Constants.FAILURE);
			response.setString("resolved_address", addr);
			msg.reply(response);
			break;
			
		case Services.TERMINATE:
			terminated = true;
			synchronized(signal_object) {
				signal_object.notifyAll();
			}
			break;
			
		default:
			log.warning("Unknown command: "+msg.getMessageName());
			break;
		}
	}
	
	public static void main(String [ ] args) {
		String addr = "tcp://";
		if(args.length > 0)
			addr = addr.concat(args[0]);
		else
			addr = addr.concat("*");
		new NameServer(addr);
	}
}
