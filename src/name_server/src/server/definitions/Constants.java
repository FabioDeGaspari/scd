package server.definitions;

public final class Constants {
	public static final String VERSION = "0.0";
	public static final String NAME_SERVER = "name_server";
	public static final String SUCCESS = "response_success";
	public static final String FAILURE = "response_failure";

	private Constants(){}
}
