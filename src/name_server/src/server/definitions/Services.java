package server.definitions;

public class Services {
	public static final String NAME_SERVER_BIND = "bind";
	public static final String NAME_SERVER_LIST = "list";
	public static final String NAME_SERVER_RELEASE = "release";
	public static final String NAME_SERVER_RESOLVE = "resolve";
	public static final String TERMINATE = "terminate";
	
	private Services(){};

}
