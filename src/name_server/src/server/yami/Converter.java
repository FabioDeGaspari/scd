package server.yami;

import java.util.HashMap;
import java.util.Map;

import com.inspirel.yami.Parameters;

public final class Converter {
	public static Map<String, String> paramsToMap(Parameters args) {
		Map<String, String>  ret = new HashMap<String, String>();
		for(Parameters.Entry entry : args) {
			ret.put(entry.name(), entry.getString());
		}
		return ret;
	}
	
	public static Parameters mapToParams(Map<String, String> map) {
		Parameters ret = new Parameters();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			ret.setString(entry.getKey(), entry.getValue());
		}
		return ret;
	}
	
	private Converter(){}
}
