package controllers

import scala.Left
import scala.Right
import scala.concurrent.Future
import models.ControllerInterface
import play.api.Play.current
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.mvc.WebSocket

object Application extends Controller {

	val UID = "uid";
	var id = 0;

	def index = Action { implicit request =>
	  val uid = request.session.get(UID).getOrElse {
  		id += 1
			id.toString
	  }
	  Ok(views.html.index(uid)).withSession(request.session + (UID -> uid)) 
	}

	def webSocket = WebSocket.tryAcceptWithActor[String, String] { implicit request =>  
     Future.successful(request.session.get(UID) match {
       case None => Left(Forbidden)
       case Some(uid) => Right(ControllerInterface.props(uid))
     })
	}
}