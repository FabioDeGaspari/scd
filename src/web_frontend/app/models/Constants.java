package models;

public final class Constants {
	// Constants Version
	public static final String VERSION = "0.0";
	
	// System Objects Constants
	public static final String NAME_SERVER = "name_server";
	public static final String DISTRICT = "district";
	public static final String ROUTER = "router";
	public static final String CONTROLLER = "controller";
	public static final String SUBSCRIBER = "subscriber";
	
	//System Response Constants
	public static final String SUCCESS = "response_success";
	public static final String FAILURE = "response_failure";
	
	// Event Filtering Constants
	public static final String EVENT_SOURCE_FILTER = "source_filter";
	public static final String EVENT_OBJECT_FILTER = "object_filter";

	private Constants(){}
}
