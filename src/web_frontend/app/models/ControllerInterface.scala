package models

import akka.actor._
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import com.google.gson.Gson


/**
 * @author fabio
 */

object ControllerInterface {
  def props(id : String)(out : ActorRef) = Props(new ControllerInterface(id, out))
}

class ControllerInterface(val id : String, val out : ActorRef) extends Actor {
  val subscriber = new SubscriberAgent
  val gson = new Gson
  
  override def preStart() = {
    subscriber.init
    subscriber.subscribe(id, self);
  }
  
  def receive = {
    case msg : Event => out ! msg.e
    case msg : String => {
      val cmd = gson.fromJson(msg, classOf[Command])
      cmd.cmd_type match {
        case Command.START =>
        case Command.STOP =>
        case Command.INTEREST => subscriber.updateInterest(cmd.interest)
        case Command.DEBUG => {
          out ! (cmd.interest.id+" said: "+cmd.interest.interests(0))
          println(cmd.interest.id+" said: "+cmd.interest.interests(0))
        }
      } 
    }
  }
  
  override def postStop() = {
    subscriber.unsubscribe(id);
    subscriber.close
    println("Stopping actor")
  }
}