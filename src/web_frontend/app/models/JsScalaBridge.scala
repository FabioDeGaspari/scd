package models

object Command {
  val START = 1
  val STOP = 2
  val INTEREST = 3
  val DEBUG = -1
}

class Command(val cmd_type : Int, val interest : Interest = null)

object Interest {
  val UNSUBSCRIBE = -1
  val COARSE = 0
  val FINE = 1
}

class Interest(
    val id : String,
    val interests : Array[String] = Array(),
    val lost_interests : Array[String] = Array(),
    val level : Int
)