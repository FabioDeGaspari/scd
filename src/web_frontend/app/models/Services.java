package models;

public class Services {
	public static final String NAME_SERVER_BIND = "bind";
	public static final String NAME_SERVER_LIST = "list";
	public static final String NAME_SERVER_RELEASE = "release";
	public static final String NAME_SERVER_RESOLVE = "resolve";
	public static final String DISTRICT_LIST_EDGE_NODES = "list_edge_nodes";
	public static final String DISTRICT_NOTIFY_EDGE_NODES = "notify_edge_nodes";
	public static final String DISTRICT_CONTAINS_NODE = "contains_node";
	public static final String ROUTER_STATE_UPDATE = "advertise";
	public static final String ROUTER_REQUEST_STATE_UPDATE = "advertise_request";
	public static final String CONTROLLER_EVENT = "event";
	public static final String CONTROLLER_BEGIN = "begin";
	public static final String CONTROLLER_TERMINATE = "terminate";
	public static final String SUBSCRIPTION_UPDATE = "subscribe";
	public static final String TERMINATE = "terminate";
	
	private Services(){};

}
