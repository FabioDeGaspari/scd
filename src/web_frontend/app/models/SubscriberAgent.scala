package models

import scala.collection.mutable.Map
import com.inspirel.yami.Agent
import com.inspirel.yami.IncomingMessage
import com.inspirel.yami.IncomingMessageCallback
import com.inspirel.yami.Parameters
import com.inspirel.yami.YAMIIOException
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.actorRef2Scala
import akka.actor.Actor


/**
 * @author fabio
 */

case class Init()

//object SubscriberAgent {
//  lazy val broker = new SubscriberAgent
//  def apply() = broker
//}

class SubscriberAgent extends IncomingMessageCallback {
  val controllerAddress = "tcp://localhost:4444"
  var agent : Agent = new Agent
  var address = ""
  var subscribers : Map[String, ActorRef] = Map()
  
  def subscribe(id : String, ref : ActorRef) = {
    subscribers += (id -> ref);
    println("subscriber "+id+" joined")
  }
  
  def unsubscribe(id : String) = {
    subscribers -= id
    updateInterest(new Interest(id, Array(), Array(), Interest.UNSUBSCRIBE))
    println("subscriber "+id+" left")
  }
  
  def updateInterest(interest : Interest) = {
    val params = new Parameters
    params.setString("address", address)
    params.setString("host_id", interest.id)
    params.setStringArray("interests", interest.interests)
    params.setStringArray("lost_interests", interest.lost_interests)
    params.setInteger("interest_level", interest.level)
    try{
      val res = agent.send(controllerAddress, Constants.CONTROLLER, Services.SUBSCRIPTION_UPDATE, params)
      if(interest.level == Interest.UNSUBSCRIBE)
        res.waitForTransmission()
//      println("Interest update sent")
    } catch {
      case e : YAMIIOException => println(e.getMessage)
    }
  }
  
  def init = {
    address = agent.addListener("tcp://*")
    println("subscriber agent running at "+address)
    agent.registerObject(Constants.SUBSCRIBER, this)
  }
  
  def close = {
    agent.close()
  }
  
  override def call(msg : IncomingMessage) = {
    val param = msg.getParameters
    val subscriber = param.getString("subscriber")
//    println("Received new "+ msg.getMessageName + "message for "+subscriber)
    msg.getMessageName match {
      case "event" => {
//        println(param.getString("event"))
        subscribers(subscriber) ! Event(param.getString("event"))
      }
      case "terminate" => subscribers(subscriber) ! PoisonPill
    }
  }
}

case class Event(val e : String)
