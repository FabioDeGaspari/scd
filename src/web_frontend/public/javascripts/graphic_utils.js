
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			GRAPHIC UTILITIES					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

;(function() {
	  Raphael.fn.addGuides = function() {
	    this.ca.guide = function(g) {
	      return {
	        guide: g
	      };
	    };
	    this.ca.along = function(percent) {
	      var g = this.attr("guide");
//	      var p = this.getBBox(true)
	      var len = raphael.getTotalLength(g);
	      var point = raphael.getPointAtLength(g, percent * len);
	      var t = {
	        transform: "t" + (point.x) + " " + (point.y) + "r" + point.alpha
	      };
	      return t;
	    };
	  };
	})();


//			RAPHAEL SETUP		//
var container = document.getElementById("map");
var raphael_canvas = new Raphael('map', container.clientHight, container.clientWidth);
var raphael = raphael_canvas.raphael
raphael_canvas.addGuides()
var panZoom = raphael_canvas.panzoom({ initialZoom: 5, initialPosition: { x: 120, y: 70}, minZoom: 5});
panZoom.enable();
var container = raphael_canvas.canvas.parentNode

var GraphicUtils = {
		toDegrees : function(theta) {
			return theta * (180/Math.PI);
		},
		
		getDistance : function(x, y, x1, y1) {
			return Math.sqrt(Math.pow(x-x1, 2) + Math.pow(y-y1, 2));
		},
		
		rotate : function(elem, theta, piv_x, piv_y) {
			elem.attr({
				transform : ["r",(theta * (180/Math.PI)), piv_x, piv_y]
			});
		},
		
		angleBetween : function(x1, y1, x2, y2) {
			return Math.atan2(y2-y1, x2-x1);
		},
		
		rotatedPoint : function(x, y, theta, piv_x, piv_y) {
			return {"x" : Math.cos(theta)*(x-piv_x) - Math.sin(theta)*(y-piv_y) + piv_x,
				"y": Math.sin(theta)*(x-piv_x) + Math.cos(theta)*(y-piv_y) + piv_y}
		},
		
		//either (x, y, theta, distance) or (point, theta, distance)
		pointAtDistance : function(x, y, theta, distance) {
			if(typeof distance === "undefined") {
				distance = theta
				theta = y
				y = x.y
				x = x.x
			}
			return { "x" : x+(distance*Math.cos(theta)), 
				"y" : y+(distance*Math.sin(theta))}
		},
		
		padLine : function(theta, height) {
			if(theta != 0)
				return (height) * Math.tan(theta/2);
			return 0;
		},
		
		animateAlong : function(elem, path, duration, callback) {
			elem.attr({guide : path, along : 0}).animate({along : 1}, duration, "linear", callback)
		},
		
		getBBox : function(x, y, width, height) {
			return {x: x, y: y, width: width, height: height}
		},
		
		getScaledBBox : function (x, y, width, height) {
			x = x*SCALE
			y = y*SCALE
			width = width*SCALE
			height = height*SCALE
			var t = {x: x, y: y, width: width, height: height}
			raphael_canvas.rect(x, y, width, height).attr({'stroke-dasharray': '--', 'stroke': DistColours[NextColour]})
			NextColour = (NextColour+1)%DistColours.length
//			console.log(t)
			return t
		},
		
		doBoxesIntersect : function(a, b) {
			return (a.x+a.width>b.x && b.x+b.width>a.x && a.y+a.height>b.y && b.y+b.height>a.y)
		},
		
		render : function(now) {
			for(idx in to_animate) {
				progress = Math.min(to_animate[idx].duration/(now-to_animate[idx].start_anim), 100)
				var p = raphael.getPointAtLength(to_animate[idx].animpath, raphael.getTotalLength(to_animate[idx].animpath)*progress)
				to_animate[idx].img.transform("t"+p.x+" "+p.y)
				if(progress==100)
					to_animate[idx].remove()
			}
			window.requestAnimationFrame(GraphicUtils.render)
		}
};

var to_animate = []

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			GRAPHIC CONSTANTS					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var IMGS = '/assets/images/'
var PoiType = Object.freeze({"MALL":0, "UNIVERSITY":1, "PARKING":2, "BUS_STOP":3, "PUB":4, "BANK":5, "HOME":6, "POST":7, "AMUSEMENT_PARK":8, "SPORT_CENTER":9, "SHOP":10, "CROSSROAD":98, "none":99})
var PoiRes = [IMGS+"market.svg", IMGS+"university.svg", IMGS+"parking.svg", IMGS+"bus_stop.svg", IMGS+"pub.svg", IMGS+"bank.svg", IMGS+"home.svg", IMGS+"post.svg", IMGS+"amusement_park.svg", IMGS+"sport.svg", IMGS+"market.svg"]
var DontDraw = Object.freeze({98:0, 99:0})
var DistColours = ['#FF0000', '#0000FF', '#33CC33', '#000000', '#FF9900', '#CC0099', '#C5C545']
var NextColour = 0
var SCALE = 0.5
var SUPERSEG_HEIGHT = 30*SCALE
var MAX_INTERPOLATION_LENGTH = SUPERSEG_HEIGHT*2
var MIN_INTERPOLATION_LENGTH = SUPERSEG_HEIGHT/3
var POI_SIZE = (SUPERSEG_HEIGHT*1.5)*SCALE
var POI_PADDING_VER = POI_SIZE/10
var POI_PADDING_HOR = POI_SIZE/5
var CROSSING_WIDTH = 0.2
var CROSSING_SEG_WIDTH = (1-CROSSING_WIDTH)/2
var MIN_POI_MOVETO_DUR = 1000


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			OBJECTS DEFINITION					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var PED_RAD = 3*SCALE
var CAR_WIDTH = 8*SCALE
var CAR_HEIGHT = 6*SCALE
var BUS_WIDTH = 10*SCALE
var BUS_HEIGHT = 8*SCALE

function GraphicObject(canvas, json_obj, x, y, poi) {
	switch(json_obj.type) {
	case "PRIVATE_VEHICLE":
		this.img = canvas.image(IMGS+"car.svg", x, y, CAR_WIDTH, CAR_HEIGHT)//.attr({'fill': 'blue'})//circle(x, y, PED_RAD*2).attr({'stroke-width':'0', 'fill':'blue'})
		break
	case "PUBLIC_VEHICLE":
		this.img = canvas.image(IMGS+"bus.svg", x, y, BUS_WIDTH, BUS_HEIGHT)
		break
	default:
		this.img = canvas.circle(x, y, PED_RAD).attr({'stroke-width':'0', 'fill':'#EB0006'})
	}
	this.id = json_obj.id
	this.name = json_obj.name
	this.type = json_obj.type
	this.poi = poi;
}

GraphicObject.prototype.moveAlong = function(path, duration, progress) {
	this.img.show()
	var v_offset = 0
	var h_offset = 0
	var pos = this.getPosition()
	var base_pos = this.getBasePosition()
	var path_string = path;
	if(this.type == "PEDESTRIAN") 
		this.img.attr({cx:0, cy:0})
	else {
		this.img.attr({x:0, y:0})
		v_offset = -this.img.getBBox(true).height/2
		h_offset = -this.img.getBBox(true).width/2
		path_string = raphael.transformPath(path, ("t"+h_offset+","+v_offset))
	}
	if(progress != 0.0) {
		var start = raphael.getTotalLength(path_string)*progress
		path_string = raphael.getSubpath(path_string, start, raphael.getTotalLength(path_string))
		duration = duration*(1-progress)
	} else if(base_pos.x != 0 || base_pos.y != 0) {
		var p1 = raphael.getPointAtLength(path, 0);
		var interp_length = raphael.getTotalLength("M"+pos.x+" "+pos.y+"L"+p1.x+" "+p1.y);
		if(interp_length>= MIN_INTERPOLATION_LENGTH && interp_length <= MAX_INTERPOLATION_LENGTH) {
			path_string = "M"+pos.x+" "+pos.y+"L"+path.substring(1)
		}
	}
	GraphicUtils.animateAlong(this.img, path_string, duration)
}

GraphicObject.prototype.moveToPoint = function(point, duration, target, callback) {
	if(this.poi == target && this.poi != undefined)
		return;
	this.poi = target;
	var p = this.getPosition()
	var distance = GraphicUtils.getDistance(p.x, p.y, point.x, point.y)
	if(distance > MAX_INTERPOLATION_LENGTH) {
		this.snapTo({x:point.x, y:point.y});
		if(callback != undefined)
			callback();
		return;
	}
	this.snapTo({x:0, y:0})
	var path = "M"+p.x+" "+p.y+"L"+point.x+" "+point.y
	GraphicUtils.animateAlong(this.img, path, duration, callback)
}

GraphicObject.prototype.snapTo = function(p) {
	if(this.type == "PEDESTRIAN") 
		this.img.attr({cx:p.x, cy:p.y})
	else
		this.img.attr({x:p.x, y:p.y})
}

GraphicObject.prototype.hide = function() {
	this.img.stop()
	this.img.hide()
}

GraphicObject.prototype.getBBox = function() {
	return this.img.getBBox()
}

GraphicObject.prototype.getCenter = function() {
	return this.getPosition()
}

GraphicObject.prototype.getBasePosition = function() {
	if(this.type == "PEDESTRIAN")
		return {x:this.img.attr('cx'), y:this.img.attr('cy')}
	else
		return this.img.getBBox()
}

GraphicObject.prototype.getPosition = function() {
	return {"x": this.img.getBBox().x+this.img.getBBox().width/2, "y": this.img.getBBox().y+this.img.getBBox().height/2}
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			SEGMENT DEFINITION					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var SEG_BASE_COL = 'EBC706'
var SEG_MIN_GRAD = 0
var SEG_MAX_GRAD = 5

function Segment(canvas, from_x, from_y, to_x, to_y, s_theta, d_theta, mid, mwalkway, mcarlane, mbuslane, mpoi_type,
		sid, swalkway, scarlane, sbuslane, spoi_type, dont_scale) {
	if(dont_scale == 'undefined' || !dont_scale) {
		from_x = from_x*SCALE
		from_y = from_y*SCALE
		to_x = to_x*SCALE
		to_y = to_y*SCALE
	}
	this.paths = [];
	this.anim_paths = [];
	this.from_x = from_x;
	this.from_y = from_y;
	this.to_x = to_x;
	this.to_y = to_y;
	this.length = GraphicUtils.getDistance(from_x, from_y, to_x, to_y);
	this.size = sid == undefined ? SUPERSEG_HEIGHT/2 : SUPERSEG_HEIGHT;
	var line_spacing = sid == undefined ? SUPERSEG_HEIGHT/3 : SUPERSEG_HEIGHT/6;
	var from_rot = GraphicUtils.padLine(s_theta, this.size/2);
	var to_rot = GraphicUtils.padLine(d_theta, this.size/2);
	this.from_x1 = from_x - from_rot;
	this.from_x2 = from_x + from_rot;
	this.from_y = from_y;
	this.seg = canvas.set();
	var theta = Math.atan2(to_y-from_y, to_x-from_x);
	this.to_x1 = from_x+ this.length + to_rot;
	this.to_x2 = from_x+ this.length - to_rot;
	this.paths[0] = canvas.path("M"+this.from_x1+" "+(from_y-this.size/2)+"L"+this.to_x1+" "+(this.from_y-this.size/2)).attr({'stroke-width':1.5,'stroke':'#876E33'});
	this.paths[1] = canvas.path("M"+this.from_x2+" "+(from_y+this.size/2)+"L"+this.to_x2+" "+(this.from_y+this.size/2)).attr({'stroke-width':1.5,'stroke':'#876E33'});
	this.paths[2] = canvas.path("M"+this.from_x+" "+this.from_y+"L"+ (this.from_x + this.length)+" "+this.from_y).attr({'stroke-width':3,'stroke':'#878787'});
	this.secLane = this.addLane(canvas, sid, this.from_x1, from_y-this.size/2, this.to_x1, this.from_y-this.size/2, this.from_x+this.length, this.from_y, this.from_x, this.from_y)
	this.mainLane = this.addLane(canvas, mid, this.from_x, this.from_y, this.from_x+this.length, this.from_y, this.to_x2, this.from_y+this.size/2, this.from_x2, from_y+this.size/2)
	this.seg.push(this.mainLane)
	this.seg.push(this.secLane)
	this.seg.push(this.paths[0]);
	this.seg.push(this.paths[1]);
	this.seg.push(this.paths[2]);
	var rect_x = this.seg.getBBox().x;
	var rect_y2 = this.seg.getBBox().y2;
	var piv_x = this.from_x
	var piv_y = this.seg.getBBox().y+((this.seg.getBBox().y2-this.seg.getBBox().y)/2)
	this.addLine(canvas, true, theta, piv_x, piv_y, mid, mwalkway, mcarlane, mbuslane, mpoi_type, line_spacing, s_theta, d_theta);
	if(sid != undefined)
		this.addLine(canvas, false, theta, piv_x, piv_y, sid, swalkway, scarlane, sbuslane, spoi_type, line_spacing, s_theta, d_theta);
	GraphicUtils.rotate(this.seg, theta, piv_x, piv_y)
}

Segment.rainbow = new Rainbow()
Segment.rainbow.setNumberRange(SEG_MIN_GRAD, SEG_MAX_GRAD)
Segment.rainbow.setSpectrum(SEG_BASE_COL, 'red')

Segment.scaleColour = function(val, lane) {
	val = Math.min(Math.max(val, SEG_MIN_GRAD), SEG_MAX_GRAD);
	val  = Segment.rainbow.colourAt(val)
	lane.attr({'fill': '#'+val})
}

Segment.prototype.addLane = function(canvas, id, x1, y1, x2, y2, x3, y3, x4, y4) {
	lanes[id] = canvas.path(["M", x1, y1, "L", x2, y2, "L", x3, y3, "L", x4, y4]).attr({'stroke-width':0, 'fill': '#'+SEG_BASE_COL}).toBack();
	return lanes[id]
}

Segment.prototype.addLine = function(canvas, is_main, theta, piv_x, piv_y, id, walkway, carlane, buslane, poi_type, line_spacing, s_theta, d_theta) {
	var modifier = 2*SCALE
	var line_offset = this.seg.getBBox().y+modifier;
	var from_x = this.from_x+ this.length, to_x = this.from_x;
	var walk_s_pad = GraphicUtils.padLine(s_theta, this.size/2-modifier);
	var walk_d_pad = GraphicUtils.padLine(d_theta, this.size/2-modifier);
	var car_s_pad = GraphicUtils.padLine(s_theta, this.size/2-modifier-line_spacing);
	var car_d_pad = GraphicUtils.padLine(d_theta, this.size/2-modifier-line_spacing);
	var bus_s_pad = GraphicUtils.padLine(s_theta, this.size/2-modifier-line_spacing*2);
	var bus_d_pad = GraphicUtils.padLine(d_theta, this.size/2-modifier-line_spacing*2);
	var poi_y = this.seg.getBBox().y - POI_SIZE - POI_PADDING_VER, poi_x = from_x+walk_d_pad-POI_SIZE-POI_PADDING_HOR;
	if(is_main) {
		walk_d_pad = [walk_s_pad, walk_s_pad = walk_d_pad][0]; //swap
		car_d_pad = [car_s_pad, car_s_pad = car_d_pad][0];
		bus_d_pad = [bus_s_pad, bus_s_pad = bus_d_pad][0];
		line_spacing = -line_spacing;
		line_offset = this.seg.getBBox().y2-(2*SCALE);
		from_x = this.from_x;
		to_x = this.from_x+this.length;
		poi_y = this.seg.getBBox().y2 + POI_PADDING_VER;
		poi_x = from_x+walk_d_pad+POI_PADDING_HOR;
	}
	var p1 = GraphicUtils.rotatedPoint(from_x+walk_d_pad, line_offset, theta, piv_x, piv_y)
	var p2 = GraphicUtils.rotatedPoint(to_x-walk_s_pad, line_offset, theta, piv_x, piv_y)
	if(walkway) {
		walkways[id] = canvas.path("M"+(p1.x)+" "+p1.y+"L"+(p2.x)+" "+p2.y).attr({'stroke-width':1.5,'stroke':'#000000'});
		walkways[id].hide();
	}
	line_offset += line_spacing;
	p1 = GraphicUtils.rotatedPoint(from_x+car_d_pad, line_offset, theta, piv_x, piv_y)
	p2 = GraphicUtils.rotatedPoint(to_x-car_s_pad, line_offset, theta, piv_x, piv_y)
	if(carlane) {
		carlanes[id] = canvas.path("M"+(p1.x)+" "+p1.y+"L"+(p2.x)+" "+p2.y).attr({'stroke-width':1.5,'stroke':'#000000'});
		carlanes[id].hide();
	}
	line_offset += line_spacing;
	p1 = GraphicUtils.rotatedPoint(from_x+bus_d_pad, line_offset, theta, piv_x, piv_y)
	p2 = GraphicUtils.rotatedPoint(to_x-bus_s_pad, line_offset, theta, piv_x, piv_y)
	if(buslane) {
		buslanes[id] = canvas.path("M"+(p1.x)+" "+p1.y+"L"+(p2.x)+" "+p2.y).attr({'stroke-width':1.5,'stroke':'#000000'});
		buslanes[id].hide();
	}
	line_spacing += line_spacing;
	if(poi_type in DontDraw)
		return
	this.poi = new POI(canvas, poi_x, poi_y, POI_SIZE, POI_SIZE, poi_type, id.split("-")[0], walkways[id]);
	this.seg.push(this.poi);
};

Segment.prototype.rotateBackground= function(theta) {
	var y = this.seg.getBBox().y;
	var y2 = this.seg.getBBox().y2;
	var piv_x = this.from_x
	var piv_y = y+((y2-y)/2)
	GraphicUtils.rotate(this.seg, theta, this.from_x, y+((y2-y)/2));
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			CROSSING DEFINITION					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Crossing(canvas, from_x, from_y, to_x, to_y, s_theta, d_theta, mid1, mid2, mid3, mid4, mid5, poi1, mcarlane, mbuslane, 
		sid1, sid2, sid3, sid4, sid5, poi2, scarlane, sbuslane) {
	from_x = from_x*SCALE
	from_y = from_y*SCALE
	to_x = to_x*SCALE
	to_y = to_y*SCALE
	line_spacing = SUPERSEG_HEIGHT/6
	this.length = GraphicUtils.getDistance(from_x, from_y, to_x, to_y);
	var from_rot = GraphicUtils.padLine(s_theta, this.size/2);
	var to_rot = GraphicUtils.padLine(d_theta, this.size/2);
	this.from_x1 = from_x - from_rot;
	this.from_x2 = from_x + from_rot;
	this.from_y = from_y;
	this.seg = canvas.set();
	var theta = Math.atan2(to_y-from_y, to_x-from_x);
	this.to_x1 = from_x+ this.length + to_rot;
	this.to_x2 = from_x+ this.length - to_rot;
	var segs_len = this.length*CROSSING_SEG_WIDTH
	var cross_len = this.length*CROSSING_WIDTH
	this.seg1_end = GraphicUtils.pointAtDistance(from_x, from_y, theta, segs_len)
	this.seg2_begin = GraphicUtils.pointAtDistance(from_x, from_y, theta, segs_len+cross_len)
	this.seg1 = new Segment(canvas, from_x, from_y, this.seg1_end.x, this.seg1_end.y, s_theta, 0.0, mid1+"-"+mid2, true, true, true, poi1, sid4+"-"+sid5, true, true, true, PoiType.none, true);
	this.crossing = new Segment(canvas, this.seg1_end.x, this.seg1_end.y, this.seg2_begin.x, this.seg2_begin.y, 0.0, 0.0, mid2+"-"+mid3, false, true, true, PoiType.none, sid2+"-"+sid3, false, true, true, PoiType.none, true);
	this.seg2 = new Segment(canvas, this.seg2_begin.x, this.seg2_begin.y, to_x, to_y, 0.0, d_theta, mid4+"-"+mid5, true, true, true, PoiType.none, sid1+"-"+sid2, true, true, true, poi2, true);
	lanes[sid2+"-"+sid4] = this.crossing.secLane
	lanes[mid2+"-"+mid4] = this.crossing.mainLane
	var walk1 = walkways[mid1+"-"+mid2]
	var walk2 = walkways[mid4+"-"+mid5]
	var walk3 = walkways[sid1+"-"+sid2]
	var walk4 = walkways[sid4+"-"+sid5]
	walkways[mid1+"-"+mid4] = canvas.path(["M", (walk1.getPointAtLength(0).x), (walk1.getPointAtLength(0).y), "L", (walk2.getPointAtLength(0).x), (walk2.getPointAtLength(0).y)])
	lanes[mid1+"-"+mid4] = this.seg1.mainLane
	walkways[mid1+"-"+mid4].hide()
	walkways[sid1+"-"+sid4] = canvas.path(["M", (walk3.getPointAtLength(0).x), (walk3.getPointAtLength(0).y), "L", (walk4.getPointAtLength(0).x), (walk4.getPointAtLength(0).y)])
	lanes[sid1+"-"+sid4] = this.seg1.secLane
	walkways[sid1+"-"+sid4].hide()
	this.addCrossing(canvas, walk1, walk4, theta, mid2+"-"+mid3, sid2+"-"+sid4)
	this.addCrossing(canvas, walk3, walk2, theta-Math.PI, sid2+"-"+sid3, mid2+"-"+mid4)
}

Crossing.prototype.addCrossing = function(canvas, walk1, walk2, theta, id1, id2) {
	var p1 = walk1.getPointAtLength(walk1.getTotalLength())
	var p2 = walk2.getPointAtLength(0)
	var cross_begin = GraphicUtils.pointAtDistance(walk1.getPointAtLength(walk1.getTotalLength()),theta, this.length*CROSSING_WIDTH*0.3)
	var cross_half = GraphicUtils.pointAtDistance(cross_begin, theta-Math.PI/2, SUPERSEG_HEIGHT/2)
	var cross_end = GraphicUtils.pointAtDistance(walk2.getPointAtLength(0), theta, this.length*CROSSING_WIDTH*0.3)
	walkways[id1] = canvas.path("M"+p1.x+" "+p1.y+"L"+cross_begin.x+" "+cross_begin.y+"L"+cross_half.x+" "+cross_half.y).attr({'stroke-width':2,'stroke':'#FFFFFF'});
	walkways[id2] = canvas.path("M"+cross_half.x+" "+cross_half.y+"L"+cross_end.x+" "+cross_end.y+"L"+p2.x+" "+p2.y).attr({'stroke-width':2,'stroke':'#FFFFFF'});
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			ROUNDABOUT DEFINITION				////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var step = SUPERSEG_HEIGHT/6
Roundabout.OFFSET = { "PEDESTRIAN": 4*SCALE, "PRIVATE_VEHICLE": 4*SCALE+SUPERSEG_HEIGHT/6, "PUBLIC_VEHICLE": 4*SCALE+2*SUPERSEG_HEIGHT/6}
Roundabout.DELTA = { "PEDESTRIAN": SUPERSEG_HEIGHT/2-2*SCALE, "PRIVATE_VEHICLE": SUPERSEG_HEIGHT/2-2*SCALE-step, "PUBLIC_VEHICLE": SUPERSEG_HEIGHT/2-2*SCALE-2*step}
Roundabout.RAD_EXTEND = 4*SCALE

function Roundabout(canvas, x, y, radius, args) {
	x=x*SCALE
	y=y*SCALE
	this.x = x;
	this.y = y;
	radius=radius*SCALE
	this.radius = radius+Roundabout.RAD_EXTEND
	this.obj = canvas.set();
//	this.section = Array(num);
	this.fill = canvas.circle(x, y, this.radius).attr({'stroke-width':'0', 'fill':'#EBC706'});
	this.outer = canvas.circle(x,y, this.radius).attr({'stroke':'#876E33', 'stroke-width':'2'});
	this.inner = canvas.circle(x, y, 20*SCALE).attr({'stroke':'#876E33', 'stroke-width':'1.5', 'fill':'#FFFFFF'});
	this.outer.toBack()
//	this.obj.push(this.section[0]);
//	this.obj.push(this.section[1]);
//	this.obj.push(this.section[2]);
//	this.obj.push(this.section[3]);
	this.obj.push(this.fill);
	this.obj.push(this.inner);
	for(key in Roundabout.DELTA)
		for(var i=0; i<args.length; i+=3)
			for(var j=0; j<args.length; j+=3)
				this.getPath(canvas, args[i], args[i+1], args[i+2], args[j], args[j+1], args[j+2], key);
//	GraphicUtils.rotate(this.obj, theta, x, y);
}

Roundabout.prototype.getPath = function(canvas, id1, x1, y1, id2, x2, y2, type) {
	x1=x1*SCALE
	y1=y1*SCALE
	x2=x2*SCALE
	y2=y2*SCALE
	var rad = this.radius-Roundabout.RAD_EXTEND-Roundabout.OFFSET[type]
	var angle_beg = GraphicUtils.angleBetween(x1, y1, this.x, this.y)
	var p0 = { x: GraphicUtils.pointAtDistance(x1, y1, angle_beg+Math.PI/2, Roundabout.DELTA[type]).x , y: GraphicUtils.pointAtDistance(x1, y1, angle_beg+Math.PI/2, Roundabout.DELTA[type]).y }
	var dist_begin = GraphicUtils.getDistance(p0.x, p0.y, this.x, this.y)
	var angle_end = GraphicUtils.angleBetween(x2, y2, this.x, this.y)
	var p3 = { x: GraphicUtils.pointAtDistance(x2, y2, angle_end-Math.PI/2, Roundabout.DELTA[type]).x , y: GraphicUtils.pointAtDistance(x2, y2, angle_end-Math.PI/2, Roundabout.DELTA[type]).y }
	var dist_end = GraphicUtils.getDistance(p3.x, p3.y, this.x, this.y)
	var p1 = GraphicUtils.pointAtDistance(p0.x, p0.y, angle_beg, dist_begin-rad)
	var p2 = GraphicUtils.pointAtDistance(p3.x, p3.y, angle_end, dist_end-rad)
	var path_angle = GraphicUtils.angleBetween(x1, y1, x2, y2)
	var path = canvas.path("M "+p0.x+","+p0.y+"L"+p1.x+","+p1.y+"A,"+rad+","+rad+","+0+","+(((this.x-p0.x)*(p3.y-p0.y)-(this.y-p0.y)*(p3.x-p0.x))>=0? 0 : 1)+","+0+","+p2.x+","+p2.y+"L"+p3.x+","+p3.y)//.attr({'stroke':'black', 'stroke-width':'2'})
	path.hide()
	var id_str = id1+".1-"+id2+".2";
	lanes[id_str] = this.fill
	switch(type) {
	case "PEDESTRIAN":
		walkways[id_str] = path
	case "PRIVATE_VEHICLE":
		carlanes[id_str] = path
	case "PUBLIC_VEHICLE":
		buslanes[id_str] = path
	}
}

Roundabout.prototype.getPathAround = function(canvas, x1, y1, x2, y2, type) {
	var rad = this.radius-Roundabout.RAD_EXTEND-Roundabout.OFFSET[type]
	var angle_beg = GraphicUtils.angleBetween(x1, y1, this.x, this.y)
	var dist_begin = GraphicUtils.getDistance(x1, y1, this.x, this.y)
	var angle_end = GraphicUtils.angleBetween(x2, y2, this.x, this.y)
	var dist_end = GraphicUtils.getDistance(x2, y2, this.x, this.y)
	var p1 = GraphicUtils.pointAtDistance(x1, y1, angle_beg, dist_begin-rad)
	var p2 = GraphicUtils.pointAtDistance(x2, y2, angle_end, dist_end-rad)
	var path_angle = GraphicUtils.angleBetween(x1, y1, x2, y2)
	return "M "+x1+","+y1+"L"+p1.x+","+p1.y+"A,"+rad+","+rad+","+0+","+(((this.x-x1)*(y2-y1)-(this.y-y1)*(x2-x1))>=0? 0 : 1)+","+0+","+p2.x+","+p2.y+"L"+x2+","+y2
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////				POI DEFINITION					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var BUS_STOP_DISTANCE = POI_PADDING_VER*2

function POI(canvas, x, y, length, height, type, id, parent) {
	length = length > 50 ? 50 : length;
	this.parent_line = parent
	this.id = id;
	this.img = canvas.image(PoiRes[type], x, y, length, height);
	this.border = canvas.rect(x, y, length, height);
	this.set = canvas.set();
	this.set.push(this.img);
	this.set.push(this.border);
	this.x = x;
	this.y = y;
	pois[this.id] = this
	return this.set;
}

POI.prototype.getBusStop = function() {
	var p1 = this.parent_line.getPointAtLength(0)
	var p2 = this.parent_line.getPointAtLength(this.parent_line.getTotalLength())
	var theta = GraphicUtils.angleBetween(p1.x, p1.y, p2.x, p2.y)+Math.PI/2
	return GraphicUtils.pointAtDistance(p1.x, p1.y, theta, BUS_STOP_DISTANCE)
}

POI.prototype.getPos = function() {
	return {"x" : this.border.getBBox().x, "y" : this.border.getBBox().y};
}
POI.prototype.getCenter = function() {
	var bbox = this.img.getBBox()
	return {x: bbox.x+(bbox.width/2), y: bbox.y+(bbox.height/2)}
}