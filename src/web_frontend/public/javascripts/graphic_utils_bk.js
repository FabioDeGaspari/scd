
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			GRAPHIC UTILITIES					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

;(function() {
	  Raphael.fn.addGuides = function() {
	    this.ca.guide = function(g) {
	      return {
	        guide: g
	      };
	    };
	    this.ca.along = function(percent) {
	      var g = this.attr("guide");
	      var len = g.getTotalLength();
	      var point = g.getPointAtLength(percent * len);
	      var t = {
	        transform: "t" + point.x + " " + point.y
	      };
	      return t;
	    };
	  };
	})();

//			RAPHAEL SETUP		//
var container = document.getElementById("map");
var raphael_canvas = new Raphael('map', container.clientHight, container.clientWidth);
raphael_canvas.addGuides()
var panZoom = raphael_canvas.panzoom({ initialZoom: 0, initialPosition: { x: 120, y: 70} });
panZoom.enable();

var GraphicUtils = {
		toDegrees : function(theta) {
			return theta * (180/Math.PI);
		},
		
		getDistance : function(x, y, x1, y1) {
			return Math.sqrt(Math.pow(x-x1, 2) + Math.pow(y-y1, 2));
		},
		
		rotate : function(elem, theta, piv_x, piv_y) {
			elem.attr({
				transform : ["r",(theta * (180/Math.PI)), piv_x, piv_y]
			});
		},
		
		getRotatedPoint : function(x, y, theta, piv_x, piv_y) {
			return {"x" : Math.cos(theta)*(x-piv_x) - Math.sin(theta)*(y-piv_y) + piv_x,
				"y": Math.sin(theta)*(x-piv_x) + Math.cos(theta)*(y-piv_y) + piv_y}
		},
		
		animateAlong : function(elem, path, duration) {
			elem.attr({cx : 0, cy : 0})
			elem.attr({guide : path, along : 0}).animate({along : 1}, duration)
		}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			GRAPHIC CONSTANTS					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var PoiType = Object.freeze({"market":0, "university":1, "parking":2, "bus_stop":3, "none":99});
var PoiRes = ["./res/market.svg", "res/university.svg", "res/parking.svg", "res/bus_stop.svg"];
var SUPERSEG_HEIGHT = 40;
var POI_SIZE = SUPERSEG_HEIGHT/2;
var POI_PADDING = POI_SIZE/10;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			OBJECTS DEFINITION					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function GraphicObject(canvas, json_obj, x, y) {
	switch(json_obj.type) {
	default:
		this.img = canvas.circle(0, 0, 3).attr({'stroke-width':'0', 'fill':'#EB0006'})
//		this.img.hide()
	}
	this.id = json_obj.id
	this.name = json_obj.name
	this.type = json_obj.type
	console.log("Object "+this.id+", "+this.name+" of type "+this.type+" created at position ("+x+","+y+")")
}

GraphicObject.prototype.moveAlong = function(path, duration) {
	GraphicUtils.animateAlong(this.img, path, duration)
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			SEGMENT DEFINITION					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Segment(canvas, from_x, from_y, to_x, to_y, s_theta, d_theta, mid, mwalkway, mcarlane, mbuslane, mpoi_type,
		sid, swalkway, scarlane, sbuslane, spoi_type) {
	this.paths = [];
	this.walkways = [];
	this.carlanes = [];
	this.buslanes = [];
	this.anim_paths = [];
	this.from_x = from_x;
	this.from_y = from_y;
	this.to_x = to_x;
	this.to_y = to_y;
	this.length = GraphicUtils.getDistance(from_x, from_y, to_x, to_y);
	this.size = sid == undefined ? SUPERSEG_HEIGHT/2 : SUPERSEG_HEIGHT;
	var line_spacing = sid == undefined ? SUPERSEG_HEIGHT/3 : SUPERSEG_HEIGHT/6;
	var from_rot = this.padLine(s_theta, this.size/2);
	var to_rot = this.padLine(d_theta, this.size/2);
	this.from_x1 = from_x - from_rot;
	this.from_x2 = from_x + from_rot;
	this.from_y = from_y;
	this.seg = canvas.set();
	var theta = Math.atan2(to_y-from_y, to_x-from_x);
	this.to_x1 = from_x+ this.length + to_rot;
	this.to_x2 = from_x+ this.length - to_rot;
	this.paths[0] = canvas.path("M"+this.from_x1+" "+(from_y-this.size/2)+"L"+this.to_x1+" "+(this.from_y-this.size/2)).attr({'stroke-width':1.5,'stroke':'#876E33'});
	this.paths[1] = canvas.path("M"+this.from_x2+" "+(from_y+this.size/2)+"L"+this.to_x2+" "+(this.from_y+this.size/2)).attr({'stroke-width':1.5,'stroke':'#876E33'});
	this.paths[2] = canvas.path("M"+this.from_x+" "+this.from_y+"L"+ (this.from_x + this.length)+" "+this.from_y).attr({'stroke-width':3,'stroke':'#878787'});
	this.seg.push(this.paths[0]);
	this.seg.push(this.paths[1]);
	this.seg.push(this.paths[2]);
	var rect_x = this.seg.getBBox().x;
	var rect_y2 = this.seg.getBBox().y2;
	this.background = canvas.rect(rect_x, this.seg.getBBox().y, this.seg.getBBox().x2-rect_x, rect_y2 - this.seg.getBBox().y).attr({'stroke-width':0, 'fill':'#EBC706'}).toBack();
	this.seg.push(this.background);
	this.addLine(canvas, true, mid, mwalkway, mcarlane, mbuslane, mpoi_type, line_spacing, s_theta, d_theta);
	if(sid != undefined)
		this.addLine(canvas, false, sid, swalkway, scarlane, sbuslane, spoi_type, line_spacing, s_theta, d_theta);
	this.rotate(theta);
	for (key in this.walkways)
		walkways[key] = this.walkways[key]
	for (key in this.carlanes)
		carlanes[key] = this.carlanes[key]
	for (key in this.buslanes)
		buslanes[key] = this.buslanes[key]
}

Segment.prototype.padLine = function(theta, height) {
	if(theta != 0)
		return (height) * Math.tan(theta/2);
	return 0;
};

Segment.prototype.addLine = function(canvas, is_main, id, walkway, carlane, buslane, poi_type, line_spacing, s_theta, d_theta) {
	var line_offset = this.seg.getBBox().y+2;
	var from_x = this.from_x+ this.length, to_x = this.from_x;
	var walk_s_pad = this.padLine(s_theta, this.size/2-2);
	var walk_d_pad = this.padLine(d_theta, this.size/2-2);
	var car_s_pad = this.padLine(s_theta, this.size/2-2-line_spacing);
	var car_d_pad = this.padLine(d_theta, this.size/2-2-line_spacing);
	var bus_s_pad = this.padLine(s_theta, this.size/2-2-line_spacing*2);
	var bus_d_pad = this.padLine(d_theta, this.size/2-2-line_spacing*2);
	var poi_y = this.seg.getBBox().y - POI_SIZE - POI_PADDING, poi_x = from_x+walk_d_pad-POI_SIZE-POI_PADDING;
//	console.log("fromx: "+this.from_x);
//	console.log("fromx2: "+this.from_x2);
	if(is_main) {
		walk_d_pad = [walk_s_pad, walk_s_pad = walk_d_pad][0]; //swap
		car_d_pad = [car_s_pad, car_s_pad = car_d_pad][0];
		bus_d_pad = [bus_s_pad, bus_s_pad = bus_d_pad][0];
		line_spacing = -line_spacing;
		line_offset = this.seg.getBBox().y2-2;
		from_x = this.from_x;
		to_x = this.from_x+this.length;
		poi_y = this.seg.getBBox().y2 + POI_PADDING;
		poi_x = from_x+walk_d_pad+POI_PADDING;
	}
	if(walkway) {
		this.walkways[id] = canvas.path("M"+(from_x+walk_d_pad)+" "+line_offset+"L"+(to_x-walk_s_pad)+" "+line_offset).attr({'stroke-width':1.5,'stroke':'#000000'});
		if(id == "n32.2-n31.2")
			this.walkways[id].hide();
		this.seg.push(this.walkways[id]);
		line_offset += line_spacing;
	}
	if(carlane) {
		this.carlanes[id] = canvas.path("M"+(from_x+car_d_pad)+" "+line_offset+"L"+(to_x-car_s_pad)+" "+line_offset).attr({'stroke-width':1.5,'stroke':'#000000'});
//		carlanes[id].hide();
		this.seg.push(this.carlanes[id]);
		line_offset += line_spacing;
	}
	if(buslane) {
		this.buslanes[id] = canvas.path("M"+(from_x+bus_d_pad)+" "+line_offset+"L"+(to_x-bus_s_pad)+" "+line_offset).attr({'stroke-width':1.5,'stroke':'#000000'});
//		buslanes[id].hide();
		this.seg.push(this.buslanes[id]);
		line_spacing += line_spacing;
	}
	this.poi = new POI(canvas, poi_x, poi_y, POI_SIZE, POI_SIZE, poi_type, id.split("-")[0]);
	this.seg.push(this.poi);
};

Segment.prototype.rotate = function(theta) {
	var y = this.seg.getBBox().y;
	var y2 = this.seg.getBBox().y2;
	var piv_x = this.from_x
	var piv_y = y+((y2-y)/2)
	for (key in this.walkways)
		this.walkways[key] = this.walkways[key].attr({transform : ["r",(theta * (180/Math.PI)), piv_x, piv_y]});
	for (key in this.carlanes)
		this.carlanes[key] = this.carlanes[key].attr({transform : ["r",(theta * (180/Math.PI)), piv_x, piv_y]});
	for (key in this.buslanes)
		this.buslanes[key] = this.buslanes[key].attr({transform : ["r",(theta * (180/Math.PI)), piv_x, piv_y]});
//	GraphicUtils.rotate(this.seg, theta, this.from_x, y+((y2-y)/2));
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////			ROUNDABOUT DEFINITION				////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function Roundabout(canvas, x, y, radius, num, theta) {//x1, y1, x2, y2, x3, y3, x4, y4) {
	this.x = x;
	this.y = y;
	this.radius = radius+4;
	this.obj = canvas.set();
	this.section = Array(num);
	this.fill = canvas.circle(x, y, this.radius).attr({'stroke-width':'0', 'fill':'#EBC706'});
	this.inner = canvas.circle(x, y, 20).attr({'stroke':'#876E33', 'stroke-width':'1.5', 'fill':'#FFFFFF'});
	this.section[0] = this.arc(canvas, x-radius, y+SUPERSEG_HEIGHT/2, x-SUPERSEG_HEIGHT/2, y+radius);
	this.section[1] = this.arc(canvas, x+SUPERSEG_HEIGHT/2, y+radius, x+radius, y+SUPERSEG_HEIGHT/2);
	this.section[2] = this.arc(canvas, x+radius, y-SUPERSEG_HEIGHT/2, x+SUPERSEG_HEIGHT/2, y-radius);
	this.section[3] = this.arc(canvas, x-SUPERSEG_HEIGHT/2, y-radius, x-radius, y-SUPERSEG_HEIGHT/2);
	if(num <= 3) {
		this.section[4] = this.arc(canvas, x+SUPERSEG_HEIGHT/2, y-radius, x-SUPERSEG_HEIGHT/2, y-radius, x-radius, y-SUPERSEG_HEIGHT/2);
		this.obj.push(this.section[4]);
	}
	this.obj.push(this.section[0]);
	this.obj.push(this.section[1]);
	this.obj.push(this.section[2]);
	this.obj.push(this.section[3]);
	this.obj.push(this.fill);
	this.obj.push(this.inner);
	GraphicUtils.rotate(this.obj, theta, x, y);
}

Roundabout.prototype.arc = function(canvas, x1, y1, x2, y2) {
	return canvas.path(["M", x1, y1, "A", 50, 50, 0, 0, 0, x2, y2]).attr({'stroke-width':1.5, 'stroke':'#876E33'});
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////				POI DEFINITION					////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function POI(canvas, x, y, length, height, type, id) {
	length = length > 50 ? 50 : length;
	this.id = id;
	this.img = canvas.image(PoiRes[type], x, y, length, height);
	this.border = canvas.rect(x, y, length, height);
	this.set = canvas.set();
	this.set.push(this.img);
	this.set.push(this.border);
	this.x = x;
	this.y = y;
	this.getPos = function(){
		return {"x" : this.border.getBBox().x, "y" : this.border.getBBox().y};
	}
	pois[this.id] = this
	console.log("created POI with id "+this.id + " at position "+this.getPos().x+","+this.getPos().y)
	return this.set;
}