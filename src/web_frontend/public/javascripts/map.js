var segments = []
var lanes = []
var pois = []
var roundabouts = []
var buslanes = []
var carlanes = []
var walkways = []
var districts = []

segments["40-102"] = new Segment(raphael_canvas, 600, 900, 650, 850, -0.7853981633974483, 0.7853981633974483, "n40.1-n102.1", true, true, true, PoiType.BANK, "n102.2-n40.2", true, true, true, PoiType.HOME);
segments["59-60"] = new Segment(raphael_canvas, 750, 50, 600, 50, 5.497787143782138, 0.0, "n59.1-n60.1", true, true, true, PoiType.UNIVERSITY, "n60.2-n59.2", true, true, true, PoiType.MALL);
segments["214-215"] = new Segment(raphael_canvas, 2250, 50, 2150, 150, -0.7853981633974483, -0.7853981633974483, "n214.1-n215.1", true, true, true, PoiType.UNIVERSITY, "n215.2-n214.2", true, true, true, PoiType.BANK);
segments["222-228"] = new Segment(raphael_canvas, 1900, 750, 1900, 850, 0.0, 0.0, "n222.1-n228.1", true, true, true, PoiType.BANK, "n228.2-n222.2", true, true, true, PoiType.SHOP);
segments["134-135"] = new Segment(raphael_canvas, 1350, 100, 1450, 100, 0.7853981633974483, 0.0, "n134.1-n135.1", true, true, true, PoiType.MALL, "n135.2-n134.2", true, true, true, PoiType.HOME);
segments["237-251"] = new Segment(raphael_canvas, 1900, 1100, 1900, 1200, 0.0, 0.0, "n237.2-n251.1", true, true, true, PoiType.CROSSROAD, "n251.2-n237.1", true, true, true, PoiType.SHOP);
segments["117-118"] = new Segment(raphael_canvas, 1000, 1150, 1100, 1250, -0.7853981633974483, -0.7853981633974483, "n117.1-n118.1", true, true, true, PoiType.SPORT_CENTER, "n118.2-n117.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["261-267"] = new Segment(raphael_canvas, 2400, 850, 2400, 800, -0.7853981633974483, 0.0, "n261.1-n267.1", true, true, true, PoiType.UNIVERSITY, "n267.2-n261.2", true, true, true, PoiType.SPORT_CENTER);
segments["209-210"] = new Segment(raphael_canvas, 2800, 50, 2650, 50, 5.497787143782138, 0.0, "n209.1-n210.1", true, true, true, PoiType.HOME, "n210.2-n209.2", true, true, true, PoiType.POST);
segments["57-62"] = new Segment(raphael_canvas, 800, 250, 800, 300, 0.0, 0.0, "n57.2-n62.1", true, true, true, PoiType.CROSSROAD, "n62.2-n57.1", true, true, true, PoiType.SHOP);
segments["80-83"] = new Segment(raphael_canvas, 1250, 550, 1250, 600, 0.0, 0.0, "n80.2-n83.1", true, true, true, PoiType.CROSSROAD, "n83.2-n80.1", true, true, true, PoiType.PUB);
segments["70-71"] = new Segment(raphael_canvas, 1000, 500, 1050, 500, 0.0, 0.0, "n70.1-n71.1", true, true, true, PoiType.HOME, "n71.2-n70.2", true, true, true, PoiType.PUB);
segments["288-289"] = new Segment(raphael_canvas, 2800, 800, 2800, 900, 0.0, 0.7853981633974483, "n288.1-n289.1", true, true, true, PoiType.AMUSEMENT_PARK, "n289.2-n288.2", true, true, true, PoiType.PUB);
segments["218-219"] = new Segment(raphael_canvas, 2250, 300, 2300, 250, -0.7853981633974483, -0.7853981633974483, "n218.1-n219.1", true, true, true, PoiType.UNIVERSITY, "n219.2-n218.2", true, true, true, PoiType.MALL);
segments["275-280"] = new Segment(raphael_canvas, 2450, 600, 2500, 600, 0.0, 0.0, "n275.2-n280.1", true, true, true, PoiType.CROSSROAD, "n280.2-n275.1", true, true, true, PoiType.AMUSEMENT_PARK);
segments["216-217"] = new Segment(raphael_canvas, 2150, 250, 2200, 300, -0.7853981633974483, -0.7853981633974483, "n216.1-n217.1", true, true, true, PoiType.AMUSEMENT_PARK, "n217.2-n216.2", true, true, true, PoiType.SHOP);
segments["12-13"] = new Segment(raphael_canvas, 200, 500, 300, 600, -0.7853981633974483, 0.7853981633974483, "n12.1-n13.1", true, true, true, PoiType.SHOP, "n13.2-n12.2", true, true, true, PoiType.POST);
segments["260-261"] = new Segment(raphael_canvas, 2300, 950, 2400, 850, 0.0, -0.7853981633974483, "n260.1-n261.1", true, true, true, PoiType.MALL, "n261.2-n260.2", true, true, true, PoiType.HOME);
segments["170-296"] = new Segment(raphael_canvas, 2050, 550, 2050, 650, 0.0, 0.0, "n170.2-n296.1", true, true, true, PoiType.CROSSROAD, "n296.2-n170.1", true, true, true, PoiType.POST);
segments["58-59"] = new Segment(raphael_canvas, 800, 100, 750, 50, -0.7853981633974483, 5.497787143782138, "n58.1-n59.1", true, true, true, PoiType.POST, "n59.2-n58.2", true, true, true, PoiType.BANK);
segments["22-34"] = new Segment(raphael_canvas, 350, 900, 400, 900, 0.0, 0.0, "n22.2-n34.1", true, true, true, PoiType.CROSSROAD, "n34.2-n22.1", true, true, true, PoiType.UNIVERSITY);
segments["291-295"] = new Segment(raphael_canvas, 2550, 1050, 2550, 1150, 0.0, 0.0, "n291.2-n295.1", true, true, true, PoiType.CROSSROAD, "n295.2-n291.1", true, true, true, PoiType.POST);
segments["160-162"] = new Segment(raphael_canvas, 1950, 100, 2050, 200, 0.7853981633974483, 0.7853981633974483, "n160.1-n162.1", true, true, true, PoiType.PUB, "n162.2-n160.2", true, true, true, PoiType.SPORT_CENTER);
segments["0-1"] = new Segment(raphael_canvas, 50, 200, 150, 200, 0.0, 0.0, "n0.1-n1.1", true, true, true, PoiType.BANK, "n1.2-n0.2", true, true, true, PoiType.CROSSROAD);
segments["102-109"] = new Segment(raphael_canvas, 650, 850, 700, 850, 0.7853981633974483, 0.7853981633974483, "n102.1-n109.1", true, true, true, PoiType.AMUSEMENT_PARK, "n109.2-n102.2", true, true, true, PoiType.PUB);
segments["67-68"] = new Segment(raphael_canvas, 800, 400, 800, 450, 0.0, -0.7853981633974483, "n67.1-n68.1", true, true, true, PoiType.HOME, "n68.2-n67.2", true, true, true, PoiType.BANK);
segments["81-127"] = new Segment(raphael_canvas, 1300, 500, 1350, 500, 0.0, 0.0, "n81.2-n127.1", true, true, true, PoiType.CROSSROAD, "n127.2-n81.1", true, true, true, PoiType.SHOP);
segments["215-216"] = new Segment(raphael_canvas, 2150, 150, 2150, 250, -0.7853981633974483, -0.7853981633974483, "n215.1-n216.1", true, true, true, PoiType.MALL, "n216.2-n215.2", true, true, true, PoiType.BANK);
segments["204-205"] = new Segment(raphael_canvas, 2600, 150, 2700, 250, 0.7853981633974483, -0.7853981633974483, "n204.1-n205.1", true, true, true, PoiType.AMUSEMENT_PARK, "n205.2-n204.2", true, true, true, PoiType.SPORT_CENTER);
segments["132-152"] = new Segment(raphael_canvas, 1450, 500, 1550, 500, 0.0, -0.7853981633974483, "n132.1-n152.1", true, true, true, PoiType.BANK, "n152.2-n132.2", true, true, true, PoiType.PUB);
segments["145-154"] = new Segment(raphael_canvas, 1700, 100, 1750, 100, 0.0, 0.0, "n145.2-n154.1", true, true, true, PoiType.CROSSROAD, "n154.2-n145.1", true, true, true, PoiType.PUB);
segments["206-207"] = new Segment(raphael_canvas, 2800, 250, 2850, 200, -0.7853981633974483, -0.7853981633974483, "n206.1-n207.1", true, true, true, PoiType.PUB, "n207.2-n206.2", true, true, true, PoiType.PUB);
segments["89-90"] = new Segment(raphael_canvas, 1250, 800, 1150, 900, 0.7853981633974483, 0.7853981633974483, "n89.1-n90.1", true, true, true, PoiType.POST, "n90.2-n89.2", true, true, true, PoiType.PUB);
segments["199-221"] = new Segment(raphael_canvas, 2350, 150, 2400, 150, 0.7853981633974483, 0.0, "n221.1-n199.1", true, true, true, PoiType.AMUSEMENT_PARK, "n199.2-n221.2", true, true, true, PoiType.CROSSROAD);
segments["20-24"] = new Segment(raphael_canvas, 250, 900, 150, 900, 0.0, 0.0, "n20.2-n24.1", true, true, true, PoiType.CROSSROAD, "n24.2-n20.1", true, true, true, PoiType.SPORT_CENTER);
segments["184-193"] = new Segment(raphael_canvas, 2450, 400, 2450, 350, -0.7853981633974483, 0.0, "n184.1-n193.1", true, true, true, PoiType.UNIVERSITY, "n193.2-n184.2", true, true, true, PoiType.BANK);
segments["119-248"] = new Segment(raphael_canvas, 1250, 1250, 1450, 1250, 0.0, -0.7853981633974483, "n119.1-n248.1", true, true, true, PoiType.SHOP, "n248.2-n119.2", true, true, true, PoiType.MALL);
segments["213-307"] = new Segment(raphael_canvas, 2450, 50, 2350, 50, 0.0, 0.0, "n307.1-n213.1", true, true, true, PoiType.MALL, "n213.2-n307.2", true, true, true, PoiType.HOME);
segments["144-146"] = new Segment(raphael_canvas, 1650, 200, 1650, 150, 0.0, 0.0, "n146.2-n144.1", true, true, true, PoiType.HOME, "n144.2-n146.1", true, true, true, PoiType.CROSSROAD);
segments["278-279"] = new Segment(raphael_canvas, 2150, 750, 2150, 850, -0.7853981633974483, 0.0, "n278.1-n279.1", true, true, true, PoiType.AMUSEMENT_PARK, "n279.2-n278.2", true, true, true, PoiType.POST);
segments["259-260"] = new Segment(raphael_canvas, 2200, 1050, 2300, 950, -0.7853981633974483, 0.0, "n259.1-n260.1", true, true, true, PoiType.SPORT_CENTER, "n260.2-n259.2", true, true, true, PoiType.BANK);
segments["183-184"] = new Segment(raphael_canvas, 2350, 500, 2450, 400, -0.7853981633974483, -0.7853981633974483, "n183.1-n184.1", true, true, true, PoiType.UNIVERSITY, "n184.2-n183.2", true, true, true, PoiType.SHOP);
segments["277-278"] = new Segment(raphael_canvas, 2200, 700, 2150, 750, 0.0, -0.7853981633974483, "n277.1-n278.1", true, true, true, PoiType.POST, "n278.2-n277.2", true, true, true, PoiType.UNIVERSITY);
segments["207-208"] = new Segment(raphael_canvas, 2850, 200, 2850, 100, -0.7853981633974483, -0.7853981633974483, "n207.1-n208.1", true, true, true, PoiType.AMUSEMENT_PARK, "n208.2-n207.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["126-133"] = new Segment(raphael_canvas, 1250, 300, 1250, 200, 0.0, 0.7853981633974483, "n126.1-n133.1", true, true, true, PoiType.HOME, "n133.2-n126.2", true, true, true, PoiType.POST);
segments["290-292"] = new Segment(raphael_canvas, 2700, 1000, 2600, 1000, 0.7853981633974483, 0.0, "n290.1-n292.1", true, true, true, PoiType.AMUSEMENT_PARK, "n292.2-n290.2", true, true, true, PoiType.CROSSROAD);
segments["219-220"] = new Segment(raphael_canvas, 2300, 250, 2300, 200, -0.7853981633974483, 0.7853981633974483, "n219.1-n220.1", true, true, true, PoiType.AMUSEMENT_PARK, "n220.2-n219.2", true, true, true, PoiType.PUB);
segments["233-239"] = new Segment(raphael_canvas, 1900, 950, 1900, 1000, 0.0, 0.0, "n233.1-n239.1", true, true, true, PoiType.SPORT_CENTER, "n239.2-n233.2", true, true, true, PoiType.CROSSROAD);
segments["210-302"] = new Segment(raphael_canvas, 2650, 50, 2550, 50, 0.0, 0.0, "n210.1-n302.1", true, true, true, PoiType.SPORT_CENTER, "n302.2-n210.2", true, true, true, PoiType.UNIVERSITY);
segments["258-259"] = new Segment(raphael_canvas, 2100, 1050, 2200, 1050, 0.0, -0.7853981633974483, "n258.1-n259.1", true, true, true, PoiType.AMUSEMENT_PARK, "n259.2-n258.2", true, true, true, PoiType.MALL);
segments["11-12"] = new Segment(raphael_canvas, 200, 400, 200, 500, 0.0, -0.7853981633974483, "n11.1-n12.1", true, true, true, PoiType.SPORT_CENTER, "n12.2-n11.2", true, true, true, PoiType.SHOP);
segments["47-48"] = new Segment(raphael_canvas, 400, 200, 500, 200, 0.0, 0.0, "n47.1-n48.1", true, true, true, PoiType.PUB, "n48.2-n47.2", true, true, true, PoiType.PUB);
segments["176-222"] = new Segment(raphael_canvas, 1900, 650, 1900, 750, 0.0, 0.0, "n176.1-n222.1", true, true, true, PoiType.AMUSEMENT_PARK, "n222.2-n176.2", true, true, true, PoiType.BANK);
segments["273-276"] = new Segment(raphael_canvas, 2350, 600, 2300, 600, 0.0, -0.7853981633974483, "n273.2-n276.1", true, true, true, PoiType.CROSSROAD, "n276.2-n273.1", true, true, true, PoiType.PUB);
segments["168-172"] = new Segment(raphael_canvas, 2050, 400, 2050, 450, 0.0, 0.0, "n168.1-n172.1", true, true, true, PoiType.SHOP, "n172.2-n168.2", true, true, true, PoiType.CROSSROAD);
segments["151-153"] = new Segment(raphael_canvas, 1650, 400, 1650, 300, -0.7853981633974483, 0.0, "n153.1-n151.2", true, true, true, PoiType.BANK, "n151.1-n153.2", true, true, true, PoiType.PUB);
segments["118-119"] = new Segment(raphael_canvas, 1100, 1250, 1250, 1250, -0.7853981633974483, 0.0, "n118.1-n119.1", true, true, true, PoiType.HOME, "n119.2-n118.2", true, true, true, PoiType.HOME);
segments["3-42"] = new Segment(raphael_canvas, 250, 200, 300, 200, 0.0, 0.0, "n3.2-n42.1", true, true, true, PoiType.CROSSROAD, "n42.2-n3.1", true, true, true, PoiType.SPORT_CENTER);
segments["95-111"] = new Segment(raphael_canvas, 1000, 950, 1000, 1000, 0.0, 0.0, "n95.2-n111.1", true, true, true, PoiType.CROSSROAD, "n111.2-n95.1", true, true, true, PoiType.SPORT_CENTER);
segments["220-221"] = new Segment(raphael_canvas, 2300, 200, 2350, 150, 0.7853981633974483, 0.7853981633974483, "n220.1-n221.1", true, true, true, PoiType.MALL, "n221.2-n220.2", true, true, true, PoiType.BANK);
segments["208-209"] = new Segment(raphael_canvas, 2850, 100, 2800, 50, -0.7853981633974483, 5.497787143782138, "n208.1-n209.1", true, true, true, PoiType.POST, "n209.2-n208.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["68-69"] = new Segment(raphael_canvas, 800, 450, 850, 500, -0.7853981633974483, -0.7853981633974483, "n68.1-n69.1", true, true, true, PoiType.SHOP, "n69.2-n68.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["82-121"] = new Segment(raphael_canvas, 1250, 450, 1250, 400, 0.0, 0.0, "n82.2-n121.1", true, true, true, PoiType.CROSSROAD, "n121.2-n82.1", true, true, true, PoiType.BANK);
segments["240-250"] = new Segment(raphael_canvas, 1650, 1050, 1700, 1050, 0.7853981633974483, 0.0, "n250.1-n240.1", true, true, true, PoiType.POST, "n240.2-n250.2", true, true, true, PoiType.BANK);
segments["249-250"] = new Segment(raphael_canvas, 1550, 1150, 1650, 1050, 0.0, 0.7853981633974483, "n249.1-n250.1", true, true, true, PoiType.AMUSEMENT_PARK, "n250.2-n249.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["88-89"] = new Segment(raphael_canvas, 1250, 700, 1250, 800, 0.0, 0.7853981633974483, "n88.1-n89.1", true, true, true, PoiType.HOME, "n89.2-n88.2", true, true, true, PoiType.BANK);
segments["60-61"] = new Segment(raphael_canvas, 600, 50, 500, 50, 0.0, 0.0, "n60.1-n61.1", true, true, true, PoiType.HOME, "n61.2-n60.2", true, true, true, PoiType.HOME);
segments["54-56"] = new Segment(raphael_canvas, 700, 200, 750, 200, 0.0, 0.0, "n54.1-n56.1", true, true, true, PoiType.SPORT_CENTER, "n56.2-n54.2", true, true, true, PoiType.CROSSROAD);
segments["289-290"] = new Segment(raphael_canvas, 2800, 900, 2700, 1000, 0.7853981633974483, 0.7853981633974483, "n289.1-n290.1", true, true, true, PoiType.SHOP, "n290.2-n289.2", true, true, true, PoiType.SHOP);
segments["140-143"] = new Segment(raphael_canvas, 1550, 100, 1600, 100, 0.0, 0.0, "n140.1-n143.1", true, true, true, PoiType.PUB, "n143.2-n140.2", true, true, true, PoiType.CROSSROAD);
segments["94-108"] = new Segment(raphael_canvas, 900, 900, 950, 900, 0.0, 0.0, "n108.1-n94.1", true, true, true, PoiType.UNIVERSITY, "n94.2-n108.2", true, true, true, PoiType.CROSSROAD);
segments["293-294"] = new Segment(raphael_canvas, 2550, 950, 2550, 900, 0.0, 0.0, "n293.2-n294.1", true, true, true, PoiType.CROSSROAD, "n294.2-n293.1", true, true, true, PoiType.HOME);
segments["205-206"] = new Segment(raphael_canvas, 2700, 250, 2800, 250, -0.7853981633974483, -0.7853981633974483, "n205.1-n206.1", true, true, true, PoiType.AMUSEMENT_PARK, "n206.2-n205.2", true, true, true, PoiType.AMUSEMENT_PARK);
segments["21-25"] = new Segment(raphael_canvas, 300, 950, 300, 1000, 0.0, 0.0, "n21.2-n25.1", true, true, true, PoiType.CROSSROAD, "n25.2-n21.1", true, true, true, PoiType.BANK);
segments["55-58"] = new Segment(raphael_canvas, 800, 150, 800, 100, 0.0, -0.7853981633974483, "n55.2-n58.1", true, true, true, PoiType.CROSSROAD, "n58.2-n55.1", true, true, true, PoiType.UNIVERSITY);
segments["286-287"] = new Segment(raphael_canvas, 2700, 600, 2800, 700, 0.7853981633974483, 0.7853981633974483, "n286.1-n287.1", true, true, true, PoiType.MALL, "n287.2-n286.2", true, true, true, PoiType.PUB);
segments["198-200"] = new Segment(raphael_canvas, 2450, 250, 2450, 200, 0.0, 0.0, "n198.1-n200.1", true, true, true, PoiType.SHOP, "n200.2-n198.2", true, true, true, PoiType.CROSSROAD);
segments["69-70"] = new Segment(raphael_canvas, 850, 500, 1000, 500, -0.7853981633974483, 0.0, "n69.1-n70.1", true, true, true, PoiType.AMUSEMENT_PARK, "n70.2-n69.2", true, true, true, PoiType.UNIVERSITY);
segments["30-31"] = new Segment(raphael_canvas, 300, 1100, 300, 1200, 0.0, -0.7853981633974483, "n30.1-n31.1", true, true, true, PoiType.BANK, "n31.2-n30.2", true, true, true, PoiType.SHOP);
segments["152-153"] = new Segment(raphael_canvas, 1550, 500, 1650, 400, -0.7853981633974483, -0.7853981633974483, "n152.1-n153.1", true, true, true, PoiType.POST, "n153.2-n152.2", true, true, true, PoiType.BANK);
segments["248-249"] = new Segment(raphael_canvas, 1450, 1250, 1550, 1150, -0.7853981633974483, 0.0, "n248.1-n249.1", true, true, true, PoiType.BANK, "n249.2-n248.2", true, true, true, PoiType.PUB);
segments["171-177"] = new Segment(raphael_canvas, 2100, 500, 2150, 500, 0.0, 0.0, "n171.2-n177.1", true, true, true, PoiType.CROSSROAD, "n177.2-n171.1", true, true, true, PoiType.POST);
segments["162-163"] = new Segment(raphael_canvas, 2050, 200, 2050, 300, 0.7853981633974483, 0.0, "n162.1-n163.1", true, true, true, PoiType.HOME, "n163.2-n162.2", true, true, true, PoiType.PUB);
segments["169-174"] = new Segment(raphael_canvas, 2000, 500, 1950, 500, 0.0, -0.7853981633974483, "n169.2-n174.1", true, true, true, PoiType.CROSSROAD, "n174.2-n169.1", true, true, true, PoiType.SPORT_CENTER);
segments["182-183"] = new Segment(raphael_canvas, 2250, 500, 2350, 500, 0.0, -0.7853981633974483, "n182.1-n183.1", true, true, true, PoiType.BANK, "n183.2-n182.2", true, true, true, PoiType.HOME);
segments["39-40"] = new Segment(raphael_canvas, 500, 900, 600, 900, 0.0, -0.7853981633974483, "n39.1-n40.1", true, true, true, PoiType.HOME, "n40.2-n39.2", true, true, true, PoiType.SPORT_CENTER);
segments["159-160"] = new Segment(raphael_canvas, 1850, 100, 1950, 100, 0.0, 0.7853981633974483, "n159.1-n160.1", true, true, true, PoiType.POST, "n160.2-n159.2", true, true, true, PoiType.HOME);
segments["272-274"] = new Segment(raphael_canvas, 2400, 700, 2400, 650, 0.0, 0.0, "n272.1-n274.1", true, true, true, PoiType.SHOP, "n274.2-n272.2", true, true, true, PoiType.CROSSROAD);
segments["236-245"] = new Segment(raphael_canvas, 1800, 1050, 1850, 1050, 0.0, 0.0, "n245.1-n236.1", true, true, true, PoiType.BANK, "n236.2-n245.2", true, true, true, PoiType.CROSSROAD);
segments["2-6"] = new Segment(raphael_canvas, 200, 250, 200, 300, 0.0, 0.0, "n2.2-n6.1", true, true, true, PoiType.CROSSROAD, "n6.2-n2.1", true, true, true, PoiType.SPORT_CENTER);
segments["32-33"] = new Segment(raphael_canvas, 350, 1250, 500, 1250, -0.7853981633974483, 0.0, "n32.1-n33.1", true, true, true, PoiType.BANK, "n33.2-n32.2", true, true, true, PoiType.SPORT_CENTER);
segments["251-252"] = new Segment(raphael_canvas, 1900, 1200, 1900, 1300, 0.0, 0.0, "n251.1-n252.1", true, true, true, PoiType.MALL, "n252.2-n251.2", true, true, true, PoiType.BANK);
segments["287-288"] = new Segment(raphael_canvas, 2800, 700, 2800, 800, 0.7853981633974483, 0.0, "n287.1-n288.1", true, true, true, PoiType.BANK, "n288.2-n287.2", true, true, true, PoiType.SHOP);
segments["90-96"] = new Segment(raphael_canvas, 1150, 900, 1050, 900, 0.7853981633974483, 0.0, "n90.1-n96.1", true, true, true, PoiType.HOME, "n96.2-n90.2", true, true, true, PoiType.CROSSROAD);
segments["4-5"] = new Segment(raphael_canvas, 200, 150, 200, 50, 0.0, 0.0, "n4.2-n5.1", true, true, true, PoiType.CROSSROAD, "n5.2-n4.1", true, true, true, PoiType.SHOP);
segments["175-176"] = new Segment(raphael_canvas, 1900, 550, 1900, 650, -0.7853981633974483, 0.0, "n175.1-n176.1", true, true, true, PoiType.MALL, "n176.2-n175.2", true, true, true, PoiType.MALL);
segments["174-175"] = new Segment(raphael_canvas, 1950, 500, 1900, 550, -0.7853981633974483, -0.7853981633974483, "n174.1-n175.1", true, true, true, PoiType.UNIVERSITY, "n175.2-n174.2", true, true, true, PoiType.MALL);
segments["13-14"] = new Segment(raphael_canvas, 300, 600, 300, 700, 0.7853981633974483, 0.0, "n13.1-n14.1", true, true, true, PoiType.SHOP, "n14.2-n13.2", true, true, true, PoiType.UNIVERSITY);
segments["109-110"] = new Segment(raphael_canvas, 700, 850, 750, 900, 0.7853981633974483, -0.7853981633974483, "n109.1-n110.1", true, true, true, PoiType.BANK, "n110.2-n109.2", true, true, true, PoiType.PUB);
segments["103-110"] = new Segment(raphael_canvas, 750, 900, 800, 900, -0.7853981633974483, 0.0, "n110.1-n103.1", true, true, true, PoiType.HOME, "n103.2-n110.2", true, true, true, PoiType.UNIVERSITY);
segments["19-23"] = new Segment(raphael_canvas, 300, 800, 300, 850, 0.0, 0.0, "n19.1-n23.1", true, true, true, PoiType.UNIVERSITY, "n23.2-n19.2", true, true, true, PoiType.CROSSROAD);
segments["116-117"] = new Segment(raphael_canvas, 1000, 1100, 1000, 1150, 0.0, -0.7853981633974483, "n116.1-n117.1", true, true, true, PoiType.POST, "n117.2-n116.2", true, true, true, PoiType.POST);
segments["31-32"] = new Segment(raphael_canvas, 300, 1200, 350, 1250, -0.7853981633974483, -0.7853981633974483, "n31.1-n32.1", true, true, true, PoiType.BANK, "n32.2-n31.2", true, true, true, PoiType.BANK);
segments["217-218"] = new Segment(raphael_canvas, 2200, 300, 2250, 300, -0.7853981633974483, -0.7853981633974483, "n217.1-n218.1", true, true, true, PoiType.HOME, "n218.2-n217.2", true, true, true, PoiType.SPORT_CENTER);
segments["285-286"] = new Segment(raphael_canvas, 2600, 600, 2700, 600, 0.0, 0.7853981633974483, "n285.1-n286.1", true, true, true, PoiType.SHOP, "n286.2-n285.2", true, true, true, PoiType.SPORT_CENTER);
segments["201-204"] = new Segment(raphael_canvas, 2500, 150, 2600, 150, 0.0, 0.7853981633974483, "n201.2-n204.1", true, true, true, PoiType.CROSSROAD, "n204.2-n201.1", true, true, true, PoiType.POST);
segments["276-277"] = new Segment(raphael_canvas, 2300, 600, 2200, 700, -0.7853981633974483, 0.0, "n276.1-n277.1", true, true, true, PoiType.SHOP, "n277.2-n276.2", true, true, true, PoiType.UNIVERSITY);
segments["213-214"] = new Segment(raphael_canvas, 2350, 50, 2250, 50, 0.0, -0.7853981633974483, "n213.1-n214.1", true, true, true, PoiType.AMUSEMENT_PARK, "n214.2-n213.2", true, true, true, PoiType.BANK);
segments["133-134"] = new Segment(raphael_canvas, 1250, 200, 1350, 100, 0.7853981633974483, 0.7853981633974483, "n133.1-n134.1", true, true, true, PoiType.BANK, "n134.2-n133.2", true, true, true, PoiType.UNIVERSITY);
segments["48-49"] = new Segment(raphael_canvas, 500, 200, 600, 200, 0.0, 0.0, "n48.1-n49.1", true, true, true, PoiType.SHOP, "n49.2-n48.2", true, true, true, PoiType.BANK);
segments["76-79"] = new Segment(raphael_canvas, 1150, 500, 1200, 500, 0.0, 0.0, "n76.1-n79.1", true, true, true, PoiType.AMUSEMENT_PARK, "n79.2-n76.2", true, true, true, PoiType.CROSSROAD);
segments["238-253"] = new Segment(raphael_canvas, 1950, 1050, 2000, 1050, 0.0, 0.0, "n238.2-n253.1", true, true, true, PoiType.CROSSROAD, "n253.2-n238.1", true, true, true, PoiType.BANK);
new Crossing(raphael_canvas, 1250, 600, 1250, 700, 0.0, 0.0, "n83.1", "n85.1", "n86.1", "n87.1", "n88.1", PoiType.SPORT_CENTER, true, true, "n88.2", "n86.2", "n85.2", "n84.2", "n83.2", PoiType.SPORT_CENTER, true, true)
roundabouts["n199.1"] = new Roundabout(raphael_canvas, 2450, 150, 50, ["n199", 2400, 150, "n200", 2450, 200, "n201", 2500, 150]);
roundabouts["n273.1"] = new Roundabout(raphael_canvas, 2400, 600, 50, ["n273", 2350, 600, "n274", 2400, 650, "n275", 2450, 600]);
roundabouts["n55.1"] = new Roundabout(raphael_canvas, 800, 200, 50, ["n55", 800, 150, "n56", 750, 200, "n57", 800, 250]);
new Crossing(raphael_canvas, 2050, 300, 2050, 400, 0.0, 0.0, "n163.1", "n165.1", "n166.1", "n167.1", "n168.1", PoiType.AMUSEMENT_PARK, true, true, "n168.2", "n166.2", "n165.2", "n164.2", "n163.2", PoiType.AMUSEMENT_PARK, true, true)
new Crossing(raphael_canvas, 2550, 50, 2450, 50, 0.0, 0.0, "n302.1", "n304.1", "n305.1", "n306.1", "n307.1", PoiType.HOME, true, true, "n307.2", "n305.2", "n304.2", "n303.2", "n302.2", PoiType.HOME, true, true)
new Crossing(raphael_canvas, 1650, 200, 1650, 300, 0.0, 0.0, "n146.1", "n148.1", "n149.1", "n150.1", "n151.1", PoiType.PUB, true, true, "n151.2", "n149.2", "n148.2", "n147.2", "n146.2", PoiType.PUB, true, true)
new Crossing(raphael_canvas, 300, 700, 300, 800, 0.0, 0.0, "n14.1", "n16.1", "n17.1", "n18.1", "n19.1", PoiType.MALL, true, true, "n19.2", "n17.2", "n16.2", "n15.2", "n14.2", PoiType.MALL, true, true)
new Crossing(raphael_canvas, 200, 300, 200, 400, 0.0, 0.0, "n6.1", "n8.1", "n9.1", "n10.1", "n11.1", PoiType.POST, true, true, "n11.2", "n9.2", "n8.2", "n7.2", "n6.2", PoiType.POST, true, true)
new Crossing(raphael_canvas, 1350, 500, 1450, 500, 0.0, 0.0, "n127.1", "n129.1", "n130.1", "n131.1", "n132.1", PoiType.PUB, true, true, "n132.2", "n130.2", "n129.2", "n128.2", "n127.2", PoiType.PUB, true, true)
roundabouts["n169.1"] = new Roundabout(raphael_canvas, 2050, 500, 50, ["n169", 2000, 500, "n170", 2050, 550, "n171", 2100, 500, "n172", 2050, 450]);
new Crossing(raphael_canvas, 400, 900, 500, 900, 0.0, 0.0, "n34.1", "n36.1", "n37.1", "n38.1", "n39.1", PoiType.POST, true, true, "n39.2", "n37.2", "n36.2", "n35.2", "n34.2", PoiType.POST, true, true)
roundabouts["n291.1"] = new Roundabout(raphael_canvas, 2550, 1000, 50, ["n291", 2550, 1050, "n292", 2600, 1000, "n293", 2550, 950]);
roundabouts["n20.1"] = new Roundabout(raphael_canvas, 300, 900, 50, ["n20", 250, 900, "n21", 300, 950, "n22", 350, 900, "n23", 300, 850]);
new Crossing(raphael_canvas, 1700, 1050, 1800, 1050, 0.0, 0.0, "n240.1", "n242.1", "n243.1", "n244.1", "n245.1", PoiType.PUB, true, true, "n245.2", "n243.2", "n242.2", "n241.2", "n240.2", PoiType.PUB, true, true)
new Crossing(raphael_canvas, 1050, 500, 1150, 500, 0.0, 0.0, "n71.1", "n73.1", "n74.1", "n75.1", "n76.1", PoiType.PUB, true, true, "n76.2", "n74.2", "n73.2", "n72.2", "n71.2", PoiType.PUB, true, true)
new Crossing(raphael_canvas, 1450, 100, 1550, 100, 0.0, 0.0, "n135.1", "n137.1", "n138.1", "n139.1", "n140.1", PoiType.SPORT_CENTER, true, true, "n140.2", "n138.2", "n137.2", "n136.2", "n135.2", PoiType.SPORT_CENTER, true, true)
new Crossing(raphael_canvas, 2150, 500, 2250, 500, 0.0, 0.0, "n177.1", "n179.1", "n180.1", "n181.1", "n182.1", PoiType.SHOP, true, true, "n182.2", "n180.2", "n179.2", "n178.2", "n177.2", PoiType.SHOP, true, true)
new Crossing(raphael_canvas, 300, 1000, 300, 1100, 0.0, 0.0, "n25.1", "n27.1", "n28.1", "n29.1", "n30.1", PoiType.SPORT_CENTER, true, true, "n30.2", "n28.2", "n27.2", "n26.2", "n25.2", PoiType.SPORT_CENTER, true, true)
roundabouts["n143.1"] = new Roundabout(raphael_canvas, 1650, 100, 50, ["n143", 1600, 100, "n144", 1650, 150, "n145", 1700, 100]);
roundabouts["n94.1"] = new Roundabout(raphael_canvas, 1000, 900, 50, ["n94", 950, 900, "n95", 1000, 950, "n96", 1050, 900]);
roundabouts["n236.1"] = new Roundabout(raphael_canvas, 1900, 1050, 50, ["n236", 1850, 1050, "n237", 1900, 1100, "n238", 1950, 1050, "n239", 1900, 1000]);
new Crossing(raphael_canvas, 300, 200, 400, 200, 0.0, 0.0, "n42.1", "n44.1", "n45.1", "n46.1", "n47.1", PoiType.MALL, true, true, "n47.2", "n45.2", "n44.2", "n43.2", "n42.2", PoiType.MALL, true, true)
roundabouts["n1.1"] = new Roundabout(raphael_canvas, 200, 200, 50, ["n1", 150, 200, "n2", 200, 250, "n3", 250, 200, "n4", 200, 150]);
new Crossing(raphael_canvas, 1750, 100, 1850, 100, 0.0, 0.0, "n154.1", "n156.1", "n157.1", "n158.1", "n159.1", PoiType.PUB, true, true, "n159.2", "n157.2", "n156.2", "n155.2", "n154.2", PoiType.PUB, true, true)
new Crossing(raphael_canvas, 600, 200, 700, 200, 0.0, 0.0, "n49.1", "n51.1", "n52.1", "n53.1", "n54.1", PoiType.POST, true, true, "n54.2", "n52.2", "n51.2", "n50.2", "n49.2", PoiType.POST, true, true)
new Crossing(raphael_canvas, 2500, 600, 2600, 600, 0.0, 0.0, "n280.1", "n282.1", "n283.1", "n284.1", "n285.1", PoiType.POST, true, true, "n285.2", "n283.2", "n282.2", "n281.2", "n280.2", PoiType.POST, true, true)
new Crossing(raphael_canvas, 2400, 800, 2400, 700, 0.0, 0.0, "n267.1", "n269.1", "n270.1", "n271.1", "n272.1", PoiType.SPORT_CENTER, true, true, "n272.2", "n270.2", "n269.2", "n268.2", "n267.2", PoiType.SPORT_CENTER, true, true)
new Crossing(raphael_canvas, 1900, 850, 1900, 950, 0.0, 0.0, "n228.1", "n230.1", "n231.1", "n232.1", "n233.1", PoiType.AMUSEMENT_PARK, true, true, "n233.2", "n231.2", "n230.2", "n229.2", "n228.2", PoiType.AMUSEMENT_PARK, true, true)
new Crossing(raphael_canvas, 1000, 1000, 1000, 1100, 0.0, 0.0, "n111.1", "n113.1", "n114.1", "n115.1", "n116.1", PoiType.BANK, true, true, "n116.2", "n114.2", "n113.2", "n112.2", "n111.2", PoiType.BANK, true, true)
new Crossing(raphael_canvas, 1250, 400, 1250, 300, 0.0, 0.0, "n121.1", "n123.1", "n124.1", "n125.1", "n126.1", PoiType.UNIVERSITY, true, true, "n126.2", "n124.2", "n123.2", "n122.2", "n121.2", PoiType.UNIVERSITY, true, true)
roundabouts["n79.1"] = new Roundabout(raphael_canvas, 1250, 500, 50, ["n79", 1200, 500, "n80", 1250, 550, "n81", 1300, 500, "n82", 1250, 450]);
new Crossing(raphael_canvas, 800, 300, 800, 400, 0.0, 0.0, "n62.1", "n64.1", "n65.1", "n66.1", "n67.1", PoiType.SHOP, true, true, "n67.2", "n65.2", "n64.2", "n63.2", "n62.2", PoiType.SHOP, true, true)
new Crossing(raphael_canvas, 800, 900, 900, 900, 0.0, 0.0, "n103.1", "n105.1", "n106.1", "n107.1", "n108.1", PoiType.POST, true, true, "n108.2", "n106.2", "n105.2", "n104.2", "n103.2", PoiType.POST, true, true)
new Crossing(raphael_canvas, 2000, 1050, 2100, 1050, 0.0, 0.0, "n253.1", "n255.1", "n256.1", "n257.1", "n258.1", PoiType.MALL, true, true, "n258.2", "n256.2", "n255.2", "n254.2", "n253.2", PoiType.MALL, true, true)
new Crossing(raphael_canvas, 2450, 350, 2450, 250, 0.0, 0.0, "n193.1", "n195.1", "n196.1", "n197.1", "n198.1", PoiType.POST, true, true, "n198.2", "n196.2", "n195.2", "n194.2", "n193.2", PoiType.POST, true, true)
districts["dist1"] = GraphicUtils.getScaledBBox(0, -5, 900, 560);
districts["dist4"] = GraphicUtils.getScaledBBox(1845, -5, 1060, 705);
districts["dist5"] = GraphicUtils.getScaledBBox(1496, 700, 758, 650);
districts["dist2"] = GraphicUtils.getScaledBBox(100, 550, 1404, 755);
districts["dist3"] = GraphicUtils.getScaledBBox(950, 45, 1054, 801);
districts["dist6"] = GraphicUtils.getScaledBBox(2095, 546, 760, 654);