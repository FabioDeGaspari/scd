function Command(type, interest) {
	this.cmd_type = type
	this.interest = interest
}
Command.START = 1;
Command.STOP = 2;
Command.INTEREST = 3;
Command.DEBUG = -1;


function Interest(id, interests, lost_interests, level) {
	this.id = id
	this.interests = interests
	this.lost_interests = lost_interests
	this.level = typeof level !== 'undefined' ? level : Interest.FINE_LEVEL
}
Interest.COARSE_LEVEL = 0;
Interest.FINE_LEVEL = 1;
Interest.FOCUS_LEVEL = 2;

function SubscriberEvent(event_type, obj_id, target_id, action_duration, wait_duration) {
	this.event_type = event_type
	this.obj_id = obj_id
	this.target_id = target_id
	this.action_duration = action_duration
	this.wait_duration = wait_duration
}
SubscriberEvent.OBJECTS_SETUP = "objs_setup";
SubscriberEvent.WAIT = "wait";
SubscriberEvent.TYPE_WAIT_FOR_EVENT = "wait_for_event";
SubscriberEvent.INIT_COMPLETE = "init_complete";
SubscriberEvent.SEGMENT_STATUS_CHANGE = "segment_status_change";
SubscriberEvent.OBJECT_MOVE_TO = "object_move_to";
SubscriberEvent.OBJECT_USES = "use";
SubscriberEvent.OBJECT_DO_STOP = "do_stop";
SubscriberEvent.TERMINATE = "terminate";
SubscriberEvent.FOCUS = "focus_";


var socket
var uid
var objects = []

function websocket_init(id, addr) {
	socket = new WebSocket(addr);
	uid = id

	socket.onopen = function() {
		console.log(uid + ": socket opened")
		handleInterests(panZoom.curView)
	}

	socket.onclose = function() {
		console.log(uid + ": socket closed")
	}

	socket.onmessage = function(e) {
		handleEvent(e.data)
	}
}

function handleEvent(event_string) {
	var events = JSON.parse(event_string, null)
	for(var i=0; i<events.length; i++) {
		var event = events[i]
		var path
		if(event == null) {
			console.log("ERROR: event is null")
			console.log(JSON.stringify(events))
			continue;
		}
		console.log(event)
		if(event.event_type.lastIndexOf(SubscriberEvent.FOCUS, 0) === 0) {
			updateFocus(event)
			continue;
		}
		switch(event.event_type) {
		case SubscriberEvent.OBJECTS_SETUP:
//			console.log(event.target_id+"\n\n")
			var map = JSON.parse(event.target_id);
			for(key in map) {
				for(var i=0; i<map[key].length; i++) {
					var obj = map[key][i]
//					console.log(obj)
					objects[obj.id] = new GraphicObject(raphael_canvas, obj, pois[key].getPos().x, pois[key].getPos().y, key)
				}
			}
			break
		case SubscriberEvent.OBJECT_MOVE_TO:
			if(interest_level == Interest.COARSE_LEVEL)
				break
			validateObjects(event.obj_ids)
			for(var j=0; j<event.obj_ids.length; j++) {
				var object = event.obj_ids[j]
				path = getObjectPath(event, objects[object.id])
				if(path == undefined) {
					console.log("ERROR: received event with unknown target: "+event.target_id)
					return
				}
				objects[object.id].moveAlong(path, event.duration, event.progress)
			}
			break
		case SubscriberEvent.TYPE_WAIT_FOR_EVENT:
//			console.log(event)
			validateObjects(event.obj_ids)
			for(var j=0; j<event.obj_ids.length; j++) {
				var object = event.obj_ids[j]
				if(objects[object.id].type == "PRIVATE_VEHICLE")
					objects[object.id].moveToPoint(pois[event.target_id].getCenter(), MIN_POI_MOVETO_DUR, event.target_id)
			}
			break
		case SubscriberEvent.WAIT:
//			console.log(event)
			validateObjects(event.obj_ids)
			for(var j=0; j<event.obj_ids.length; j++) {
				var object = event.obj_ids[j]
				if(pois[event.target_id] == undefined)
					console.log("couldn't find target "+event.target_id)
				objects[object.id].moveToPoint(pois[event.target_id].getCenter(), Math.min(MIN_POI_MOVETO_DUR, event.duration/2), event.target_id)
			}
			break
		case SubscriberEvent.OBJECT_USES:
//			console.log(event)
			validateObjects(event.obj_ids)
			for(var j=0; j<event.obj_ids.length; j++) {
				var object = event.obj_ids[j]
				var target_obj = pois[event.target_id];
				if(target_obj == undefined)	//the target is another object
					if(event.target_id.lastIndexOf("bus", 0) === 0)
						target_obj = objects[object.id].getPosition()
					else
						target_obj = objects[event.target_id].getCenter();
				if(target_obj == undefined)
					console.log("couldn't find target "+event.target_id)
				objects[object.id].moveToPoint(target_obj, event.duration, event.target_id, (function(o){return o.img.hide})(objects[object.id]));
			}
			break
		case SubscriberEvent.OBJECT_DO_STOP:
			validateObjects(event.obj_ids)
			for(var j=0; j<event.obj_ids.length; j++) {
				var object = event.obj_ids[j]
				objects[object.id].moveToPoint(pois[event.target_id].getBusStop(), MIN_POI_MOVETO_DUR/2)//, function(){handleEvent(event.additional_info)}
			}
			break
		case SubscriberEvent.SEGMENT_STATUS_CHANGE:
			var lane = lanes[event.target_id]
			if(lane == undefined)
				lane = lanes[event.target_id.split("-")[0]]
			Segment.scaleColour(event.duration, lane)
			break
		case SubscriberEvent.TERMINATE:
			window.alert("Simulation Terminated")
			socket.close()
			break
		}
	}
}


function validateObjects(ids) {
	for(var i=0; i<ids.length; ++i) {
		if(objects[ids[i].id] == undefined) {
			objects[ids[i].id] = new GraphicObject(raphael_canvas, ids[i], 0, 0)
		}
	}
}

function getObjectPath(event, obj) {
	var path_id = event.target_id
	var ret = getPathById(path_id, obj.type);
	if(ret == undefined)
		console.log(path_id)
	ret = ret.attr('path').toString()
	return ret
}

function getPathById(path_id, obj_type) {
	switch(obj_type) {
	case "PEDESTRIAN":
		return walkways[path_id]
	case "PRIVATE_VEHICLE":
		return carlanes[path_id]
	case "PUBLIC_VEHICLE":
		return buslanes[path_id]
	}
	return null
}

//map <id, bbox>
var interests = []
var last_handled = 0;
var HANDLE_INTERVAL = 500;
var interest_level
var level_thresh = 8;
var terminate = false

function handleInterests(curView) {
	if(terminate)
		return;
	var do_update = false
	var now = Date.now()
	var cur_lev = curView.zoomLevel>level_thresh ? Interest.FINE_LEVEL : Interest.COARSE_LEVEL
	if(interest_level != cur_lev) {
		do_update = true
		interest_level = cur_lev
		if(interest_level == Interest.COARSE_LEVEL) {
			for(obj in objects) {
				objects[obj].hide()
			}
		} else {
			for(idx in lanes)
				Segment.scaleColour(SEG_MIN_GRAD, lanes[idx])
		}
	}
	if(now-last_handled < HANDLE_INTERVAL) {
		updateInterests([], [], do_update, cur_lev)
		return
	}
	last_handled = now
	var new_interests = []	//normal array
	var lost_interests = [] //normal array
	for(key in districts) {
		if(GraphicUtils.doBoxesIntersect(districts[key], curView)) {
			if(!(key in interests)) {
				new_interests.push(key)
				interests[key] = districts[key]
			}
				
		} else {
			if(key in interests) {
				lost_interests.push(key)
				delete interests[key]
			}
		}
	}
	var all = "" 
	for(key in interests)
		all += key
	updateInterests(new_interests, lost_interests, do_update, interest_level)
}

panZoom.setViewModHandler(handleInterests)

function updateInterests(new_interests, lost_interests, do_update, update_lev) {
	if(new_interests.length==0 && lost_interests.length==0 && !do_update)
		return
	socket.send(JSON.stringify(new Command(Command.INTEREST, new Interest(uid, new_interests, lost_interests, update_lev))))
}

function queryObj() {
	var obj_id = document.getElementById("obj_id_text").value
	updateInterests([obj_id], [], true, Interest.FOCUS_LEVEL);
}

function stopTracking() {
	updateInterests([], [], true, Interest.FOCUS_LEVEL);
	document.getElementById("focus_area").value = "";
}

function updateFocus(event) {
	var text_area = document.getElementById("focus_area")
	var ev = event.event_type.substring(SubscriberEvent.FOCUS.length, event.event_type.length)
	text_area.value += "\n"+event.description
	text_area.scrollTop = text_area.scrollHeight;
}